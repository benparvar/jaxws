package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.AssinaturaEnum;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.util.ConstantesCte;
import com.benparvar.cte.util.LoggerUtil;
import com.benparvar.cte.util.ObjetoCTeUtil;
import com.benparvar.cte.util.WebServiceCteUtil;
import com.benparvar.cte.wsdl.cterecepcaoevento.CteRecepcaoEventoStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axis2.transport.http.HTTPConstants;

import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

/**
 * The type Eventos.
 */
class Eventos {

    /**
     * Enviar evento string.
     *
     * @param config     the config
     * @param xml        the xml
     * @param tipoEvento the tipo evento
     * @param valida     the valida
     * @return the string
     * @throws CteException the cte exception
     */
    static String enviarEvento(ConfiguracoesCte config, String xml, ServicosEnum tipoEvento, boolean valida)
            throws CteException {

        try {

            xml = Assinar.assinaCte(config, xml, AssinaturaEnum.EVENTO);

            LoggerUtil.log(Eventos.class, "[XML-ENVIO-" + tipoEvento + "]: " + xml);

            if (valida) {
                new Validar().validaXml(config, xml, ServicosEnum.EVENTO);
            }

            OMElement ome = AXIOMUtil.stringToOM(xml);

            CteRecepcaoEventoStub.CteDadosMsg dadosMsg = new CteRecepcaoEventoStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);

            CteRecepcaoEventoStub.CteCabecMsg cteCabecMsg = new CteRecepcaoEventoStub.CteCabecMsg();
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteRecepcaoEventoStub.CteCabecMsgE cteCabecMsgE = new CteRecepcaoEventoStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            String url = WebServiceCteUtil.getUrl(config, tipoEvento);

            CteRecepcaoEventoStub stub = new CteRecepcaoEventoStub(url);
            // Timeout
            if (ObjetoCTeUtil.verifica(config.getTimeout()).isPresent()) {
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getTimeout());
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeout());
            }
            CteRecepcaoEventoStub.CteRecepcaoEventoResult result = stub.cteRecepcaoEvento(dadosMsg, cteCabecMsgE);

            LoggerUtil.log(Eventos.class, "[XML-RETORNO-" + tipoEvento + "]: " + result.getExtraElement().toString());
            return result.getExtraElement().toString();
        } catch (RemoteException | XMLStreamException e) {
            throw new CteException(e.getMessage());
        }

    }
}