package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.AssinaturaEnum;
import com.benparvar.cte.dom.enuns.EstadosEnum;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.envicte.TEnviCTe;
import com.benparvar.cte.schema_300.retenvicte.TRetEnviCTe;
import com.benparvar.cte.util.ConstantesCte;
import com.benparvar.cte.util.LoggerUtil;
import com.benparvar.cte.util.WebServiceCteUtil;
import com.benparvar.cte.util.XmlCteUtil;
import com.benparvar.cte.wsdl.cterecepcao.CteRecepcaoStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;
import java.util.Iterator;

/**
 * The type Envio cte.
 */
class EnvioCte {

    /**
     * Monta cte t envi c te.
     *
     * @param config  the config
     * @param enviCTe the envi c te
     * @param valida  the valida
     * @return the t envi c te
     * @throws CteException the cte exception
     */
    static TEnviCTe montaCte(ConfiguracoesCte config, TEnviCTe enviCTe, boolean valida) throws CteException {
        try {

            /**
             * Cria o xml
             */
            String xml = XmlCteUtil.objectToXml(enviCTe);

            /**
             * Assina o Xml
             */
            xml = Assinar.assinaCte(config, xml, AssinaturaEnum.CTE);

            //Retira Quebra de Linha
            xml = xml.replaceAll(System.lineSeparator(), "");

            LoggerUtil.log(EnvioCte.class, "[XML-ASSINADO]: " + xml);

            /**
             * Valida o Xml caso sejá selecionado True
             */
            if (valida) {
                new Validar().validaXml(config, xml, ServicosEnum.ENVIO_CTE);
            }

            return XmlCteUtil.xmlToObject(xml, TEnviCTe.class);

        } catch (Exception e) {
            throw new CteException(e.getMessage());
        }

    }

    /**
     * Envia cte t ret envi c te.
     *
     * @param config  the config
     * @param enviCTe the envi c te
     * @return the t ret envi c te
     * @throws CteException the cte exception
     */
    static TRetEnviCTe enviaCte(ConfiguracoesCte config, TEnviCTe enviCTe) throws CteException {

        try {

            String xml = XmlCteUtil.objectToXml(enviCTe);

            OMElement ome = AXIOMUtil.stringToOM(xml);

            if (config.getEstado().equals(EstadosEnum.PR)) {
                Iterator<?> children = ome.getChildrenWithLocalName("CTe");
                while (children.hasNext()) {
                    OMElement omElement = (OMElement) children.next();
                    if (omElement != null && "CTe".equals(omElement.getLocalName())) {
                        omElement.addAttribute("xmlns", "http://www.portalfiscal.inf.br/cte", null);
                    }
                }
            }

            LoggerUtil.log(EnvioCte.class, "[XML-ENVIO]: " + xml);

            CteRecepcaoStub.CteDadosMsg dadosMsg = new CteRecepcaoStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);
            CteRecepcaoStub.CteCabecMsg cteCabecMsg = new CteRecepcaoStub.CteCabecMsg();

            /**
             * Codigo do Estado.
             */
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));

            /**
             * Versao do XML
             */
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteRecepcaoStub.CteCabecMsgE cteCabecMsgE = new CteRecepcaoStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            CteRecepcaoStub stub = new CteRecepcaoStub(
                    WebServiceCteUtil.getUrl(config, ServicosEnum.ENVIO_CTE));
            CteRecepcaoStub.CteRecepcaoLoteResult result = stub.cteRecepcaoLote(dadosMsg, cteCabecMsgE);

            return XmlCteUtil.xmlToObject(result.getExtraElement().toString(),
                    TRetEnviCTe.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }

}
