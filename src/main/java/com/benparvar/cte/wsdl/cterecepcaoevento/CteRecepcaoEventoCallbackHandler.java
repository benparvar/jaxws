/**
 * CteRecepcaoEventoCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cterecepcaoevento;


/**
 * The type Cte recepcao evento callback handler.
 */
public abstract class CteRecepcaoEventoCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte recepcao evento callback handler.
     *
     * @param clientData the client data
     */
    public CteRecepcaoEventoCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte recepcao evento callback handler.
     */
    public CteRecepcaoEventoCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte recepcao evento.
     *
     * @param result the result
     */
    public void receiveResultcteRecepcaoEvento(
            com.benparvar.cte.wsdl.cterecepcaoevento.CteRecepcaoEventoStub.CteRecepcaoEventoResult result) {
    }

    /**
     * Receive errorcte recepcao evento.
     *
     * @param e the e
     */
    public void receiveErrorcteRecepcaoEvento(Exception e) {
    }
}
