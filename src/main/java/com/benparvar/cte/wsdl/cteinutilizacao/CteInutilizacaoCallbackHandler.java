/**
 * CteInutilizacaoCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cteinutilizacao;


/**
 * The type Cte inutilizacao callback handler.
 */
public abstract class CteInutilizacaoCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte inutilizacao callback handler.
     *
     * @param clientData the client data
     */
    public CteInutilizacaoCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte inutilizacao callback handler.
     */
    public CteInutilizacaoCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte inutilizacao ct.
     *
     * @param result the result
     */
    public void receiveResultcteInutilizacaoCT(
            com.benparvar.cte.wsdl.cteinutilizacao.CteInutilizacaoStub.CteInutilizacaoCTResult result) {
    }

    /**
     * Receive errorcte inutilizacao ct.
     *
     * @param e the e
     */
    public void receiveErrorcteInutilizacaoCT(Exception e) {
    }
}
