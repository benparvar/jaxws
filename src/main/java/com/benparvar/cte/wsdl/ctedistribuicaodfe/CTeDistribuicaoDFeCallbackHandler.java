/**
 * CTeDistribuicaoDFeCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Feb 28, 2020 (09:45:00 IST)
 */
package com.benparvar.cte.wsdl.ctedistribuicaodfe;

/**
 * The type C te distribuicao d fe callback handler.
 */
public abstract class CTeDistribuicaoDFeCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new C te distribuicao d fe callback handler.
     *
     * @param clientData the client data
     */
    public CTeDistribuicaoDFeCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new C te distribuicao d fe callback handler.
     */
    public CTeDistribuicaoDFeCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte dist d fe interesse.
     *
     * @param result the result
     */
    public void receiveResultcteDistDFeInteresse(
            CTeDistribuicaoDFeStub.CteDistDFeInteresseResponse result
    ) {
    }

    /**
     * Receive errorcte dist d fe interesse.
     *
     * @param e the e
     */
    public void receiveErrorcteDistDFeInteresse(Exception e) {
    }
}
