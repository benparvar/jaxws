/**
 * CteStatusServicoCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.ctestatusservico;


/**
 * The type Cte status servico callback handler.
 */
public abstract class CteStatusServicoCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte status servico callback handler.
     *
     * @param clientData the client data
     */
    public CteStatusServicoCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte status servico callback handler.
     */
    public CteStatusServicoCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte status servico ct.
     *
     * @param result the result
     */
    public void receiveResultcteStatusServicoCT(
            com.benparvar.cte.wsdl.ctestatusservico.CteStatusServicoStub.CteStatusServicoCTResult result) {
    }

    /**
     * Receive errorcte status servico ct.
     *
     * @param e the e
     */
    public void receiveErrorcteStatusServicoCT(Exception e) {
    }
}
