/**
 * CteRecepcaoOSCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cterecepcaoos;


/**
 * The type Cte recepcao os callback handler.
 */
public abstract class CteRecepcaoOSCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte recepcao os callback handler.
     *
     * @param clientData the client data
     */
    public CteRecepcaoOSCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte recepcao os callback handler.
     */
    public CteRecepcaoOSCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte recepcao os.
     *
     * @param result the result
     */
    public void receiveResultcteRecepcaoOS(
            com.benparvar.cte.wsdl.cterecepcaoos.CteRecepcaoOSStub.CteRecepcaoOSResult result) {
    }

    /**
     * Receive errorcte recepcao os.
     *
     * @param e the e
     */
    public void receiveErrorcteRecepcaoOS(Exception e) {
    }
}
