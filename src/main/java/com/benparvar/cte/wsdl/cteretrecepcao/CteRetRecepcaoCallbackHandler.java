/**
 * CteRetRecepcaoCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cteretrecepcao;


/**
 * The type Cte ret recepcao callback handler.
 */
public abstract class CteRetRecepcaoCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte ret recepcao callback handler.
     *
     * @param clientData the client data
     */
    public CteRetRecepcaoCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte ret recepcao callback handler.
     */
    public CteRetRecepcaoCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte ret recepcao.
     *
     * @param result the result
     */
    public void receiveResultcteRetRecepcao(
            com.benparvar.cte.wsdl.cteretrecepcao.CteRetRecepcaoStub.CteRetRecepcaoResult result) {
    }

    /**
     * Receive errorcte ret recepcao.
     *
     * @param e the e
     */
    public void receiveErrorcteRetRecepcao(Exception e) {
    }
}
