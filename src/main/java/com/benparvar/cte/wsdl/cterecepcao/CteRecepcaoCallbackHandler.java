/**
 * CteRecepcaoCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cterecepcao;


/**
 * The type Cte recepcao callback handler.
 */
public abstract class CteRecepcaoCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte recepcao callback handler.
     *
     * @param clientData the client data
     */
    public CteRecepcaoCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte recepcao callback handler.
     */
    public CteRecepcaoCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte recepcao lote.
     *
     * @param result the result
     */
    public void receiveResultcteRecepcaoLote(
            CteRecepcaoStub.CteRecepcaoLoteResult result) {
    }

    /**
     * Receive errorcte recepcao lote.
     *
     * @param e the e
     */
    public void receiveErrorcteRecepcaoLote(Exception e) {
    }
}
