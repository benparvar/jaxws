/**
 * CteConsultaCallbackHandler.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.5  Built on : May 06, 2017 (03:45:26 BST)
 */
package com.benparvar.cte.wsdl.cteconsulta;


/**
 * The type Cte consulta callback handler.
 */
public abstract class CteConsultaCallbackHandler {
    /**
     * The Client data.
     */
    protected Object clientData;

    /**
     * Instantiates a new Cte consulta callback handler.
     *
     * @param clientData the client data
     */
    public CteConsultaCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Instantiates a new Cte consulta callback handler.
     */
    public CteConsultaCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Gets client data.
     *
     * @return the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * Receive resultcte consulta ct.
     *
     * @param result the result
     */
    public void receiveResultcteConsultaCT(
            com.benparvar.cte.wsdl.cteconsulta.CteConsultaStub.CteConsultaCTResult result) {
    }

    /**
     * Receive errorcte consulta ct.
     *
     * @param e the e
     */
    public void receiveErrorcteConsultaCT(Exception e) {
    }
}
