//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:29 PM BRT 
//


package com.benparvar.cte.schema_300.evregmultimodal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The type X 509 data type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X509DataType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "x509Certificate"
})
public class X509DataType {

    /**
     * The X 509 certificate.
     */
    @XmlElement(name = "X509Certificate", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected byte[] x509Certificate;

    /**
     * Get x 509 certificate byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getX509Certificate() {
        return x509Certificate;
    }

    /**
     * Sets x 509 certificate.
     *
     * @param value the value
     */
    public void setX509Certificate(byte[] value) {
        this.x509Certificate = value;
    }

}
