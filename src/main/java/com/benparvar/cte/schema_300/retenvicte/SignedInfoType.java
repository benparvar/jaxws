//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:57:45 PM BRT 
//


package com.benparvar.cte.schema_300.retenvicte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type Signed info type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignedInfoType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "canonicalizationMethod",
        "signatureMethod",
        "reference"
})
public class SignedInfoType {

    /**
     * The Canonicalization method.
     */
    @XmlElement(name = "CanonicalizationMethod", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignedInfoType.CanonicalizationMethod canonicalizationMethod;
    /**
     * The Signature method.
     */
    @XmlElement(name = "SignatureMethod", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignedInfoType.SignatureMethod signatureMethod;
    /**
     * The Reference.
     */
    @XmlElement(name = "Reference", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected ReferenceType reference;
    /**
     * The Id.
     */
    @XmlAttribute(name = "Id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets canonicalization method.
     *
     * @return the canonicalization method
     */
    public SignedInfoType.CanonicalizationMethod getCanonicalizationMethod() {
        return canonicalizationMethod;
    }

    /**
     * Sets canonicalization method.
     *
     * @param value the value
     */
    public void setCanonicalizationMethod(SignedInfoType.CanonicalizationMethod value) {
        this.canonicalizationMethod = value;
    }

    /**
     * Gets signature method.
     *
     * @return the signature method
     */
    public SignedInfoType.SignatureMethod getSignatureMethod() {
        return signatureMethod;
    }

    /**
     * Sets signature method.
     *
     * @param value the value
     */
    public void setSignatureMethod(SignedInfoType.SignatureMethod value) {
        this.signatureMethod = value;
    }

    /**
     * Gets reference.
     *
     * @return the reference
     */
    public ReferenceType getReference() {
        return reference;
    }

    /**
     * Sets reference.
     *
     * @param value the value
     */
    public void setReference(ReferenceType value) {
        this.reference = value;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param value the value
     */
    public void setId(String value) {
        this.id = value;
    }


    /**
     * The type Canonicalization method.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CanonicalizationMethod {

        /**
         * The Algorithm.
         */
        @XmlAttribute(name = "Algorithm", required = true)
        @XmlSchemaType(name = "anyURI")
        protected String algorithm;

        /**
         * Gets algorithm.
         *
         * @return the algorithm
         */
        public String getAlgorithm() {
            if (algorithm == null) {
                return "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
            } else {
                return algorithm;
            }
        }

        /**
         * Sets algorithm.
         *
         * @param value the value
         */
        public void setAlgorithm(String value) {
            this.algorithm = value;
        }

    }


    /**
     * The type Signature method.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SignatureMethod {

        /**
         * The Algorithm.
         */
        @XmlAttribute(name = "Algorithm", required = true)
        @XmlSchemaType(name = "anyURI")
        protected String algorithm;

        /**
         * Gets algorithm.
         *
         * @return the algorithm
         */
        public String getAlgorithm() {
            if (algorithm == null) {
                return "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
            } else {
                return algorithm;
            }
        }

        /**
         * Sets algorithm.
         *
         * @param value the value
         */
        public void setAlgorithm(String value) {
            this.algorithm = value;
        }

    }

}
