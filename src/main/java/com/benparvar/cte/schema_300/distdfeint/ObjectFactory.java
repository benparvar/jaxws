package com.benparvar.cte.schema_300.distdfeint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DistDFeInt_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "distDFeInt");

    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create dist d fe int dist d fe int.
     *
     * @return the dist d fe int
     */
    public DistDFeInt createDistDFeInt() {
        return new DistDFeInt();
    }

    /**
     * Create dist d fe int dist nsu dist d fe int . dist nsu.
     *
     * @return the dist d fe int . dist nsu
     */
    public DistDFeInt.DistNSU createDistDFeIntDistNSU() {
        return new DistDFeInt.DistNSU();
    }

    /**
     * Create dist d fe int cons nsu dist d fe int . cons nsu.
     *
     * @return the dist d fe int . cons nsu
     */
    public DistDFeInt.ConsNSU createDistDFeIntConsNSU() {
        return new DistDFeInt.ConsNSU();
    }

    /**
     * Create dist d fe int jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "distDFeInt")
    public JAXBElement<DistDFeInt> createDistDFeInt(DistDFeInt value) {
        return new JAXBElement<DistDFeInt>(_DistDFeInt_QNAME, DistDFeInt.class, null, value);
    }

}
