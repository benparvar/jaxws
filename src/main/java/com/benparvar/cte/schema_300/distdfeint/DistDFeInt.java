package com.benparvar.cte.schema_300.distdfeint;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type Dist d fe int.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tpAmb",
        "cufAutor",
        "cnpj",
        "cpf",
        "distNSU",
        "consNSU"
})
@XmlRootElement(name = "distDFeInt")
public class DistDFeInt {

    /**
     * The Tp amb.
     */
    @XmlElement(required = true)
    protected String tpAmb;
    /**
     * The Cuf autor.
     */
    @XmlElement(name = "cUFAutor", required = true)
    protected String cufAutor;
    /**
     * The Cnpj.
     */
    @XmlElement(name = "CNPJ")
    protected String cnpj;
    /**
     * The Cpf.
     */
    @XmlElement(name = "CPF")
    protected String cpf;
    /**
     * The Dist nsu.
     */
    protected DistNSU distNSU;
    /**
     * The Cons nsu.
     */
    protected ConsNSU consNSU;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets cuf autor.
     *
     * @return the cuf autor
     */
    public String getCUFAutor() {
        return cufAutor;
    }

    /**
     * Sets cuf autor.
     *
     * @param value the value
     */
    public void setCUFAutor(String value) {
        this.cufAutor = value;
    }

    /**
     * Gets cnpj.
     *
     * @return the cnpj
     */
    public String getCNPJ() {
        return cnpj;
    }

    /**
     * Sets cnpj.
     *
     * @param value the value
     */
    public void setCNPJ(String value) {
        this.cnpj = value;
    }

    /**
     * Gets cpf.
     *
     * @return the cpf
     */
    public String getCPF() {
        return cpf;
    }

    /**
     * Sets cpf.
     *
     * @param value the value
     */
    public void setCPF(String value) {
        this.cpf = value;
    }

    /**
     * Gets dist nsu.
     *
     * @return the dist nsu
     */
    public DistNSU getDistNSU() {
        return distNSU;
    }

    /**
     * Sets dist nsu.
     *
     * @param value the value
     */
    public void setDistNSU(DistNSU value) {
        this.distNSU = value;
    }

    /**
     * Gets cons nsu.
     *
     * @return the cons nsu
     */
    public ConsNSU getConsNSU() {
        return consNSU;
    }

    /**
     * Sets cons nsu.
     *
     * @param value the value
     */
    public void setConsNSU(ConsNSU value) {
        this.consNSU = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Cons nsu.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "nsu"
    })
    public static class ConsNSU {

        /**
         * The Nsu.
         */
        @XmlElement(name = "NSU", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        protected String nsu;

        /**
         * Gets nsu.
         *
         * @return the nsu
         */
        public String getNSU() {
            return nsu;
        }

        /**
         * Sets nsu.
         *
         * @param value the value
         */
        public void setNSU(String value) {
            this.nsu = value;
        }

    }


    /**
     * The type Dist nsu.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ultNSU"
    })
    public static class DistNSU {

        /**
         * The Ult nsu.
         */
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        protected String ultNSU;

        /**
         * Gets ult nsu.
         *
         * @return the ult nsu
         */
        public String getUltNSU() {
            return ultNSU;
        }

        /**
         * Sets ult nsu.
         *
         * @param value the value
         */
        public void setUltNSU(String value) {
            this.ultNSU = value;
        }

    }

}
