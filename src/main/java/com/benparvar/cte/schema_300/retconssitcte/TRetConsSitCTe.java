//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:55:16 PM BRT 
//


package com.benparvar.cte.schema_300.retconssitcte;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The type T ret cons sit c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetConsSitCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "verAplic",
        "cStat",
        "xMotivo",
        "cuf",
        "protCTe",
        "procEventoCTe"
})
public class TRetConsSitCTe {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The Ver aplic.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String verAplic;
    /**
     * The C stat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cStat;
    /**
     * The X motivo.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMotivo;
    /**
     * The Cuf.
     */
    @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cuf;
    /**
     * The Prot c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected TRetConsSitCTe.ProtCTe protCTe;
    /**
     * The Proc evento c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<ProcEventoCTe> procEventoCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets ver aplic.
     *
     * @return the ver aplic
     */
    public String getVerAplic() {
        return verAplic;
    }

    /**
     * Sets ver aplic.
     *
     * @param value the value
     */
    public void setVerAplic(String value) {
        this.verAplic = value;
    }

    /**
     * Gets c stat.
     *
     * @return the c stat
     */
    public String getCStat() {
        return cStat;
    }

    /**
     * Sets c stat.
     *
     * @param value the value
     */
    public void setCStat(String value) {
        this.cStat = value;
    }

    /**
     * Gets x motivo.
     *
     * @return the x motivo
     */
    public String getXMotivo() {
        return xMotivo;
    }

    /**
     * Sets x motivo.
     *
     * @param value the value
     */
    public void setXMotivo(String value) {
        this.xMotivo = value;
    }

    /**
     * Gets cuf.
     *
     * @return the cuf
     */
    public String getCUF() {
        return cuf;
    }

    /**
     * Sets cuf.
     *
     * @param value the value
     */
    public void setCUF(String value) {
        this.cuf = value;
    }

    /**
     * Gets prot c te.
     *
     * @return the prot c te
     */
    public TRetConsSitCTe.ProtCTe getProtCTe() {
        return protCTe;
    }

    /**
     * Sets prot c te.
     *
     * @param value the value
     */
    public void setProtCTe(TRetConsSitCTe.ProtCTe value) {
        this.protCTe = value;
    }

    /**
     * Gets proc evento c te.
     *
     * @return the proc evento c te
     */
    public List<ProcEventoCTe> getProcEventoCTe() {
        if (procEventoCTe == null) {
            procEventoCTe = new ArrayList<ProcEventoCTe>();
        }
        return this.procEventoCTe;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Proc evento c te.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "any"
    })
    public static class ProcEventoCTe {

        /**
         * The Any.
         */
        @XmlAnyElement(lax = true)
        protected Object any;
        /**
         * The Versao.
         */
        @XmlAttribute(name = "versao", required = true)
        protected String versao;

        /**
         * Gets any.
         *
         * @return the any
         */
        public Object getAny() {
            return any;
        }

        /**
         * Sets any.
         *
         * @param value the value
         */
        public void setAny(Object value) {
            this.any = value;
        }

        /**
         * Gets versao.
         *
         * @return the versao
         */
        public String getVersao() {
            return versao;
        }

        /**
         * Sets versao.
         *
         * @param value the value
         */
        public void setVersao(String value) {
            this.versao = value;
        }

    }


    /**
     * The type Prot c te.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "any"
    })
    public static class ProtCTe {

        /**
         * The Any.
         */
        @XmlAnyElement
        protected Element any;
        /**
         * The Versao.
         */
        @XmlAttribute(name = "versao", required = true)
        protected String versao;

        /**
         * Gets any.
         *
         * @return the any
         */
        public Element getAny() {
            return any;
        }

        /**
         * Sets any.
         *
         * @param value the value
         */
        public void setAny(Element value) {
            this.any = value;
        }

        /**
         * Gets versao.
         *
         * @return the versao
         */
        public String getVersao() {
            return versao;
        }

        /**
         * Sets versao.
         *
         * @param value the value
         */
        public void setVersao(String value) {
            this.versao = value;
        }

    }

}
