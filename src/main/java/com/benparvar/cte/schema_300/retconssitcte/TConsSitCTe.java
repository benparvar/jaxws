//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:55:16 PM BRT 
//


package com.benparvar.cte.schema_300.retconssitcte;

import javax.xml.bind.annotation.*;


/**
 * The type T cons sit c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TConsSitCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "xServ",
        "chCTe"
})
public class TConsSitCTe {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The X serv.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xServ;
    /**
     * The Ch c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String chCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets x serv.
     *
     * @return the x serv
     */
    public String getXServ() {
        return xServ;
    }

    /**
     * Sets x serv.
     *
     * @param value the value
     */
    public void setXServ(String value) {
        this.xServ = value;
    }

    /**
     * Gets ch c te.
     *
     * @return the ch c te
     */
    public String getChCTe() {
        return chCTe;
    }

    /**
     * Sets ch c te.
     *
     * @param value the value
     */
    public void setChCTe(String value) {
        this.chCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
