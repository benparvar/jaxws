//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:03 PM BRT 
//


package com.benparvar.cte.schema_300.evprestdesacordo;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type T evento.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TEvento", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infEvento",
        "signature"
})
public class TEvento {

    /**
     * The Inf evento.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TEvento.InfEvento infEvento;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf evento.
     *
     * @return the inf evento
     */
    public TEvento.InfEvento getInfEvento() {
        return infEvento;
    }

    /**
     * Sets inf evento.
     *
     * @param value the value
     */
    public void setInfEvento(TEvento.InfEvento value) {
        this.infEvento = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf evento.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cOrgao",
            "tpAmb",
            "cnpj",
            "chCTe",
            "dhEvento",
            "tpEvento",
            "nSeqEvento",
            "detEvento"
    })
    public static class InfEvento {

        /**
         * The C orgao.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cOrgao;
        /**
         * The Tp amb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpAmb;
        /**
         * The Cnpj.
         */
        @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cnpj;
        /**
         * The Ch c te.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String chCTe;
        /**
         * The Dh evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String dhEvento;
        /**
         * The Tp evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpEvento;
        /**
         * The N seq evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nSeqEvento;
        /**
         * The Det evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TEvento.InfEvento.DetEvento detEvento;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;

        /**
         * Gets c orgao.
         *
         * @return the c orgao
         */
        public String getCOrgao() {
            return cOrgao;
        }

        /**
         * Sets c orgao.
         *
         * @param value the value
         */
        public void setCOrgao(String value) {
            this.cOrgao = value;
        }

        /**
         * Gets tp amb.
         *
         * @return the tp amb
         */
        public String getTpAmb() {
            return tpAmb;
        }

        /**
         * Sets tp amb.
         *
         * @param value the value
         */
        public void setTpAmb(String value) {
            this.tpAmb = value;
        }

        /**
         * Gets cnpj.
         *
         * @return the cnpj
         */
        public String getCNPJ() {
            return cnpj;
        }

        /**
         * Sets cnpj.
         *
         * @param value the value
         */
        public void setCNPJ(String value) {
            this.cnpj = value;
        }

        /**
         * Gets ch c te.
         *
         * @return the ch c te
         */
        public String getChCTe() {
            return chCTe;
        }

        /**
         * Sets ch c te.
         *
         * @param value the value
         */
        public void setChCTe(String value) {
            this.chCTe = value;
        }

        /**
         * Gets dh evento.
         *
         * @return the dh evento
         */
        public String getDhEvento() {
            return dhEvento;
        }

        /**
         * Sets dh evento.
         *
         * @param value the value
         */
        public void setDhEvento(String value) {
            this.dhEvento = value;
        }

        /**
         * Gets tp evento.
         *
         * @return the tp evento
         */
        public String getTpEvento() {
            return tpEvento;
        }

        /**
         * Sets tp evento.
         *
         * @param value the value
         */
        public void setTpEvento(String value) {
            this.tpEvento = value;
        }

        /**
         * Gets n seq evento.
         *
         * @return the n seq evento
         */
        public String getNSeqEvento() {
            return nSeqEvento;
        }

        /**
         * Sets n seq evento.
         *
         * @param value the value
         */
        public void setNSeqEvento(String value) {
            this.nSeqEvento = value;
        }

        /**
         * Gets det evento.
         *
         * @return the det evento
         */
        public TEvento.InfEvento.DetEvento getDetEvento() {
            return detEvento;
        }

        /**
         * Sets det evento.
         *
         * @param value the value
         */
        public void setDetEvento(TEvento.InfEvento.DetEvento value) {
            this.detEvento = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * The type Det evento.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "any"
        })
        public static class DetEvento {

            /**
             * The Any.
             */
            @XmlAnyElement
            protected Element any;
            /**
             * The Versao evento.
             */
            @XmlAttribute(name = "versaoEvento", required = true)
            protected String versaoEvento;

            /**
             * Gets any.
             *
             * @return the any
             */
            public Element getAny() {
                return any;
            }

            /**
             * Sets any.
             *
             * @param value the value
             */
            public void setAny(Element value) {
                this.any = value;
            }

            /**
             * Gets versao evento.
             *
             * @return the versao evento
             */
            public String getVersaoEvento() {
                return versaoEvento;
            }

            /**
             * Sets versao evento.
             *
             * @param value the value
             */
            public void setVersaoEvento(String value) {
                this.versaoEvento = value;
            }

        }

    }

}
