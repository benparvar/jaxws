//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 09:57:43 PM BRT 
//


package com.benparvar.cte.schema_300.proccteos;

import javax.xml.bind.annotation.*;


/**
 * The type Cte os proc.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "cTeOS",
        "protCTe"
})
@XmlRootElement(name = "cteOSProc", namespace = "http://www.portalfiscal.inf.br/cte")
public class CteOSProc {

    /**
     * The C te os.
     */
    @XmlElement(name = "CTeOS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TCTeOS cTeOS;
    /**
     * The Prot c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TProtCTeOS protCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;
    /**
     * The Ip transmissor.
     */
    @XmlAttribute(name = "ipTransmissor")
    protected String ipTransmissor;

    /**
     * Gets c te os.
     *
     * @return the c te os
     */
    public TCTeOS getCTeOS() {
        return cTeOS;
    }

    /**
     * Sets c te os.
     *
     * @param value the value
     */
    public void setCTeOS(TCTeOS value) {
        this.cTeOS = value;
    }

    /**
     * Gets prot c te.
     *
     * @return the prot c te
     */
    public TProtCTeOS getProtCTe() {
        return protCTe;
    }

    /**
     * Sets prot c te.
     *
     * @param value the value
     */
    public void setProtCTe(TProtCTeOS value) {
        this.protCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

    /**
     * Gets ip transmissor.
     *
     * @return the ip transmissor
     */
    public String getIpTransmissor() {
        return ipTransmissor;
    }

    /**
     * Sets ip transmissor.
     *
     * @param value the value
     */
    public void setIpTransmissor(String value) {
        this.ipTransmissor = value;
    }

}
