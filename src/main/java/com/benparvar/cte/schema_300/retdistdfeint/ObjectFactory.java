package com.benparvar.cte.schema_300.retdistdfeint;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create ret dist d fe int ret dist d fe int.
     *
     * @return the ret dist d fe int
     */
    public RetDistDFeInt createRetDistDFeInt() {
        return new RetDistDFeInt();
    }

    /**
     * Create ret dist d fe int lote dist d fe int ret dist d fe int . lote dist d fe int.
     *
     * @return the ret dist d fe int . lote dist d fe int
     */
    public RetDistDFeInt.LoteDistDFeInt createRetDistDFeIntLoteDistDFeInt() {
        return new RetDistDFeInt.LoteDistDFeInt();
    }

    /**
     * Create ret dist d fe int lote dist d fe int doc zip ret dist d fe int . lote dist d fe int . doc zip.
     *
     * @return the ret dist d fe int . lote dist d fe int . doc zip
     */
    public RetDistDFeInt.LoteDistDFeInt.DocZip createRetDistDFeIntLoteDistDFeIntDocZip() {
        return new RetDistDFeInt.LoteDistDFeInt.DocZip();
    }

}
