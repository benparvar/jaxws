package com.benparvar.cte.schema_300.retdistdfeint;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Ret dist d fe int.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tpAmb",
        "verAplic",
        "cStat",
        "xMotivo",
        "dhResp",
        "ultNSU",
        "maxNSU",
        "loteDistDFeInt"
})
@XmlRootElement(name = "retDistDFeInt")
public class RetDistDFeInt {

    /**
     * The Tp amb.
     */
    @XmlElement(required = true)
    protected String tpAmb;
    /**
     * The Ver aplic.
     */
    @XmlElement(required = true)
    protected String verAplic;
    /**
     * The C stat.
     */
    @XmlElement(required = true)
    protected String cStat;
    /**
     * The X motivo.
     */
    @XmlElement(required = true)
    protected String xMotivo;
    /**
     * The Dh resp.
     */
    @XmlElement(required = true)
    protected String dhResp;
    /**
     * The Ult nsu.
     */
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String ultNSU;
    /**
     * The Max nsu.
     */
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String maxNSU;
    /**
     * The Lote dist d fe int.
     */
    protected LoteDistDFeInt loteDistDFeInt;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets ver aplic.
     *
     * @return the ver aplic
     */
    public String getVerAplic() {
        return verAplic;
    }

    /**
     * Sets ver aplic.
     *
     * @param value the value
     */
    public void setVerAplic(String value) {
        this.verAplic = value;
    }

    /**
     * Gets c stat.
     *
     * @return the c stat
     */
    public String getCStat() {
        return cStat;
    }

    /**
     * Sets c stat.
     *
     * @param value the value
     */
    public void setCStat(String value) {
        this.cStat = value;
    }

    /**
     * Gets x motivo.
     *
     * @return the x motivo
     */
    public String getXMotivo() {
        return xMotivo;
    }

    /**
     * Sets x motivo.
     *
     * @param value the value
     */
    public void setXMotivo(String value) {
        this.xMotivo = value;
    }

    /**
     * Gets dh resp.
     *
     * @return the dh resp
     */
    public String getDhResp() {
        return dhResp;
    }

    /**
     * Sets dh resp.
     *
     * @param value the value
     */
    public void setDhResp(String value) {
        this.dhResp = value;
    }

    /**
     * Gets ult nsu.
     *
     * @return the ult nsu
     */
    public String getUltNSU() {
        return ultNSU;
    }

    /**
     * Sets ult nsu.
     *
     * @param value the value
     */
    public void setUltNSU(String value) {
        this.ultNSU = value;
    }

    /**
     * Gets max nsu.
     *
     * @return the max nsu
     */
    public String getMaxNSU() {
        return maxNSU;
    }

    /**
     * Sets max nsu.
     *
     * @param value the value
     */
    public void setMaxNSU(String value) {
        this.maxNSU = value;
    }

    /**
     * Gets lote dist d fe int.
     *
     * @return the lote dist d fe int
     */
    public LoteDistDFeInt getLoteDistDFeInt() {
        return loteDistDFeInt;
    }

    /**
     * Sets lote dist d fe int.
     *
     * @param value the value
     */
    public void setLoteDistDFeInt(LoteDistDFeInt value) {
        this.loteDistDFeInt = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Lote dist d fe int.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "docZip"
    })
    public static class LoteDistDFeInt {

        /**
         * The Doc zip.
         */
        @XmlElement(required = true)
        protected List<DocZip> docZip;

        /**
         * Gets doc zip.
         *
         * @return the doc zip
         */
        public List<DocZip> getDocZip() {
            if (docZip == null) {
                docZip = new ArrayList<DocZip>();
            }
            return this.docZip;
        }


        /**
         * The type Doc zip.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        public static class DocZip {

            /**
             * The Value.
             */
            @XmlValue
            protected byte[] value;
            /**
             * The Nsu.
             */
            @XmlAttribute(name = "NSU", required = true)
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            protected String nsu;
            /**
             * The Schema.
             */
            @XmlAttribute(name = "schema", required = true)
            protected String schema;

            /**
             * Get value byte [ ].
             *
             * @return the byte [ ]
             */
            public byte[] getValue() {
                return value;
            }

            /**
             * Sets value.
             *
             * @param value the value
             */
            public void setValue(byte[] value) {
                this.value = value;
            }

            /**
             * Gets nsu.
             *
             * @return the nsu
             */
            public String getNSU() {
                return nsu;
            }

            /**
             * Sets nsu.
             *
             * @param value the value
             */
            public void setNSU(String value) {
                this.nsu = value;
            }

            /**
             * Gets schema.
             *
             * @return the schema
             */
            public String getSchema() {
                return schema;
            }

            /**
             * Sets schema.
             *
             * @param value the value
             */
            public void setSchema(String value) {
                this.schema = value;
            }

        }

    }

}
