//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:51 PM BRT 
//


package com.benparvar.cte.schema_300.inutcte;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InutCTe_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "inutCTe");
    private final static QName _Signature_QNAME = new QName("http://www.w3.org/2000/09/xmldsig#", "Signature");

    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create reference type reference type.
     *
     * @return the reference type
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create signed info type signed info type.
     *
     * @return the signed info type
     */
    public SignedInfoType createSignedInfoType() {
        return new SignedInfoType();
    }

    /**
     * Create t ret inut c te t ret inut c te.
     *
     * @return the t ret inut c te
     */
    public TRetInutCTe createTRetInutCTe() {
        return new TRetInutCTe();
    }

    /**
     * Create t inut c te t inut c te.
     *
     * @return the t inut c te
     */
    public TInutCTe createTInutCTe() {
        return new TInutCTe();
    }

    /**
     * Create t proc inut c te t proc inut c te.
     *
     * @return the t proc inut c te
     */
    public TProcInutCTe createTProcInutCTe() {
        return new TProcInutCTe();
    }

    /**
     * Create signature type signature type.
     *
     * @return the signature type
     */
    public SignatureType createSignatureType() {
        return new SignatureType();
    }

    /**
     * Create x 509 data type x 509 data type.
     *
     * @return the x 509 data type
     */
    public X509DataType createX509DataType() {
        return new X509DataType();
    }

    /**
     * Create signature value type signature value type.
     *
     * @return the signature value type
     */
    public SignatureValueType createSignatureValueType() {
        return new SignatureValueType();
    }

    /**
     * Create transforms type transforms type.
     *
     * @return the transforms type
     */
    public TransformsType createTransformsType() {
        return new TransformsType();
    }

    /**
     * Create transform type transform type.
     *
     * @return the transform type
     */
    public TransformType createTransformType() {
        return new TransformType();
    }

    /**
     * Create key info type key info type.
     *
     * @return the key info type
     */
    public KeyInfoType createKeyInfoType() {
        return new KeyInfoType();
    }

    /**
     * Create reference type digest method reference type . digest method.
     *
     * @return the reference type . digest method
     */
    public ReferenceType.DigestMethod createReferenceTypeDigestMethod() {
        return new ReferenceType.DigestMethod();
    }

    /**
     * Create signed info type canonicalization method signed info type . canonicalization method.
     *
     * @return the signed info type . canonicalization method
     */
    public SignedInfoType.CanonicalizationMethod createSignedInfoTypeCanonicalizationMethod() {
        return new SignedInfoType.CanonicalizationMethod();
    }

    /**
     * Create signed info type signature method signed info type . signature method.
     *
     * @return the signed info type . signature method
     */
    public SignedInfoType.SignatureMethod createSignedInfoTypeSignatureMethod() {
        return new SignedInfoType.SignatureMethod();
    }

    /**
     * Create t ret inut c te inf inut t ret inut c te . inf inut.
     *
     * @return the t ret inut c te . inf inut
     */
    public TRetInutCTe.InfInut createTRetInutCTeInfInut() {
        return new TRetInutCTe.InfInut();
    }

    /**
     * Create t inut c te inf inut t inut c te . inf inut.
     *
     * @return the t inut c te . inf inut
     */
    public TInutCTe.InfInut createTInutCTeInfInut() {
        return new TInutCTe.InfInut();
    }

    /**
     * Create inut c te jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "inutCTe")
    public JAXBElement<TInutCTe> createInutCTe(TInutCTe value) {
        return new JAXBElement<TInutCTe>(_InutCTe_QNAME, TInutCTe.class, null, value);
    }

    /**
     * Create signature jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2000/09/xmldsig#", name = "Signature")
    public JAXBElement<SignatureType> createSignature(SignatureType value) {
        return new JAXBElement<SignatureType>(_Signature_QNAME, SignatureType.class, null, value);
    }

}
