//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:51 PM BRT 
//


package com.benparvar.cte.schema_300.inutcte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type T inut c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TInutCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infInut",
        "signature"
})
public class TInutCTe {

    /**
     * The Inf inut.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TInutCTe.InfInut infInut;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf inut.
     *
     * @return the inf inut
     */
    public TInutCTe.InfInut getInfInut() {
        return infInut;
    }

    /**
     * Sets inf inut.
     *
     * @param value the value
     */
    public void setInfInut(TInutCTe.InfInut value) {
        this.infInut = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf inut.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "tpAmb",
            "xServ",
            "cuf",
            "ano",
            "cnpj",
            "mod",
            "serie",
            "nctIni",
            "nctFin",
            "xJust"
    })
    public static class InfInut {

        /**
         * The Tp amb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpAmb;
        /**
         * The X serv.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xServ;
        /**
         * The Cuf.
         */
        @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cuf;
        /**
         * The Ano.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected short ano;
        /**
         * The Cnpj.
         */
        @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cnpj;
        /**
         * The Mod.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String mod;
        /**
         * The Serie.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String serie;
        /**
         * The Nct ini.
         */
        @XmlElement(name = "nCTIni", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nctIni;
        /**
         * The Nct fin.
         */
        @XmlElement(name = "nCTFin", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nctFin;
        /**
         * The X just.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xJust;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;

        /**
         * Gets tp amb.
         *
         * @return the tp amb
         */
        public String getTpAmb() {
            return tpAmb;
        }

        /**
         * Sets tp amb.
         *
         * @param value the value
         */
        public void setTpAmb(String value) {
            this.tpAmb = value;
        }

        /**
         * Gets x serv.
         *
         * @return the x serv
         */
        public String getXServ() {
            return xServ;
        }

        /**
         * Sets x serv.
         *
         * @param value the value
         */
        public void setXServ(String value) {
            this.xServ = value;
        }

        /**
         * Gets cuf.
         *
         * @return the cuf
         */
        public String getCUF() {
            return cuf;
        }

        /**
         * Sets cuf.
         *
         * @param value the value
         */
        public void setCUF(String value) {
            this.cuf = value;
        }

        /**
         * Gets ano.
         *
         * @return the ano
         */
        public short getAno() {
            return ano;
        }

        /**
         * Sets ano.
         *
         * @param value the value
         */
        public void setAno(short value) {
            this.ano = value;
        }

        /**
         * Gets cnpj.
         *
         * @return the cnpj
         */
        public String getCNPJ() {
            return cnpj;
        }

        /**
         * Sets cnpj.
         *
         * @param value the value
         */
        public void setCNPJ(String value) {
            this.cnpj = value;
        }

        /**
         * Gets mod.
         *
         * @return the mod
         */
        public String getMod() {
            return mod;
        }

        /**
         * Sets mod.
         *
         * @param value the value
         */
        public void setMod(String value) {
            this.mod = value;
        }

        /**
         * Gets serie.
         *
         * @return the serie
         */
        public String getSerie() {
            return serie;
        }

        /**
         * Sets serie.
         *
         * @param value the value
         */
        public void setSerie(String value) {
            this.serie = value;
        }

        /**
         * Gets nct ini.
         *
         * @return the nct ini
         */
        public String getNCTIni() {
            return nctIni;
        }

        /**
         * Sets nct ini.
         *
         * @param value the value
         */
        public void setNCTIni(String value) {
            this.nctIni = value;
        }

        /**
         * Gets nct fin.
         *
         * @return the nct fin
         */
        public String getNCTFin() {
            return nctFin;
        }

        /**
         * Sets nct fin.
         *
         * @param value the value
         */
        public void setNCTFin(String value) {
            this.nctFin = value;
        }

        /**
         * Gets x just.
         *
         * @return the x just
         */
        public String getXJust() {
            return xJust;
        }

        /**
         * Sets x just.
         *
         * @param value the value
         */
        public void setXJust(String value) {
            this.xJust = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }

    }

}
