//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:51 PM BRT 
//


package com.benparvar.cte.schema_300.inutcte;

import javax.xml.bind.annotation.*;


/**
 * The type T proc inut c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProcInutCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "inutCTe",
        "retInutCTe"
})
public class TProcInutCTe {

    /**
     * The Inut c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TInutCTe inutCTe;
    /**
     * The Ret inut c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TRetInutCTe retInutCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;
    /**
     * The Ip transmissor.
     */
    @XmlAttribute(name = "ipTransmissor")
    protected String ipTransmissor;

    /**
     * Gets inut c te.
     *
     * @return the inut c te
     */
    public TInutCTe getInutCTe() {
        return inutCTe;
    }

    /**
     * Sets inut c te.
     *
     * @param value the value
     */
    public void setInutCTe(TInutCTe value) {
        this.inutCTe = value;
    }

    /**
     * Gets ret inut c te.
     *
     * @return the ret inut c te
     */
    public TRetInutCTe getRetInutCTe() {
        return retInutCTe;
    }

    /**
     * Sets ret inut c te.
     *
     * @param value the value
     */
    public void setRetInutCTe(TRetInutCTe value) {
        this.retInutCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

    /**
     * Gets ip transmissor.
     *
     * @return the ip transmissor
     */
    public String getIpTransmissor() {
        return ipTransmissor;
    }

    /**
     * Sets ip transmissor.
     *
     * @param value the value
     */
    public void setIpTransmissor(String value) {
        this.ipTransmissor = value;
    }

}
