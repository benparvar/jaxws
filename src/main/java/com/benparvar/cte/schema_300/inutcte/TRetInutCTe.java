//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:50:51 PM BRT 
//


package com.benparvar.cte.schema_300.inutcte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type T ret inut c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetInutCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infInut",
        "signature"
})
public class TRetInutCTe {

    /**
     * The Inf inut.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TRetInutCTe.InfInut infInut;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf inut.
     *
     * @return the inf inut
     */
    public TRetInutCTe.InfInut getInfInut() {
        return infInut;
    }

    /**
     * Sets inf inut.
     *
     * @param value the value
     */
    public void setInfInut(TRetInutCTe.InfInut value) {
        this.infInut = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf inut.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "tpAmb",
            "verAplic",
            "cStat",
            "xMotivo",
            "cuf",
            "ano",
            "cnpj",
            "mod",
            "serie",
            "nctIni",
            "nctFin",
            "dhRecbto",
            "nProt"
    })
    public static class InfInut {

        /**
         * The Tp amb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpAmb;
        /**
         * The Ver aplic.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String verAplic;
        /**
         * The C stat.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cStat;
        /**
         * The X motivo.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xMotivo;
        /**
         * The Cuf.
         */
        @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cuf;
        /**
         * The Ano.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected Short ano;
        /**
         * The Cnpj.
         */
        @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
        protected String cnpj;
        /**
         * The Mod.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String mod;
        /**
         * The Serie.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String serie;
        /**
         * The Nct ini.
         */
        @XmlElement(name = "nCTIni", namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nctIni;
        /**
         * The Nct fin.
         */
        @XmlElement(name = "nCTFin", namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nctFin;
        /**
         * The Dh recbto.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String dhRecbto;
        /**
         * The N prot.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nProt;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        @XmlSchemaType(name = "ID")
        protected String id;

        /**
         * Gets tp amb.
         *
         * @return the tp amb
         */
        public String getTpAmb() {
            return tpAmb;
        }

        /**
         * Sets tp amb.
         *
         * @param value the value
         */
        public void setTpAmb(String value) {
            this.tpAmb = value;
        }

        /**
         * Gets ver aplic.
         *
         * @return the ver aplic
         */
        public String getVerAplic() {
            return verAplic;
        }

        /**
         * Sets ver aplic.
         *
         * @param value the value
         */
        public void setVerAplic(String value) {
            this.verAplic = value;
        }

        /**
         * Gets c stat.
         *
         * @return the c stat
         */
        public String getCStat() {
            return cStat;
        }

        /**
         * Sets c stat.
         *
         * @param value the value
         */
        public void setCStat(String value) {
            this.cStat = value;
        }

        /**
         * Gets x motivo.
         *
         * @return the x motivo
         */
        public String getXMotivo() {
            return xMotivo;
        }

        /**
         * Sets x motivo.
         *
         * @param value the value
         */
        public void setXMotivo(String value) {
            this.xMotivo = value;
        }

        /**
         * Gets cuf.
         *
         * @return the cuf
         */
        public String getCUF() {
            return cuf;
        }

        /**
         * Sets cuf.
         *
         * @param value the value
         */
        public void setCUF(String value) {
            this.cuf = value;
        }

        /**
         * Gets ano.
         *
         * @return the ano
         */
        public Short getAno() {
            return ano;
        }

        /**
         * Sets ano.
         *
         * @param value the value
         */
        public void setAno(Short value) {
            this.ano = value;
        }

        /**
         * Gets cnpj.
         *
         * @return the cnpj
         */
        public String getCNPJ() {
            return cnpj;
        }

        /**
         * Sets cnpj.
         *
         * @param value the value
         */
        public void setCNPJ(String value) {
            this.cnpj = value;
        }

        /**
         * Gets mod.
         *
         * @return the mod
         */
        public String getMod() {
            return mod;
        }

        /**
         * Sets mod.
         *
         * @param value the value
         */
        public void setMod(String value) {
            this.mod = value;
        }

        /**
         * Gets serie.
         *
         * @return the serie
         */
        public String getSerie() {
            return serie;
        }

        /**
         * Sets serie.
         *
         * @param value the value
         */
        public void setSerie(String value) {
            this.serie = value;
        }

        /**
         * Gets nct ini.
         *
         * @return the nct ini
         */
        public String getNCTIni() {
            return nctIni;
        }

        /**
         * Sets nct ini.
         *
         * @param value the value
         */
        public void setNCTIni(String value) {
            this.nctIni = value;
        }

        /**
         * Gets nct fin.
         *
         * @return the nct fin
         */
        public String getNCTFin() {
            return nctFin;
        }

        /**
         * Sets nct fin.
         *
         * @param value the value
         */
        public void setNCTFin(String value) {
            this.nctFin = value;
        }

        /**
         * Gets dh recbto.
         *
         * @return the dh recbto
         */
        public String getDhRecbto() {
            return dhRecbto;
        }

        /**
         * Sets dh recbto.
         *
         * @param value the value
         */
        public void setDhRecbto(String value) {
            this.dhRecbto = value;
        }

        /**
         * Gets n prot.
         *
         * @return the n prot
         */
        public String getNProt() {
            return nProt;
        }

        /**
         * Sets n prot.
         *
         * @param value the value
         */
        public void setNProt(String value) {
            this.nProt = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }

    }

}
