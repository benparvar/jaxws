//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:39:18 PM BRT 
//


package com.benparvar.cte.schema_300.consrecicte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type Reference type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "transforms",
        "digestMethod",
        "digestValue"
})
public class ReferenceType {

    /**
     * The Transforms.
     */
    @XmlElement(name = "Transforms", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected TransformsType transforms;
    /**
     * The Digest method.
     */
    @XmlElement(name = "DigestMethod", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected ReferenceType.DigestMethod digestMethod;
    /**
     * The Digest value.
     */
    @XmlElement(name = "DigestValue", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected byte[] digestValue;
    /**
     * The Id.
     */
    @XmlAttribute(name = "Id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    /**
     * The Uri.
     */
    @XmlAttribute(name = "URI", required = true)
    protected String uri;
    /**
     * The Type.
     */
    @XmlAttribute(name = "Type")
    @XmlSchemaType(name = "anyURI")
    protected String type;

    /**
     * Gets transforms.
     *
     * @return the transforms
     */
    public TransformsType getTransforms() {
        return transforms;
    }

    /**
     * Sets transforms.
     *
     * @param value the value
     */
    public void setTransforms(TransformsType value) {
        this.transforms = value;
    }

    /**
     * Gets digest method.
     *
     * @return the digest method
     */
    public ReferenceType.DigestMethod getDigestMethod() {
        return digestMethod;
    }

    /**
     * Sets digest method.
     *
     * @param value the value
     */
    public void setDigestMethod(ReferenceType.DigestMethod value) {
        this.digestMethod = value;
    }

    /**
     * Get digest value byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getDigestValue() {
        return digestValue;
    }

    /**
     * Sets digest value.
     *
     * @param value the value
     */
    public void setDigestValue(byte[] value) {
        this.digestValue = value;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param value the value
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets uri.
     *
     * @return the uri
     */
    public String getURI() {
        return uri;
    }

    /**
     * Sets uri.
     *
     * @param value the value
     */
    public void setURI(String value) {
        this.uri = value;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param value the value
     */
    public void setType(String value) {
        this.type = value;
    }


    /**
     * The type Digest method.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DigestMethod {

        /**
         * The Algorithm.
         */
        @XmlAttribute(name = "Algorithm", required = true)
        @XmlSchemaType(name = "anyURI")
        protected String algorithm;

        /**
         * Gets algorithm.
         *
         * @return the algorithm
         */
        public String getAlgorithm() {
            if (algorithm == null) {
                return "http://www.w3.org/2000/09/xmldsig#sha1";
            } else {
                return algorithm;
            }
        }

        /**
         * Sets algorithm.
         *
         * @param value the value
         */
        public void setAlgorithm(String value) {
            this.algorithm = value;
        }

    }

}
