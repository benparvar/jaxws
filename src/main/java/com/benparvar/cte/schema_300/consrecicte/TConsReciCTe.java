//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:39:18 PM BRT 
//


package com.benparvar.cte.schema_300.consrecicte;

import javax.xml.bind.annotation.*;


/**
 * The type T cons reci c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TConsReciCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "nRec"
})
public class TConsReciCTe {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The N rec.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String nRec;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets n rec.
     *
     * @return the n rec
     */
    public String getNRec() {
        return nRec;
    }

    /**
     * Sets n rec.
     *
     * @param value the value
     */
    public void setNRec(String value) {
        this.nRec = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
