//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:39:18 PM BRT 
//


package com.benparvar.cte.schema_300.consrecicte;

import javax.xml.bind.annotation.*;


/**
 * The type T local.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TLocal", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "cMun",
        "xMun",
        "uf"
})
public class TLocal {

    /**
     * The C mun.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cMun;
    /**
     * The X mun.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMun;
    /**
     * The Uf.
     */
    @XmlElement(name = "UF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    @XmlSchemaType(name = "string")
    protected TUf uf;

    /**
     * Gets c mun.
     *
     * @return the c mun
     */
    public String getCMun() {
        return cMun;
    }

    /**
     * Sets c mun.
     *
     * @param value the value
     */
    public void setCMun(String value) {
        this.cMun = value;
    }

    /**
     * Gets x mun.
     *
     * @return the x mun
     */
    public String getXMun() {
        return xMun;
    }

    /**
     * Sets x mun.
     *
     * @param value the value
     */
    public void setXMun(String value) {
        this.xMun = value;
    }

    /**
     * Gets uf.
     *
     * @return the uf
     */
    public TUf getUF() {
        return uf;
    }

    /**
     * Sets uf.
     *
     * @param value the value
     */
    public void setUF(TUf value) {
        this.uf = value;
    }

}
