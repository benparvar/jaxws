//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:39:18 PM BRT 
//


package com.benparvar.cte.schema_300.consrecicte;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The type T ret cons reci c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetConsReciCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "verAplic",
        "nRec",
        "cStat",
        "xMotivo",
        "cuf",
        "protCTe"
})
public class TRetConsReciCTe {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The Ver aplic.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String verAplic;
    /**
     * The N rec.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String nRec;
    /**
     * The C stat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cStat;
    /**
     * The X motivo.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMotivo;
    /**
     * The Cuf.
     */
    @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cuf;
    /**
     * The Prot c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<TProtCTe> protCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets ver aplic.
     *
     * @return the ver aplic
     */
    public String getVerAplic() {
        return verAplic;
    }

    /**
     * Sets ver aplic.
     *
     * @param value the value
     */
    public void setVerAplic(String value) {
        this.verAplic = value;
    }

    /**
     * Gets n rec.
     *
     * @return the n rec
     */
    public String getNRec() {
        return nRec;
    }

    /**
     * Sets n rec.
     *
     * @param value the value
     */
    public void setNRec(String value) {
        this.nRec = value;
    }

    /**
     * Gets c stat.
     *
     * @return the c stat
     */
    public String getCStat() {
        return cStat;
    }

    /**
     * Sets c stat.
     *
     * @param value the value
     */
    public void setCStat(String value) {
        this.cStat = value;
    }

    /**
     * Gets x motivo.
     *
     * @return the x motivo
     */
    public String getXMotivo() {
        return xMotivo;
    }

    /**
     * Sets x motivo.
     *
     * @param value the value
     */
    public void setXMotivo(String value) {
        this.xMotivo = value;
    }

    /**
     * Gets cuf.
     *
     * @return the cuf
     */
    public String getCUF() {
        return cuf;
    }

    /**
     * Sets cuf.
     *
     * @param value the value
     */
    public void setCUF(String value) {
        this.cuf = value;
    }

    /**
     * Gets prot c te.
     *
     * @return the prot c te
     */
    public List<TProtCTe> getProtCTe() {
        if (protCTe == null) {
            protCTe = new ArrayList<TProtCTe>();
        }
        return this.protCTe;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
