//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Signature_QNAME = new QName("http://www.w3.org/2000/09/xmldsig#", "Signature");
    private final static QName _RetConsReciCTe_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "retConsReciCTe");

    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create reference type reference type.
     *
     * @return the reference type
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create signed info type signed info type.
     *
     * @return the signed info type
     */
    public SignedInfoType createSignedInfoType() {
        return new SignedInfoType();
    }

    /**
     * Create t imp t imp.
     *
     * @return the t imp
     */
    public TImp createTImp() {
        return new TImp();
    }

    /**
     * Create t prot c te t prot c te.
     *
     * @return the t prot c te
     */
    public TProtCTe createTProtCTe() {
        return new TProtCTe();
    }

    /**
     * Create tc te tc te.
     *
     * @return the tc te
     */
    public TCTe createTCTe() {
        return new TCTe();
    }

    /**
     * Create tc te inf cte tc te . inf cte.
     *
     * @return the tc te . inf cte
     */
    public TCTe.InfCte createTCTeInfCte() {
        return new TCTe.InfCte();
    }

    /**
     * Create tc te inf cte inf c te norm tc te . inf cte . inf c te norm.
     *
     * @return the tc te . inf cte . inf c te norm
     */
    public TCTe.InfCte.InfCTeNorm createTCTeInfCteInfCTeNorm() {
        return new TCTe.InfCte.InfCTeNorm();
    }

    /**
     * Create tc te inf cte inf c te norm inf serv vinc tc te . inf cte . inf c te norm . inf serv vinc.
     *
     * @return the tc te . inf cte . inf c te norm . inf serv vinc
     */
    public TCTe.InfCte.InfCTeNorm.InfServVinc createTCTeInfCteInfCTeNormInfServVinc() {
        return new TCTe.InfCte.InfCTeNorm.InfServVinc();
    }

    /**
     * Create tc te inf cte inf c te norm inf cte sub tc te . inf cte . inf c te norm . inf cte sub.
     *
     * @return the tc te . inf cte . inf c te norm . inf cte sub
     */
    public TCTe.InfCte.InfCTeNorm.InfCteSub createTCTeInfCteInfCTeNormInfCteSub() {
        return new TCTe.InfCte.InfCTeNorm.InfCteSub();
    }

    /**
     * Create tc te inf cte inf c te norm inf cte sub toma icms tc te . inf cte . inf c te norm . inf cte sub . toma icms.
     *
     * @return the tc te . inf cte . inf c te norm . inf cte sub . toma icms
     */
    public TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS createTCTeInfCteInfCTeNormInfCteSubTomaICMS() {
        return new TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS();
    }

    /**
     * Create tc te inf cte inf c te norm cobr tc te . inf cte . inf c te norm . cobr.
     *
     * @return the tc te . inf cte . inf c te norm . cobr
     */
    public TCTe.InfCte.InfCTeNorm.Cobr createTCTeInfCteInfCTeNormCobr() {
        return new TCTe.InfCte.InfCTeNorm.Cobr();
    }

    /**
     * Create tc te inf cte inf c te norm doc ant tc te . inf cte . inf c te norm . doc ant.
     *
     * @return the tc te . inf cte . inf c te norm . doc ant
     */
    public TCTe.InfCte.InfCTeNorm.DocAnt createTCTeInfCteInfCTeNormDocAnt() {
        return new TCTe.InfCte.InfCTeNorm.DocAnt();
    }

    /**
     * Create tc te inf cte inf c te norm doc ant emi doc ant tc te . inf cte . inf c te norm . doc ant . emi doc ant.
     *
     * @return the tc te . inf cte . inf c te norm . doc ant . emi doc ant
     */
    public TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt createTCTeInfCteInfCTeNormDocAntEmiDocAnt() {
        return new TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt();
    }

    /**
     * Create tc te inf cte inf c te norm doc ant emi doc ant id doc ant tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant.
     *
     * @return the tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant
     */
    public TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt createTCTeInfCteInfCTeNormDocAntEmiDocAntIdDocAnt() {
        return new TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt();
    }

    /**
     * Create tc te inf cte inf c te norm inf doc tc te . inf cte . inf c te norm . inf doc.
     *
     * @return the tc te . inf cte . inf c te norm . inf doc
     */
    public TCTe.InfCte.InfCTeNorm.InfDoc createTCTeInfCteInfCTeNormInfDoc() {
        return new TCTe.InfCte.InfCTeNorm.InfDoc();
    }

    /**
     * Create tc te inf cte inf c te norm inf carga tc te . inf cte . inf c te norm . inf carga.
     *
     * @return the tc te . inf cte . inf c te norm . inf carga
     */
    public TCTe.InfCte.InfCTeNorm.InfCarga createTCTeInfCteInfCTeNormInfCarga() {
        return new TCTe.InfCte.InfCTeNorm.InfCarga();
    }

    /**
     * Create tc te inf cte imp tc te . inf cte . imp.
     *
     * @return the tc te . inf cte . imp
     */
    public TCTe.InfCte.Imp createTCTeInfCteImp() {
        return new TCTe.InfCte.Imp();
    }

    /**
     * Create tc te inf cte v prest tc te . inf cte . v prest.
     *
     * @return the tc te . inf cte . v prest
     */
    public TCTe.InfCte.VPrest createTCTeInfCteVPrest() {
        return new TCTe.InfCte.VPrest();
    }

    /**
     * Create tc te inf cte compl tc te . inf cte . compl.
     *
     * @return the tc te . inf cte . compl
     */
    public TCTe.InfCte.Compl createTCTeInfCteCompl() {
        return new TCTe.InfCte.Compl();
    }

    /**
     * Create tc te inf cte compl entrega tc te . inf cte . compl . entrega.
     *
     * @return the tc te . inf cte . compl . entrega
     */
    public TCTe.InfCte.Compl.Entrega createTCTeInfCteComplEntrega() {
        return new TCTe.InfCte.Compl.Entrega();
    }

    /**
     * Create tc te inf cte compl fluxo tc te . inf cte . compl . fluxo.
     *
     * @return the tc te . inf cte . compl . fluxo
     */
    public TCTe.InfCte.Compl.Fluxo createTCTeInfCteComplFluxo() {
        return new TCTe.InfCte.Compl.Fluxo();
    }

    /**
     * Create tc te inf cte ide tc te . inf cte . ide.
     *
     * @return the tc te . inf cte . ide
     */
    public TCTe.InfCte.Ide createTCTeInfCteIde() {
        return new TCTe.InfCte.Ide();
    }

    /**
     * Create t unidade transp t unidade transp.
     *
     * @return the t unidade transp
     */
    public TUnidadeTransp createTUnidadeTransp() {
        return new TUnidadeTransp();
    }

    /**
     * Create t imp os t imp os.
     *
     * @return the t imp os
     */
    public TImpOS createTImpOS() {
        return new TImpOS();
    }

    /**
     * Create t unid carga t unid carga.
     *
     * @return the t unid carga
     */
    public TUnidCarga createTUnidCarga() {
        return new TUnidCarga();
    }

    /**
     * Create t prot c te os t prot c te os.
     *
     * @return the t prot c te os
     */
    public TProtCTeOS createTProtCTeOS() {
        return new TProtCTeOS();
    }

    /**
     * Create tc te os tc te os.
     *
     * @return the tc te os
     */
    public TCTeOS createTCTeOS() {
        return new TCTeOS();
    }

    /**
     * Create tc te os inf cte tc te os . inf cte.
     *
     * @return the tc te os . inf cte
     */
    public TCTeOS.InfCte createTCTeOSInfCte() {
        return new TCTeOS.InfCte();
    }

    /**
     * Create tc te os inf cte inf c te norm tc te os . inf cte . inf c te norm.
     *
     * @return the tc te os . inf cte . inf c te norm
     */
    public TCTeOS.InfCte.InfCTeNorm createTCTeOSInfCteInfCTeNorm() {
        return new TCTeOS.InfCte.InfCTeNorm();
    }

    /**
     * Create tc te os inf cte inf c te norm cobr tc te os . inf cte . inf c te norm . cobr.
     *
     * @return the tc te os . inf cte . inf c te norm . cobr
     */
    public TCTeOS.InfCte.InfCTeNorm.Cobr createTCTeOSInfCteInfCTeNormCobr() {
        return new TCTeOS.InfCte.InfCTeNorm.Cobr();
    }

    /**
     * Create tc te os inf cte inf c te norm inf cte sub tc te os . inf cte . inf c te norm . inf cte sub.
     *
     * @return the tc te os . inf cte . inf c te norm . inf cte sub
     */
    public TCTeOS.InfCte.InfCTeNorm.InfCteSub createTCTeOSInfCteInfCTeNormInfCteSub() {
        return new TCTeOS.InfCte.InfCTeNorm.InfCteSub();
    }

    /**
     * Create tc te os inf cte inf c te norm inf cte sub toma icms tc te os . inf cte . inf c te norm . inf cte sub . toma icms.
     *
     * @return the tc te os . inf cte . inf c te norm . inf cte sub . toma icms
     */
    public TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS createTCTeOSInfCteInfCTeNormInfCteSubTomaICMS() {
        return new TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS();
    }

    /**
     * Create tc te os inf cte inf c te norm inf servico tc te os . inf cte . inf c te norm . inf servico.
     *
     * @return the tc te os . inf cte . inf c te norm . inf servico
     */
    public TCTeOS.InfCte.InfCTeNorm.InfServico createTCTeOSInfCteInfCTeNormInfServico() {
        return new TCTeOS.InfCte.InfCTeNorm.InfServico();
    }

    /**
     * Create tc te os inf cte imp tc te os . inf cte . imp.
     *
     * @return the tc te os . inf cte . imp
     */
    public TCTeOS.InfCte.Imp createTCTeOSInfCteImp() {
        return new TCTeOS.InfCte.Imp();
    }

    /**
     * Create tc te os inf cte v prest tc te os . inf cte . v prest.
     *
     * @return the tc te os . inf cte . v prest
     */
    public TCTeOS.InfCte.VPrest createTCTeOSInfCteVPrest() {
        return new TCTeOS.InfCte.VPrest();
    }

    /**
     * Create tc te os inf cte compl tc te os . inf cte . compl.
     *
     * @return the tc te os . inf cte . compl
     */
    public TCTeOS.InfCte.Compl createTCTeOSInfCteCompl() {
        return new TCTeOS.InfCte.Compl();
    }

    /**
     * Create tc te os inf cte ide tc te os . inf cte . ide.
     *
     * @return the tc te os . inf cte . ide
     */
    public TCTeOS.InfCte.Ide createTCTeOSInfCteIde() {
        return new TCTeOS.InfCte.Ide();
    }

    /**
     * Create t ret envi c te t ret envi c te.
     *
     * @return the t ret envi c te
     */
    public TRetEnviCTe createTRetEnviCTe() {
        return new TRetEnviCTe();
    }

    /**
     * Create t ret cons reci c te t ret cons reci c te.
     *
     * @return the t ret cons reci c te
     */
    public TRetConsReciCTe createTRetConsReciCTe() {
        return new TRetConsReciCTe();
    }

    /**
     * Create t resp tec t resp tec.
     *
     * @return the t resp tec
     */
    public TRespTec createTRespTec() {
        return new TRespTec();
    }

    /**
     * Create t endernac t endernac.
     *
     * @return the t endernac
     */
    public TEndernac createTEndernac() {
        return new TEndernac();
    }

    /**
     * Create t endereco t endereco.
     *
     * @return the t endereco
     */
    public TEndereco createTEndereco() {
        return new TEndereco();
    }

    /**
     * Create t local t local.
     *
     * @return the t local
     */
    public TLocal createTLocal() {
        return new TLocal();
    }

    /**
     * Create t envi c te t envi c te.
     *
     * @return the t envi c te
     */
    public TEnviCTe createTEnviCTe() {
        return new TEnviCTe();
    }

    /**
     * Create t ende emi t ende emi.
     *
     * @return the t ende emi
     */
    public TEndeEmi createTEndeEmi() {
        return new TEndeEmi();
    }

    /**
     * Create t ret c te os t ret c te os.
     *
     * @return the t ret c te os
     */
    public TRetCTeOS createTRetCTeOS() {
        return new TRetCTeOS();
    }

    /**
     * Create t ret c te t ret c te.
     *
     * @return the t ret c te
     */
    public TRetCTe createTRetCTe() {
        return new TRetCTe();
    }

    /**
     * Create t end org t end org.
     *
     * @return the t end org
     */
    public TEndOrg createTEndOrg() {
        return new TEndOrg();
    }

    /**
     * Create t end re ent t end re ent.
     *
     * @return the t end re ent
     */
    public TEndReEnt createTEndReEnt() {
        return new TEndReEnt();
    }

    /**
     * Create t cons reci c te t cons reci c te.
     *
     * @return the t cons reci c te
     */
    public TConsReciCTe createTConsReciCTe() {
        return new TConsReciCTe();
    }

    /**
     * Create signature type signature type.
     *
     * @return the signature type
     */
    public SignatureType createSignatureType() {
        return new SignatureType();
    }

    /**
     * Create x 509 data type x 509 data type.
     *
     * @return the x 509 data type
     */
    public X509DataType createX509DataType() {
        return new X509DataType();
    }

    /**
     * Create signature value type signature value type.
     *
     * @return the signature value type
     */
    public SignatureValueType createSignatureValueType() {
        return new SignatureValueType();
    }

    /**
     * Create transforms type transforms type.
     *
     * @return the transforms type
     */
    public TransformsType createTransformsType() {
        return new TransformsType();
    }

    /**
     * Create transform type transform type.
     *
     * @return the transform type
     */
    public TransformType createTransformType() {
        return new TransformType();
    }

    /**
     * Create key info type key info type.
     *
     * @return the key info type
     */
    public KeyInfoType createKeyInfoType() {
        return new KeyInfoType();
    }

    /**
     * Create reference type digest method reference type . digest method.
     *
     * @return the reference type . digest method
     */
    public ReferenceType.DigestMethod createReferenceTypeDigestMethod() {
        return new ReferenceType.DigestMethod();
    }

    /**
     * Create signed info type canonicalization method signed info type . canonicalization method.
     *
     * @return the signed info type . canonicalization method
     */
    public SignedInfoType.CanonicalizationMethod createSignedInfoTypeCanonicalizationMethod() {
        return new SignedInfoType.CanonicalizationMethod();
    }

    /**
     * Create signed info type signature method signed info type . signature method.
     *
     * @return the signed info type . signature method
     */
    public SignedInfoType.SignatureMethod createSignedInfoTypeSignatureMethod() {
        return new SignedInfoType.SignatureMethod();
    }

    /**
     * Create t imp icms 00 t imp . icms 00.
     *
     * @return the t imp . icms 00
     */
    public TImp.ICMS00 createTImpICMS00() {
        return new TImp.ICMS00();
    }

    /**
     * Create t imp icms 20 t imp . icms 20.
     *
     * @return the t imp . icms 20
     */
    public TImp.ICMS20 createTImpICMS20() {
        return new TImp.ICMS20();
    }

    /**
     * Create t imp icms 45 t imp . icms 45.
     *
     * @return the t imp . icms 45
     */
    public TImp.ICMS45 createTImpICMS45() {
        return new TImp.ICMS45();
    }

    /**
     * Create t imp icms 60 t imp . icms 60.
     *
     * @return the t imp . icms 60
     */
    public TImp.ICMS60 createTImpICMS60() {
        return new TImp.ICMS60();
    }

    /**
     * Create t imp icms 90 t imp . icms 90.
     *
     * @return the t imp . icms 90
     */
    public TImp.ICMS90 createTImpICMS90() {
        return new TImp.ICMS90();
    }

    /**
     * Create t imp icms outra uf t imp . icms outra uf.
     *
     * @return the t imp . icms outra uf
     */
    public TImp.ICMSOutraUF createTImpICMSOutraUF() {
        return new TImp.ICMSOutraUF();
    }

    /**
     * Create t imp icmssn t imp . icmssn.
     *
     * @return the t imp . icmssn
     */
    public TImp.ICMSSN createTImpICMSSN() {
        return new TImp.ICMSSN();
    }

    /**
     * Create t prot c te inf prot t prot c te . inf prot.
     *
     * @return the t prot c te . inf prot
     */
    public TProtCTe.InfProt createTProtCTeInfProt() {
        return new TProtCTe.InfProt();
    }

    /**
     * Create t prot c te inf fisco t prot c te . inf fisco.
     *
     * @return the t prot c te . inf fisco
     */
    public TProtCTe.InfFisco createTProtCTeInfFisco() {
        return new TProtCTe.InfFisco();
    }

    /**
     * Create tc te inf c te supl tc te . inf c te supl.
     *
     * @return the tc te . inf c te supl
     */
    public TCTe.InfCTeSupl createTCTeInfCTeSupl() {
        return new TCTe.InfCTeSupl();
    }

    /**
     * Create tc te inf cte emit tc te . inf cte . emit.
     *
     * @return the tc te . inf cte . emit
     */
    public TCTe.InfCte.Emit createTCTeInfCteEmit() {
        return new TCTe.InfCte.Emit();
    }

    /**
     * Create tc te inf cte rem tc te . inf cte . rem.
     *
     * @return the tc te . inf cte . rem
     */
    public TCTe.InfCte.Rem createTCTeInfCteRem() {
        return new TCTe.InfCte.Rem();
    }

    /**
     * Create tc te inf cte exped tc te . inf cte . exped.
     *
     * @return the tc te . inf cte . exped
     */
    public TCTe.InfCte.Exped createTCTeInfCteExped() {
        return new TCTe.InfCte.Exped();
    }

    /**
     * Create tc te inf cte receb tc te . inf cte . receb.
     *
     * @return the tc te . inf cte . receb
     */
    public TCTe.InfCte.Receb createTCTeInfCteReceb() {
        return new TCTe.InfCte.Receb();
    }

    /**
     * Create tc te inf cte dest tc te . inf cte . dest.
     *
     * @return the tc te . inf cte . dest
     */
    public TCTe.InfCte.Dest createTCTeInfCteDest() {
        return new TCTe.InfCte.Dest();
    }

    /**
     * Create tc te inf cte inf cte comp tc te . inf cte . inf cte comp.
     *
     * @return the tc te . inf cte . inf cte comp
     */
    public TCTe.InfCte.InfCteComp createTCTeInfCteInfCteComp() {
        return new TCTe.InfCte.InfCteComp();
    }

    /**
     * Create tc te inf cte inf cte anu tc te . inf cte . inf cte anu.
     *
     * @return the tc te . inf cte . inf cte anu
     */
    public TCTe.InfCte.InfCteAnu createTCTeInfCteInfCteAnu() {
        return new TCTe.InfCte.InfCteAnu();
    }

    /**
     * Create tc te inf cte aut xml tc te . inf cte . aut xml.
     *
     * @return the tc te . inf cte . aut xml
     */
    public TCTe.InfCte.AutXML createTCTeInfCteAutXML() {
        return new TCTe.InfCte.AutXML();
    }

    /**
     * Create tc te inf cte inf c te norm inf modal tc te . inf cte . inf c te norm . inf modal.
     *
     * @return the tc te . inf cte . inf c te norm . inf modal
     */
    public TCTe.InfCte.InfCTeNorm.InfModal createTCTeInfCteInfCTeNormInfModal() {
        return new TCTe.InfCte.InfCTeNorm.InfModal();
    }

    /**
     * Create tc te inf cte inf c te norm veic novos tc te . inf cte . inf c te norm . veic novos.
     *
     * @return the tc te . inf cte . inf c te norm . veic novos
     */
    public TCTe.InfCte.InfCTeNorm.VeicNovos createTCTeInfCteInfCTeNormVeicNovos() {
        return new TCTe.InfCte.InfCTeNorm.VeicNovos();
    }

    /**
     * Create tc te inf cte inf c te norm inf globalizado tc te . inf cte . inf c te norm . inf globalizado.
     *
     * @return the tc te . inf cte . inf c te norm . inf globalizado
     */
    public TCTe.InfCte.InfCTeNorm.InfGlobalizado createTCTeInfCteInfCTeNormInfGlobalizado() {
        return new TCTe.InfCte.InfCTeNorm.InfGlobalizado();
    }

    /**
     * Create tc te inf cte inf c te norm inf serv vinc inf c te multimodal tc te . inf cte . inf c te norm . inf serv vinc . inf c te multimodal.
     *
     * @return the tc te . inf cte . inf c te norm . inf serv vinc . inf c te multimodal
     */
    public TCTe.InfCte.InfCTeNorm.InfServVinc.InfCTeMultimodal createTCTeInfCteInfCTeNormInfServVincInfCTeMultimodal() {
        return new TCTe.InfCte.InfCTeNorm.InfServVinc.InfCTeMultimodal();
    }

    /**
     * Create tc te inf cte inf c te norm inf cte sub toma icms ref nf tc te . inf cte . inf c te norm . inf cte sub . toma icms . ref nf.
     *
     * @return the tc te . inf cte . inf c te norm . inf cte sub . toma icms . ref nf
     */
    public TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF createTCTeInfCteInfCTeNormInfCteSubTomaICMSRefNF() {
        return new TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF();
    }

    /**
     * Create tc te inf cte inf c te norm cobr fat tc te . inf cte . inf c te norm . cobr . fat.
     *
     * @return the tc te . inf cte . inf c te norm . cobr . fat
     */
    public TCTe.InfCte.InfCTeNorm.Cobr.Fat createTCTeInfCteInfCTeNormCobrFat() {
        return new TCTe.InfCte.InfCTeNorm.Cobr.Fat();
    }

    /**
     * Create tc te inf cte inf c te norm cobr dup tc te . inf cte . inf c te norm . cobr . dup.
     *
     * @return the tc te . inf cte . inf c te norm . cobr . dup
     */
    public TCTe.InfCte.InfCTeNorm.Cobr.Dup createTCTeInfCteInfCTeNormCobrDup() {
        return new TCTe.InfCte.InfCTeNorm.Cobr.Dup();
    }

    /**
     * Create tc te inf cte inf c te norm doc ant emi doc ant id doc ant id doc ant pap tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant . id doc ant pap.
     *
     * @return the tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant . id doc ant pap
     */
    public TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt.IdDocAntPap createTCTeInfCteInfCTeNormDocAntEmiDocAntIdDocAntIdDocAntPap() {
        return new TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt.IdDocAntPap();
    }

    /**
     * Create tc te inf cte inf c te norm doc ant emi doc ant id doc ant id doc ant ele tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant . id doc ant ele.
     *
     * @return the tc te . inf cte . inf c te norm . doc ant . emi doc ant . id doc ant . id doc ant ele
     */
    public TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt.IdDocAntEle createTCTeInfCteInfCTeNormDocAntEmiDocAntIdDocAntIdDocAntEle() {
        return new TCTe.InfCte.InfCTeNorm.DocAnt.EmiDocAnt.IdDocAnt.IdDocAntEle();
    }

    /**
     * Create tc te inf cte inf c te norm inf doc inf nf tc te . inf cte . inf c te norm . inf doc . inf nf.
     *
     * @return the tc te . inf cte . inf c te norm . inf doc . inf nf
     */
    public TCTe.InfCte.InfCTeNorm.InfDoc.InfNF createTCTeInfCteInfCTeNormInfDocInfNF() {
        return new TCTe.InfCte.InfCTeNorm.InfDoc.InfNF();
    }

    /**
     * Create tc te inf cte inf c te norm inf doc inf n fe tc te . inf cte . inf c te norm . inf doc . inf n fe.
     *
     * @return the tc te . inf cte . inf c te norm . inf doc . inf n fe
     */
    public TCTe.InfCte.InfCTeNorm.InfDoc.InfNFe createTCTeInfCteInfCTeNormInfDocInfNFe() {
        return new TCTe.InfCte.InfCTeNorm.InfDoc.InfNFe();
    }

    /**
     * Create tc te inf cte inf c te norm inf doc inf outros tc te . inf cte . inf c te norm . inf doc . inf outros.
     *
     * @return the tc te . inf cte . inf c te norm . inf doc . inf outros
     */
    public TCTe.InfCte.InfCTeNorm.InfDoc.InfOutros createTCTeInfCteInfCTeNormInfDocInfOutros() {
        return new TCTe.InfCte.InfCTeNorm.InfDoc.InfOutros();
    }

    /**
     * Create tc te inf cte inf c te norm inf carga inf q tc te . inf cte . inf c te norm . inf carga . inf q.
     *
     * @return the tc te . inf cte . inf c te norm . inf carga . inf q
     */
    public TCTe.InfCte.InfCTeNorm.InfCarga.InfQ createTCTeInfCteInfCTeNormInfCargaInfQ() {
        return new TCTe.InfCte.InfCTeNorm.InfCarga.InfQ();
    }

    /**
     * Create tc te inf cte imp icmsuf fim tc te . inf cte . imp . icmsuf fim.
     *
     * @return the tc te . inf cte . imp . icmsuf fim
     */
    public TCTe.InfCte.Imp.ICMSUFFim createTCTeInfCteImpICMSUFFim() {
        return new TCTe.InfCte.Imp.ICMSUFFim();
    }

    /**
     * Create tc te inf cte v prest comp tc te . inf cte . v prest . comp.
     *
     * @return the tc te . inf cte . v prest . comp
     */
    public TCTe.InfCte.VPrest.Comp createTCTeInfCteVPrestComp() {
        return new TCTe.InfCte.VPrest.Comp();
    }

    /**
     * Create tc te inf cte compl obs cont tc te . inf cte . compl . obs cont.
     *
     * @return the tc te . inf cte . compl . obs cont
     */
    public TCTe.InfCte.Compl.ObsCont createTCTeInfCteComplObsCont() {
        return new TCTe.InfCte.Compl.ObsCont();
    }

    /**
     * Create tc te inf cte compl obs fisco tc te . inf cte . compl . obs fisco.
     *
     * @return the tc te . inf cte . compl . obs fisco
     */
    public TCTe.InfCte.Compl.ObsFisco createTCTeInfCteComplObsFisco() {
        return new TCTe.InfCte.Compl.ObsFisco();
    }

    /**
     * Create tc te inf cte compl entrega sem data tc te . inf cte . compl . entrega . sem data.
     *
     * @return the tc te . inf cte . compl . entrega . sem data
     */
    public TCTe.InfCte.Compl.Entrega.SemData createTCTeInfCteComplEntregaSemData() {
        return new TCTe.InfCte.Compl.Entrega.SemData();
    }

    /**
     * Create tc te inf cte compl entrega com data tc te . inf cte . compl . entrega . com data.
     *
     * @return the tc te . inf cte . compl . entrega . com data
     */
    public TCTe.InfCte.Compl.Entrega.ComData createTCTeInfCteComplEntregaComData() {
        return new TCTe.InfCte.Compl.Entrega.ComData();
    }

    /**
     * Create tc te inf cte compl entrega no periodo tc te . inf cte . compl . entrega . no periodo.
     *
     * @return the tc te . inf cte . compl . entrega . no periodo
     */
    public TCTe.InfCte.Compl.Entrega.NoPeriodo createTCTeInfCteComplEntregaNoPeriodo() {
        return new TCTe.InfCte.Compl.Entrega.NoPeriodo();
    }

    /**
     * Create tc te inf cte compl entrega sem hora tc te . inf cte . compl . entrega . sem hora.
     *
     * @return the tc te . inf cte . compl . entrega . sem hora
     */
    public TCTe.InfCte.Compl.Entrega.SemHora createTCTeInfCteComplEntregaSemHora() {
        return new TCTe.InfCte.Compl.Entrega.SemHora();
    }

    /**
     * Create tc te inf cte compl entrega com hora tc te . inf cte . compl . entrega . com hora.
     *
     * @return the tc te . inf cte . compl . entrega . com hora
     */
    public TCTe.InfCte.Compl.Entrega.ComHora createTCTeInfCteComplEntregaComHora() {
        return new TCTe.InfCte.Compl.Entrega.ComHora();
    }

    /**
     * Create tc te inf cte compl entrega no inter tc te . inf cte . compl . entrega . no inter.
     *
     * @return the tc te . inf cte . compl . entrega . no inter
     */
    public TCTe.InfCte.Compl.Entrega.NoInter createTCTeInfCteComplEntregaNoInter() {
        return new TCTe.InfCte.Compl.Entrega.NoInter();
    }

    /**
     * Create tc te inf cte compl fluxo pass tc te . inf cte . compl . fluxo . pass.
     *
     * @return the tc te . inf cte . compl . fluxo . pass
     */
    public TCTe.InfCte.Compl.Fluxo.Pass createTCTeInfCteComplFluxoPass() {
        return new TCTe.InfCte.Compl.Fluxo.Pass();
    }

    /**
     * Create tc te inf cte ide toma 3 tc te . inf cte . ide . toma 3.
     *
     * @return the tc te . inf cte . ide . toma 3
     */
    public TCTe.InfCte.Ide.Toma3 createTCTeInfCteIdeToma3() {
        return new TCTe.InfCte.Ide.Toma3();
    }

    /**
     * Create tc te inf cte ide toma 4 tc te . inf cte . ide . toma 4.
     *
     * @return the tc te . inf cte . ide . toma 4
     */
    public TCTe.InfCte.Ide.Toma4 createTCTeInfCteIdeToma4() {
        return new TCTe.InfCte.Ide.Toma4();
    }

    /**
     * Create t unidade transp lac unid transp t unidade transp . lac unid transp.
     *
     * @return the t unidade transp . lac unid transp
     */
    public TUnidadeTransp.LacUnidTransp createTUnidadeTranspLacUnidTransp() {
        return new TUnidadeTransp.LacUnidTransp();
    }

    /**
     * Create t imp osicms 00 t imp os . icms 00.
     *
     * @return the t imp os . icms 00
     */
    public TImpOS.ICMS00 createTImpOSICMS00() {
        return new TImpOS.ICMS00();
    }

    /**
     * Create t imp osicms 20 t imp os . icms 20.
     *
     * @return the t imp os . icms 20
     */
    public TImpOS.ICMS20 createTImpOSICMS20() {
        return new TImpOS.ICMS20();
    }

    /**
     * Create t imp osicms 45 t imp os . icms 45.
     *
     * @return the t imp os . icms 45
     */
    public TImpOS.ICMS45 createTImpOSICMS45() {
        return new TImpOS.ICMS45();
    }

    /**
     * Create t imp osicms 90 t imp os . icms 90.
     *
     * @return the t imp os . icms 90
     */
    public TImpOS.ICMS90 createTImpOSICMS90() {
        return new TImpOS.ICMS90();
    }

    /**
     * Create t imp osicms outra uf t imp os . icms outra uf.
     *
     * @return the t imp os . icms outra uf
     */
    public TImpOS.ICMSOutraUF createTImpOSICMSOutraUF() {
        return new TImpOS.ICMSOutraUF();
    }

    /**
     * Create t imp osicmssn t imp os . icmssn.
     *
     * @return the t imp os . icmssn
     */
    public TImpOS.ICMSSN createTImpOSICMSSN() {
        return new TImpOS.ICMSSN();
    }

    /**
     * Create t unid carga lac unid carga t unid carga . lac unid carga.
     *
     * @return the t unid carga . lac unid carga
     */
    public TUnidCarga.LacUnidCarga createTUnidCargaLacUnidCarga() {
        return new TUnidCarga.LacUnidCarga();
    }

    /**
     * Create t prot c te os inf prot t prot c te os . inf prot.
     *
     * @return the t prot c te os . inf prot
     */
    public TProtCTeOS.InfProt createTProtCTeOSInfProt() {
        return new TProtCTeOS.InfProt();
    }

    /**
     * Create t prot c te os inf fisco t prot c te os . inf fisco.
     *
     * @return the t prot c te os . inf fisco
     */
    public TProtCTeOS.InfFisco createTProtCTeOSInfFisco() {
        return new TProtCTeOS.InfFisco();
    }

    /**
     * Create tc te os inf c te supl tc te os . inf c te supl.
     *
     * @return the tc te os . inf c te supl
     */
    public TCTeOS.InfCTeSupl createTCTeOSInfCTeSupl() {
        return new TCTeOS.InfCTeSupl();
    }

    /**
     * Create tc te os inf cte emit tc te os . inf cte . emit.
     *
     * @return the tc te os . inf cte . emit
     */
    public TCTeOS.InfCte.Emit createTCTeOSInfCteEmit() {
        return new TCTeOS.InfCte.Emit();
    }

    /**
     * Create tc te os inf cte toma tc te os . inf cte . toma.
     *
     * @return the tc te os . inf cte . toma
     */
    public TCTeOS.InfCte.Toma createTCTeOSInfCteToma() {
        return new TCTeOS.InfCte.Toma();
    }

    /**
     * Create tc te os inf cte inf cte comp tc te os . inf cte . inf cte comp.
     *
     * @return the tc te os . inf cte . inf cte comp
     */
    public TCTeOS.InfCte.InfCteComp createTCTeOSInfCteInfCteComp() {
        return new TCTeOS.InfCte.InfCteComp();
    }

    /**
     * Create tc te os inf cte inf cte anu tc te os . inf cte . inf cte anu.
     *
     * @return the tc te os . inf cte . inf cte anu
     */
    public TCTeOS.InfCte.InfCteAnu createTCTeOSInfCteInfCteAnu() {
        return new TCTeOS.InfCte.InfCteAnu();
    }

    /**
     * Create tc te os inf cte aut xml tc te os . inf cte . aut xml.
     *
     * @return the tc te os . inf cte . aut xml
     */
    public TCTeOS.InfCte.AutXML createTCTeOSInfCteAutXML() {
        return new TCTeOS.InfCte.AutXML();
    }

    /**
     * Create tc te os inf cte inf c te norm inf doc ref tc te os . inf cte . inf c te norm . inf doc ref.
     *
     * @return the tc te os . inf cte . inf c te norm . inf doc ref
     */
    public TCTeOS.InfCte.InfCTeNorm.InfDocRef createTCTeOSInfCteInfCTeNormInfDocRef() {
        return new TCTeOS.InfCte.InfCTeNorm.InfDocRef();
    }

    /**
     * Create tc te os inf cte inf c te norm seg tc te os . inf cte . inf c te norm . seg.
     *
     * @return the tc te os . inf cte . inf c te norm . seg
     */
    public TCTeOS.InfCte.InfCTeNorm.Seg createTCTeOSInfCteInfCTeNormSeg() {
        return new TCTeOS.InfCte.InfCTeNorm.Seg();
    }

    /**
     * Create tc te os inf cte inf c te norm inf modal tc te os . inf cte . inf c te norm . inf modal.
     *
     * @return the tc te os . inf cte . inf c te norm . inf modal
     */
    public TCTeOS.InfCte.InfCTeNorm.InfModal createTCTeOSInfCteInfCTeNormInfModal() {
        return new TCTeOS.InfCte.InfCTeNorm.InfModal();
    }

    /**
     * Create tc te os inf cte inf c te norm cobr fat tc te os . inf cte . inf c te norm . cobr . fat.
     *
     * @return the tc te os . inf cte . inf c te norm . cobr . fat
     */
    public TCTeOS.InfCte.InfCTeNorm.Cobr.Fat createTCTeOSInfCteInfCTeNormCobrFat() {
        return new TCTeOS.InfCte.InfCTeNorm.Cobr.Fat();
    }

    /**
     * Create tc te os inf cte inf c te norm cobr dup tc te os . inf cte . inf c te norm . cobr . dup.
     *
     * @return the tc te os . inf cte . inf c te norm . cobr . dup
     */
    public TCTeOS.InfCte.InfCTeNorm.Cobr.Dup createTCTeOSInfCteInfCTeNormCobrDup() {
        return new TCTeOS.InfCte.InfCTeNorm.Cobr.Dup();
    }

    /**
     * Create tc te os inf cte inf c te norm inf cte sub toma icms ref nf tc te os . inf cte . inf c te norm . inf cte sub . toma icms . ref nf.
     *
     * @return the tc te os . inf cte . inf c te norm . inf cte sub . toma icms . ref nf
     */
    public TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF createTCTeOSInfCteInfCTeNormInfCteSubTomaICMSRefNF() {
        return new TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF();
    }

    /**
     * Create tc te os inf cte inf c te norm inf servico inf q tc te os . inf cte . inf c te norm . inf servico . inf q.
     *
     * @return the tc te os . inf cte . inf c te norm . inf servico . inf q
     */
    public TCTeOS.InfCte.InfCTeNorm.InfServico.InfQ createTCTeOSInfCteInfCTeNormInfServicoInfQ() {
        return new TCTeOS.InfCte.InfCTeNorm.InfServico.InfQ();
    }

    /**
     * Create tc te os inf cte imp icmsuf fim tc te os . inf cte . imp . icmsuf fim.
     *
     * @return the tc te os . inf cte . imp . icmsuf fim
     */
    public TCTeOS.InfCte.Imp.ICMSUFFim createTCTeOSInfCteImpICMSUFFim() {
        return new TCTeOS.InfCte.Imp.ICMSUFFim();
    }

    /**
     * Create tc te os inf cte imp inf trib fed tc te os . inf cte . imp . inf trib fed.
     *
     * @return the tc te os . inf cte . imp . inf trib fed
     */
    public TCTeOS.InfCte.Imp.InfTribFed createTCTeOSInfCteImpInfTribFed() {
        return new TCTeOS.InfCte.Imp.InfTribFed();
    }

    /**
     * Create tc te os inf cte v prest comp tc te os . inf cte . v prest . comp.
     *
     * @return the tc te os . inf cte . v prest . comp
     */
    public TCTeOS.InfCte.VPrest.Comp createTCTeOSInfCteVPrestComp() {
        return new TCTeOS.InfCte.VPrest.Comp();
    }

    /**
     * Create tc te os inf cte compl obs cont tc te os . inf cte . compl . obs cont.
     *
     * @return the tc te os . inf cte . compl . obs cont
     */
    public TCTeOS.InfCte.Compl.ObsCont createTCTeOSInfCteComplObsCont() {
        return new TCTeOS.InfCte.Compl.ObsCont();
    }

    /**
     * Create tc te os inf cte compl obs fisco tc te os . inf cte . compl . obs fisco.
     *
     * @return the tc te os . inf cte . compl . obs fisco
     */
    public TCTeOS.InfCte.Compl.ObsFisco createTCTeOSInfCteComplObsFisco() {
        return new TCTeOS.InfCte.Compl.ObsFisco();
    }

    /**
     * Create tc te os inf cte ide inf percurso tc te os . inf cte . ide . inf percurso.
     *
     * @return the tc te os . inf cte . ide . inf percurso
     */
    public TCTeOS.InfCte.Ide.InfPercurso createTCTeOSInfCteIdeInfPercurso() {
        return new TCTeOS.InfCte.Ide.InfPercurso();
    }

    /**
     * Create t ret envi c te inf rec t ret envi c te . inf rec.
     *
     * @return the t ret envi c te . inf rec
     */
    public TRetEnviCTe.InfRec createTRetEnviCTeInfRec() {
        return new TRetEnviCTe.InfRec();
    }

    /**
     * Create signature jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2000/09/xmldsig#", name = "Signature")
    public JAXBElement<SignatureType> createSignature(SignatureType value) {
        return new JAXBElement<SignatureType>(_Signature_QNAME, SignatureType.class, null, value);
    }

    /**
     * Create ret cons reci c te jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "retConsReciCTe")
    public JAXBElement<TRetConsReciCTe> createRetConsReciCTe(TRetConsReciCTe value) {
        return new JAXBElement<TRetConsReciCTe>(_RetConsReciCTe_QNAME, TRetConsReciCTe.class, null, value);
    }

}
