//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Tc te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infCte",
        "infCTeSupl",
        "signature"
})
public class TCTe {

    /**
     * The Inf cte.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TCTe.InfCte infCte;
    /**
     * The Inf c te supl.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected TCTe.InfCTeSupl infCTeSupl;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;

    /**
     * Gets inf cte.
     *
     * @return the inf cte
     */
    public TCTe.InfCte getInfCte() {
        return infCte;
    }

    /**
     * Sets inf cte.
     *
     * @param value the value
     */
    public void setInfCte(TCTe.InfCte value) {
        this.infCte = value;
    }

    /**
     * Gets inf c te supl.
     *
     * @return the inf c te supl
     */
    public TCTe.InfCTeSupl getInfCTeSupl() {
        return infCTeSupl;
    }

    /**
     * Sets inf c te supl.
     *
     * @param value the value
     */
    public void setInfCTeSupl(TCTe.InfCTeSupl value) {
        this.infCTeSupl = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }


    /**
     * The type Inf c te supl.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "qrCodCTe"
    })
    public static class InfCTeSupl {

        /**
         * The Qr cod c te.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String qrCodCTe;

        /**
         * Gets qr cod c te.
         *
         * @return the qr cod c te
         */
        public String getQrCodCTe() {
            return qrCodCTe;
        }

        /**
         * Sets qr cod c te.
         *
         * @param value the value
         */
        public void setQrCodCTe(String value) {
            this.qrCodCTe = value;
        }

    }


    /**
     * The type Inf cte.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ide",
            "compl",
            "emit",
            "rem",
            "exped",
            "receb",
            "dest",
            "vPrest",
            "imp",
            "infCTeNorm",
            "infCteComp",
            "infCteAnu",
            "autXML",
            "infRespTec"
    })
    public static class InfCte {

        /**
         * The Ide.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTe.InfCte.Ide ide;
        /**
         * The Compl.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.Compl compl;
        /**
         * The Emit.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTe.InfCte.Emit emit;
        /**
         * The Rem.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.Rem rem;
        /**
         * The Exped.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.Exped exped;
        /**
         * The Receb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.Receb receb;
        /**
         * The Dest.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.Dest dest;
        /**
         * The V prest.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTe.InfCte.VPrest vPrest;
        /**
         * The Imp.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTe.InfCte.Imp imp;
        /**
         * The Inf c te norm.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.InfCTeNorm infCTeNorm;
        /**
         * The Inf cte comp.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.InfCteComp infCteComp;
        /**
         * The Inf cte anu.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTe.InfCte.InfCteAnu infCteAnu;
        /**
         * The Aut xml.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected List<AutXML> autXML;
        /**
         * The Inf resp tec.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TRespTec infRespTec;
        /**
         * The Versao.
         */
        @XmlAttribute(name = "versao", required = true)
        protected String versao;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;

        /**
         * Gets ide.
         *
         * @return the ide
         */
        public TCTe.InfCte.Ide getIde() {
            return ide;
        }

        /**
         * Sets ide.
         *
         * @param value the value
         */
        public void setIde(TCTe.InfCte.Ide value) {
            this.ide = value;
        }

        /**
         * Gets compl.
         *
         * @return the compl
         */
        public TCTe.InfCte.Compl getCompl() {
            return compl;
        }

        /**
         * Sets compl.
         *
         * @param value the value
         */
        public void setCompl(TCTe.InfCte.Compl value) {
            this.compl = value;
        }

        /**
         * Gets emit.
         *
         * @return the emit
         */
        public TCTe.InfCte.Emit getEmit() {
            return emit;
        }

        /**
         * Sets emit.
         *
         * @param value the value
         */
        public void setEmit(TCTe.InfCte.Emit value) {
            this.emit = value;
        }

        /**
         * Gets rem.
         *
         * @return the rem
         */
        public TCTe.InfCte.Rem getRem() {
            return rem;
        }

        /**
         * Sets rem.
         *
         * @param value the value
         */
        public void setRem(TCTe.InfCte.Rem value) {
            this.rem = value;
        }

        /**
         * Gets exped.
         *
         * @return the exped
         */
        public TCTe.InfCte.Exped getExped() {
            return exped;
        }

        /**
         * Sets exped.
         *
         * @param value the value
         */
        public void setExped(TCTe.InfCte.Exped value) {
            this.exped = value;
        }

        /**
         * Gets receb.
         *
         * @return the receb
         */
        public TCTe.InfCte.Receb getReceb() {
            return receb;
        }

        /**
         * Sets receb.
         *
         * @param value the value
         */
        public void setReceb(TCTe.InfCte.Receb value) {
            this.receb = value;
        }

        /**
         * Gets dest.
         *
         * @return the dest
         */
        public TCTe.InfCte.Dest getDest() {
            return dest;
        }

        /**
         * Sets dest.
         *
         * @param value the value
         */
        public void setDest(TCTe.InfCte.Dest value) {
            this.dest = value;
        }

        /**
         * Gets v prest.
         *
         * @return the v prest
         */
        public TCTe.InfCte.VPrest getVPrest() {
            return vPrest;
        }

        /**
         * Sets v prest.
         *
         * @param value the value
         */
        public void setVPrest(TCTe.InfCte.VPrest value) {
            this.vPrest = value;
        }

        /**
         * Gets imp.
         *
         * @return the imp
         */
        public TCTe.InfCte.Imp getImp() {
            return imp;
        }

        /**
         * Sets imp.
         *
         * @param value the value
         */
        public void setImp(TCTe.InfCte.Imp value) {
            this.imp = value;
        }

        /**
         * Gets inf c te norm.
         *
         * @return the inf c te norm
         */
        public TCTe.InfCte.InfCTeNorm getInfCTeNorm() {
            return infCTeNorm;
        }

        /**
         * Sets inf c te norm.
         *
         * @param value the value
         */
        public void setInfCTeNorm(TCTe.InfCte.InfCTeNorm value) {
            this.infCTeNorm = value;
        }

        /**
         * Gets inf cte comp.
         *
         * @return the inf cte comp
         */
        public TCTe.InfCte.InfCteComp getInfCteComp() {
            return infCteComp;
        }

        /**
         * Sets inf cte comp.
         *
         * @param value the value
         */
        public void setInfCteComp(TCTe.InfCte.InfCteComp value) {
            this.infCteComp = value;
        }

        /**
         * Gets inf cte anu.
         *
         * @return the inf cte anu
         */
        public TCTe.InfCte.InfCteAnu getInfCteAnu() {
            return infCteAnu;
        }

        /**
         * Sets inf cte anu.
         *
         * @param value the value
         */
        public void setInfCteAnu(TCTe.InfCte.InfCteAnu value) {
            this.infCteAnu = value;
        }

        /**
         * Gets aut xml.
         *
         * @return the aut xml
         */
        public List<AutXML> getAutXML() {
            if (autXML == null) {
                autXML = new ArrayList<AutXML>();
            }
            return this.autXML;
        }

        /**
         * Gets inf resp tec.
         *
         * @return the inf resp tec
         */
        public TRespTec getInfRespTec() {
            return infRespTec;
        }

        /**
         * Sets inf resp tec.
         *
         * @param value the value
         */
        public void setInfRespTec(TRespTec value) {
            this.infRespTec = value;
        }

        /**
         * Gets versao.
         *
         * @return the versao
         */
        public String getVersao() {
            return versao;
        }

        /**
         * Sets versao.
         *
         * @param value the value
         */
        public void setVersao(String value) {
            this.versao = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * The type Aut xml.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf"
        })
        public static class AutXML {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

        }


        /**
         * The type Compl.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "xCaracAd",
                "xCaracSer",
                "xEmi",
                "fluxo",
                "entrega",
                "origCalc",
                "destCalc",
                "xObs",
                "obsCont",
                "obsFisco"
        })
        public static class Compl {

            /**
             * The X carac ad.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xCaracAd;
            /**
             * The X carac ser.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xCaracSer;
            /**
             * The X emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xEmi;
            /**
             * The Fluxo.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.Compl.Fluxo fluxo;
            /**
             * The Entrega.
             */
            @XmlElement(name = "Entrega", namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.Compl.Entrega entrega;
            /**
             * The Orig calc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String origCalc;
            /**
             * The Dest calc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String destCalc;
            /**
             * The X obs.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xObs;
            /**
             * The Obs cont.
             */
            @XmlElement(name = "ObsCont", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<ObsCont> obsCont;
            /**
             * The Obs fisco.
             */
            @XmlElement(name = "ObsFisco", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<ObsFisco> obsFisco;

            /**
             * Gets x carac ad.
             *
             * @return the x carac ad
             */
            public String getXCaracAd() {
                return xCaracAd;
            }

            /**
             * Sets x carac ad.
             *
             * @param value the value
             */
            public void setXCaracAd(String value) {
                this.xCaracAd = value;
            }

            /**
             * Gets x carac ser.
             *
             * @return the x carac ser
             */
            public String getXCaracSer() {
                return xCaracSer;
            }

            /**
             * Sets x carac ser.
             *
             * @param value the value
             */
            public void setXCaracSer(String value) {
                this.xCaracSer = value;
            }

            /**
             * Gets x emi.
             *
             * @return the x emi
             */
            public String getXEmi() {
                return xEmi;
            }

            /**
             * Sets x emi.
             *
             * @param value the value
             */
            public void setXEmi(String value) {
                this.xEmi = value;
            }

            /**
             * Gets fluxo.
             *
             * @return the fluxo
             */
            public TCTe.InfCte.Compl.Fluxo getFluxo() {
                return fluxo;
            }

            /**
             * Sets fluxo.
             *
             * @param value the value
             */
            public void setFluxo(TCTe.InfCte.Compl.Fluxo value) {
                this.fluxo = value;
            }

            /**
             * Gets entrega.
             *
             * @return the entrega
             */
            public TCTe.InfCte.Compl.Entrega getEntrega() {
                return entrega;
            }

            /**
             * Sets entrega.
             *
             * @param value the value
             */
            public void setEntrega(TCTe.InfCte.Compl.Entrega value) {
                this.entrega = value;
            }

            /**
             * Gets orig calc.
             *
             * @return the orig calc
             */
            public String getOrigCalc() {
                return origCalc;
            }

            /**
             * Sets orig calc.
             *
             * @param value the value
             */
            public void setOrigCalc(String value) {
                this.origCalc = value;
            }

            /**
             * Gets dest calc.
             *
             * @return the dest calc
             */
            public String getDestCalc() {
                return destCalc;
            }

            /**
             * Sets dest calc.
             *
             * @param value the value
             */
            public void setDestCalc(String value) {
                this.destCalc = value;
            }

            /**
             * Gets x obs.
             *
             * @return the x obs
             */
            public String getXObs() {
                return xObs;
            }

            /**
             * Sets x obs.
             *
             * @param value the value
             */
            public void setXObs(String value) {
                this.xObs = value;
            }

            /**
             * Gets obs cont.
             *
             * @return the obs cont
             */
            public List<ObsCont> getObsCont() {
                if (obsCont == null) {
                    obsCont = new ArrayList<ObsCont>();
                }
                return this.obsCont;
            }

            /**
             * Gets obs fisco.
             *
             * @return the obs fisco
             */
            public List<ObsFisco> getObsFisco() {
                if (obsFisco == null) {
                    obsFisco = new ArrayList<ObsFisco>();
                }
                return this.obsFisco;
            }


            /**
             * The type Entrega.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "semData",
                    "comData",
                    "noPeriodo",
                    "semHora",
                    "comHora",
                    "noInter"
            })
            public static class Entrega {

                /**
                 * The Sem data.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.SemData semData;
                /**
                 * The Com data.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.ComData comData;
                /**
                 * The No periodo.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.NoPeriodo noPeriodo;
                /**
                 * The Sem hora.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.SemHora semHora;
                /**
                 * The Com hora.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.ComHora comHora;
                /**
                 * The No inter.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.Compl.Entrega.NoInter noInter;

                /**
                 * Gets sem data.
                 *
                 * @return the sem data
                 */
                public TCTe.InfCte.Compl.Entrega.SemData getSemData() {
                    return semData;
                }

                /**
                 * Sets sem data.
                 *
                 * @param value the value
                 */
                public void setSemData(TCTe.InfCte.Compl.Entrega.SemData value) {
                    this.semData = value;
                }

                /**
                 * Gets com data.
                 *
                 * @return the com data
                 */
                public TCTe.InfCte.Compl.Entrega.ComData getComData() {
                    return comData;
                }

                /**
                 * Sets com data.
                 *
                 * @param value the value
                 */
                public void setComData(TCTe.InfCte.Compl.Entrega.ComData value) {
                    this.comData = value;
                }

                /**
                 * Gets no periodo.
                 *
                 * @return the no periodo
                 */
                public TCTe.InfCte.Compl.Entrega.NoPeriodo getNoPeriodo() {
                    return noPeriodo;
                }

                /**
                 * Sets no periodo.
                 *
                 * @param value the value
                 */
                public void setNoPeriodo(TCTe.InfCte.Compl.Entrega.NoPeriodo value) {
                    this.noPeriodo = value;
                }

                /**
                 * Gets sem hora.
                 *
                 * @return the sem hora
                 */
                public TCTe.InfCte.Compl.Entrega.SemHora getSemHora() {
                    return semHora;
                }

                /**
                 * Sets sem hora.
                 *
                 * @param value the value
                 */
                public void setSemHora(TCTe.InfCte.Compl.Entrega.SemHora value) {
                    this.semHora = value;
                }

                /**
                 * Gets com hora.
                 *
                 * @return the com hora
                 */
                public TCTe.InfCte.Compl.Entrega.ComHora getComHora() {
                    return comHora;
                }

                /**
                 * Sets com hora.
                 *
                 * @param value the value
                 */
                public void setComHora(TCTe.InfCte.Compl.Entrega.ComHora value) {
                    this.comHora = value;
                }

                /**
                 * Gets no inter.
                 *
                 * @return the no inter
                 */
                public TCTe.InfCte.Compl.Entrega.NoInter getNoInter() {
                    return noInter;
                }

                /**
                 * Sets no inter.
                 *
                 * @param value the value
                 */
                public void setNoInter(TCTe.InfCte.Compl.Entrega.NoInter value) {
                    this.noInter = value;
                }


                /**
                 * The type Com data.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpPer",
                        "dProg"
                })
                public static class ComData {

                    /**
                     * The Tp per.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpPer;
                    /**
                     * The D prog.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String dProg;

                    /**
                     * Gets tp per.
                     *
                     * @return the tp per
                     */
                    public String getTpPer() {
                        return tpPer;
                    }

                    /**
                     * Sets tp per.
                     *
                     * @param value the value
                     */
                    public void setTpPer(String value) {
                        this.tpPer = value;
                    }

                    /**
                     * Gets d prog.
                     *
                     * @return the d prog
                     */
                    public String getDProg() {
                        return dProg;
                    }

                    /**
                     * Sets d prog.
                     *
                     * @param value the value
                     */
                    public void setDProg(String value) {
                        this.dProg = value;
                    }

                }


                /**
                 * The type Com hora.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpHor",
                        "hProg"
                })
                public static class ComHora {

                    /**
                     * The Tp hor.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpHor;
                    /**
                     * The H prog.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String hProg;

                    /**
                     * Gets tp hor.
                     *
                     * @return the tp hor
                     */
                    public String getTpHor() {
                        return tpHor;
                    }

                    /**
                     * Sets tp hor.
                     *
                     * @param value the value
                     */
                    public void setTpHor(String value) {
                        this.tpHor = value;
                    }

                    /**
                     * Gets h prog.
                     *
                     * @return the h prog
                     */
                    public String getHProg() {
                        return hProg;
                    }

                    /**
                     * Sets h prog.
                     *
                     * @param value the value
                     */
                    public void setHProg(String value) {
                        this.hProg = value;
                    }

                }


                /**
                 * The type No inter.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpHor",
                        "hIni",
                        "hFim"
                })
                public static class NoInter {

                    /**
                     * The Tp hor.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpHor;
                    /**
                     * The H ini.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String hIni;
                    /**
                     * The H fim.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String hFim;

                    /**
                     * Gets tp hor.
                     *
                     * @return the tp hor
                     */
                    public String getTpHor() {
                        return tpHor;
                    }

                    /**
                     * Sets tp hor.
                     *
                     * @param value the value
                     */
                    public void setTpHor(String value) {
                        this.tpHor = value;
                    }

                    /**
                     * Gets h ini.
                     *
                     * @return the h ini
                     */
                    public String getHIni() {
                        return hIni;
                    }

                    /**
                     * Sets h ini.
                     *
                     * @param value the value
                     */
                    public void setHIni(String value) {
                        this.hIni = value;
                    }

                    /**
                     * Gets h fim.
                     *
                     * @return the h fim
                     */
                    public String getHFim() {
                        return hFim;
                    }

                    /**
                     * Sets h fim.
                     *
                     * @param value the value
                     */
                    public void setHFim(String value) {
                        this.hFim = value;
                    }

                }


                /**
                 * The type No periodo.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpPer",
                        "dIni",
                        "dFim"
                })
                public static class NoPeriodo {

                    /**
                     * The Tp per.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpPer;
                    /**
                     * The D ini.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String dIni;
                    /**
                     * The D fim.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String dFim;

                    /**
                     * Gets tp per.
                     *
                     * @return the tp per
                     */
                    public String getTpPer() {
                        return tpPer;
                    }

                    /**
                     * Sets tp per.
                     *
                     * @param value the value
                     */
                    public void setTpPer(String value) {
                        this.tpPer = value;
                    }

                    /**
                     * Gets d ini.
                     *
                     * @return the d ini
                     */
                    public String getDIni() {
                        return dIni;
                    }

                    /**
                     * Sets d ini.
                     *
                     * @param value the value
                     */
                    public void setDIni(String value) {
                        this.dIni = value;
                    }

                    /**
                     * Gets d fim.
                     *
                     * @return the d fim
                     */
                    public String getDFim() {
                        return dFim;
                    }

                    /**
                     * Sets d fim.
                     *
                     * @param value the value
                     */
                    public void setDFim(String value) {
                        this.dFim = value;
                    }

                }


                /**
                 * The type Sem data.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpPer"
                })
                public static class SemData {

                    /**
                     * The Tp per.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpPer;

                    /**
                     * Gets tp per.
                     *
                     * @return the tp per
                     */
                    public String getTpPer() {
                        return tpPer;
                    }

                    /**
                     * Sets tp per.
                     *
                     * @param value the value
                     */
                    public void setTpPer(String value) {
                        this.tpPer = value;
                    }

                }


                /**
                 * The type Sem hora.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpHor"
                })
                public static class SemHora {

                    /**
                     * The Tp hor.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpHor;

                    /**
                     * Gets tp hor.
                     *
                     * @return the tp hor
                     */
                    public String getTpHor() {
                        return tpHor;
                    }

                    /**
                     * Sets tp hor.
                     *
                     * @param value the value
                     */
                    public void setTpHor(String value) {
                        this.tpHor = value;
                    }

                }

            }


            /**
             * The type Fluxo.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xOrig",
                    "pass",
                    "xDest",
                    "xRota"
            })
            public static class Fluxo {

                /**
                 * The X orig.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xOrig;
                /**
                 * The Pass.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<Pass> pass;
                /**
                 * The X dest.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xDest;
                /**
                 * The X rota.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xRota;

                /**
                 * Gets x orig.
                 *
                 * @return the x orig
                 */
                public String getXOrig() {
                    return xOrig;
                }

                /**
                 * Sets x orig.
                 *
                 * @param value the value
                 */
                public void setXOrig(String value) {
                    this.xOrig = value;
                }

                /**
                 * Gets pass.
                 *
                 * @return the pass
                 */
                public List<Pass> getPass() {
                    if (pass == null) {
                        pass = new ArrayList<Pass>();
                    }
                    return this.pass;
                }

                /**
                 * Gets x dest.
                 *
                 * @return the x dest
                 */
                public String getXDest() {
                    return xDest;
                }

                /**
                 * Sets x dest.
                 *
                 * @param value the value
                 */
                public void setXDest(String value) {
                    this.xDest = value;
                }

                /**
                 * Gets x rota.
                 *
                 * @return the x rota
                 */
                public String getXRota() {
                    return xRota;
                }

                /**
                 * Sets x rota.
                 *
                 * @param value the value
                 */
                public void setXRota(String value) {
                    this.xRota = value;
                }


                /**
                 * The type Pass.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "xPass"
                })
                public static class Pass {

                    /**
                     * The X pass.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String xPass;

                    /**
                     * Gets x pass.
                     *
                     * @return the x pass
                     */
                    public String getXPass() {
                        return xPass;
                    }

                    /**
                     * Sets x pass.
                     *
                     * @param value the value
                     */
                    public void setXPass(String value) {
                        this.xPass = value;
                    }

                }

            }


            /**
             * The type Obs cont.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xTexto"
            })
            public static class ObsCont {

                /**
                 * The X texto.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xTexto;
                /**
                 * The X campo.
                 */
                @XmlAttribute(name = "xCampo", required = true)
                protected String xCampo;

                /**
                 * Gets x texto.
                 *
                 * @return the x texto
                 */
                public String getXTexto() {
                    return xTexto;
                }

                /**
                 * Sets x texto.
                 *
                 * @param value the value
                 */
                public void setXTexto(String value) {
                    this.xTexto = value;
                }

                /**
                 * Gets x campo.
                 *
                 * @return the x campo
                 */
                public String getXCampo() {
                    return xCampo;
                }

                /**
                 * Sets x campo.
                 *
                 * @param value the value
                 */
                public void setXCampo(String value) {
                    this.xCampo = value;
                }

            }


            /**
             * The type Obs fisco.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xTexto"
            })
            public static class ObsFisco {

                /**
                 * The X texto.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xTexto;
                /**
                 * The X campo.
                 */
                @XmlAttribute(name = "xCampo", required = true)
                protected String xCampo;

                /**
                 * Gets x texto.
                 *
                 * @return the x texto
                 */
                public String getXTexto() {
                    return xTexto;
                }

                /**
                 * Sets x texto.
                 *
                 * @param value the value
                 */
                public void setXTexto(String value) {
                    this.xTexto = value;
                }

                /**
                 * Gets x campo.
                 *
                 * @return the x campo
                 */
                public String getXCampo() {
                    return xCampo;
                }

                /**
                 * Sets x campo.
                 *
                 * @param value the value
                 */
                public void setXCampo(String value) {
                    this.xCampo = value;
                }

            }

        }


        /**
         * The type Dest.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf",
                "ie",
                "xNome",
                "fone",
                "isuf",
                "enderDest",
                "email"
        })
        public static class Dest {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String ie;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;
            /**
             * The Isuf.
             */
            @XmlElement(name = "ISUF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String isuf;
            /**
             * The Ender dest.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndereco enderDest;
            /**
             * The Email.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String email;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

            /**
             * Gets isuf.
             *
             * @return the isuf
             */
            public String getISUF() {
                return isuf;
            }

            /**
             * Sets isuf.
             *
             * @param value the value
             */
            public void setISUF(String value) {
                this.isuf = value;
            }

            /**
             * Gets ender dest.
             *
             * @return the ender dest
             */
            public TEndereco getEnderDest() {
                return enderDest;
            }

            /**
             * Sets ender dest.
             *
             * @param value the value
             */
            public void setEnderDest(TEndereco value) {
                this.enderDest = value;
            }

            /**
             * Gets email.
             *
             * @return the email
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets email.
             *
             * @param value the value
             */
            public void setEmail(String value) {
                this.email = value;
            }

        }


        /**
         * The type Emit.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "ie",
                "iest",
                "xNome",
                "xFant",
                "enderEmit"
        })
        public static class Emit {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cnpj;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String ie;
            /**
             * The Iest.
             */
            @XmlElement(name = "IEST", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String iest;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The X fant.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xFant;
            /**
             * The Ender emit.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndeEmi enderEmit;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets iest.
             *
             * @return the iest
             */
            public String getIEST() {
                return iest;
            }

            /**
             * Sets iest.
             *
             * @param value the value
             */
            public void setIEST(String value) {
                this.iest = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets x fant.
             *
             * @return the x fant
             */
            public String getXFant() {
                return xFant;
            }

            /**
             * Sets x fant.
             *
             * @param value the value
             */
            public void setXFant(String value) {
                this.xFant = value;
            }

            /**
             * Gets ender emit.
             *
             * @return the ender emit
             */
            public TEndeEmi getEnderEmit() {
                return enderEmit;
            }

            /**
             * Sets ender emit.
             *
             * @param value the value
             */
            public void setEnderEmit(TEndeEmi value) {
                this.enderEmit = value;
            }

        }


        /**
         * The type Exped.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf",
                "ie",
                "xNome",
                "fone",
                "enderExped",
                "email"
        })
        public static class Exped {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String ie;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;
            /**
             * The Ender exped.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndereco enderExped;
            /**
             * The Email.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String email;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

            /**
             * Gets ender exped.
             *
             * @return the ender exped
             */
            public TEndereco getEnderExped() {
                return enderExped;
            }

            /**
             * Sets ender exped.
             *
             * @param value the value
             */
            public void setEnderExped(TEndereco value) {
                this.enderExped = value;
            }

            /**
             * Gets email.
             *
             * @return the email
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets email.
             *
             * @param value the value
             */
            public void setEmail(String value) {
                this.email = value;
            }

        }


        /**
         * The type Ide.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cuf",
                "cct",
                "cfop",
                "natOp",
                "mod",
                "serie",
                "nct",
                "dhEmi",
                "tpImp",
                "tpEmis",
                "cdv",
                "tpAmb",
                "tpCTe",
                "procEmi",
                "verProc",
                "indGlobalizado",
                "cMunEnv",
                "xMunEnv",
                "ufEnv",
                "modal",
                "tpServ",
                "cMunIni",
                "xMunIni",
                "ufIni",
                "cMunFim",
                "xMunFim",
                "ufFim",
                "retira",
                "xDetRetira",
                "indIEToma",
                "toma3",
                "toma4",
                "dhCont",
                "xJust"
        })
        public static class Ide {

            /**
             * The Cuf.
             */
            @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cuf;
            /**
             * The Cct.
             */
            @XmlElement(name = "cCT", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cct;
            /**
             * The Cfop.
             */
            @XmlElement(name = "CFOP", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cfop;
            /**
             * The Nat op.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String natOp;
            /**
             * The Mod.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String mod;
            /**
             * The Serie.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String serie;
            /**
             * The Nct.
             */
            @XmlElement(name = "nCT", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String nct;
            /**
             * The Dh emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String dhEmi;
            /**
             * The Tp imp.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpImp;
            /**
             * The Tp emis.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpEmis;
            /**
             * The Cdv.
             */
            @XmlElement(name = "cDV", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cdv;
            /**
             * The Tp amb.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpAmb;
            /**
             * The Tp c te.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpCTe;
            /**
             * The Proc emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String procEmi;
            /**
             * The Ver proc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String verProc;
            /**
             * The Ind globalizado.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String indGlobalizado;
            /**
             * The C mun env.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cMunEnv;
            /**
             * The X mun env.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xMunEnv;
            /**
             * The Uf env.
             */
            @XmlElement(name = "UFEnv", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            @XmlSchemaType(name = "string")
            protected TUf ufEnv;
            /**
             * The Modal.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String modal;
            /**
             * The Tp serv.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpServ;
            /**
             * The C mun ini.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cMunIni;
            /**
             * The X mun ini.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xMunIni;
            /**
             * The Uf ini.
             */
            @XmlElement(name = "UFIni", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            @XmlSchemaType(name = "string")
            protected TUf ufIni;
            /**
             * The C mun fim.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cMunFim;
            /**
             * The X mun fim.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xMunFim;
            /**
             * The Uf fim.
             */
            @XmlElement(name = "UFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            @XmlSchemaType(name = "string")
            protected TUf ufFim;
            /**
             * The Retira.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String retira;
            /**
             * The X det retira.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xDetRetira;
            /**
             * The Ind ie toma.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String indIEToma;
            /**
             * The Toma 3.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.Ide.Toma3 toma3;
            /**
             * The Toma 4.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.Ide.Toma4 toma4;
            /**
             * The Dh cont.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String dhCont;
            /**
             * The X just.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xJust;

            /**
             * Gets cuf.
             *
             * @return the cuf
             */
            public String getCUF() {
                return cuf;
            }

            /**
             * Sets cuf.
             *
             * @param value the value
             */
            public void setCUF(String value) {
                this.cuf = value;
            }

            /**
             * Gets cct.
             *
             * @return the cct
             */
            public String getCCT() {
                return cct;
            }

            /**
             * Sets cct.
             *
             * @param value the value
             */
            public void setCCT(String value) {
                this.cct = value;
            }

            /**
             * Gets cfop.
             *
             * @return the cfop
             */
            public String getCFOP() {
                return cfop;
            }

            /**
             * Sets cfop.
             *
             * @param value the value
             */
            public void setCFOP(String value) {
                this.cfop = value;
            }

            /**
             * Gets nat op.
             *
             * @return the nat op
             */
            public String getNatOp() {
                return natOp;
            }

            /**
             * Sets nat op.
             *
             * @param value the value
             */
            public void setNatOp(String value) {
                this.natOp = value;
            }

            /**
             * Gets mod.
             *
             * @return the mod
             */
            public String getMod() {
                return mod;
            }

            /**
             * Sets mod.
             *
             * @param value the value
             */
            public void setMod(String value) {
                this.mod = value;
            }

            /**
             * Gets serie.
             *
             * @return the serie
             */
            public String getSerie() {
                return serie;
            }

            /**
             * Sets serie.
             *
             * @param value the value
             */
            public void setSerie(String value) {
                this.serie = value;
            }

            /**
             * Gets nct.
             *
             * @return the nct
             */
            public String getNCT() {
                return nct;
            }

            /**
             * Sets nct.
             *
             * @param value the value
             */
            public void setNCT(String value) {
                this.nct = value;
            }

            /**
             * Gets dh emi.
             *
             * @return the dh emi
             */
            public String getDhEmi() {
                return dhEmi;
            }

            /**
             * Sets dh emi.
             *
             * @param value the value
             */
            public void setDhEmi(String value) {
                this.dhEmi = value;
            }

            /**
             * Gets tp imp.
             *
             * @return the tp imp
             */
            public String getTpImp() {
                return tpImp;
            }

            /**
             * Sets tp imp.
             *
             * @param value the value
             */
            public void setTpImp(String value) {
                this.tpImp = value;
            }

            /**
             * Gets tp emis.
             *
             * @return the tp emis
             */
            public String getTpEmis() {
                return tpEmis;
            }

            /**
             * Sets tp emis.
             *
             * @param value the value
             */
            public void setTpEmis(String value) {
                this.tpEmis = value;
            }

            /**
             * Gets cdv.
             *
             * @return the cdv
             */
            public String getCDV() {
                return cdv;
            }

            /**
             * Sets cdv.
             *
             * @param value the value
             */
            public void setCDV(String value) {
                this.cdv = value;
            }

            /**
             * Gets tp amb.
             *
             * @return the tp amb
             */
            public String getTpAmb() {
                return tpAmb;
            }

            /**
             * Sets tp amb.
             *
             * @param value the value
             */
            public void setTpAmb(String value) {
                this.tpAmb = value;
            }

            /**
             * Gets tp c te.
             *
             * @return the tp c te
             */
            public String getTpCTe() {
                return tpCTe;
            }

            /**
             * Sets tp c te.
             *
             * @param value the value
             */
            public void setTpCTe(String value) {
                this.tpCTe = value;
            }

            /**
             * Gets proc emi.
             *
             * @return the proc emi
             */
            public String getProcEmi() {
                return procEmi;
            }

            /**
             * Sets proc emi.
             *
             * @param value the value
             */
            public void setProcEmi(String value) {
                this.procEmi = value;
            }

            /**
             * Gets ver proc.
             *
             * @return the ver proc
             */
            public String getVerProc() {
                return verProc;
            }

            /**
             * Sets ver proc.
             *
             * @param value the value
             */
            public void setVerProc(String value) {
                this.verProc = value;
            }

            /**
             * Gets ind globalizado.
             *
             * @return the ind globalizado
             */
            public String getIndGlobalizado() {
                return indGlobalizado;
            }

            /**
             * Sets ind globalizado.
             *
             * @param value the value
             */
            public void setIndGlobalizado(String value) {
                this.indGlobalizado = value;
            }

            /**
             * Gets c mun env.
             *
             * @return the c mun env
             */
            public String getCMunEnv() {
                return cMunEnv;
            }

            /**
             * Sets c mun env.
             *
             * @param value the value
             */
            public void setCMunEnv(String value) {
                this.cMunEnv = value;
            }

            /**
             * Gets x mun env.
             *
             * @return the x mun env
             */
            public String getXMunEnv() {
                return xMunEnv;
            }

            /**
             * Sets x mun env.
             *
             * @param value the value
             */
            public void setXMunEnv(String value) {
                this.xMunEnv = value;
            }

            /**
             * Gets uf env.
             *
             * @return the uf env
             */
            public TUf getUFEnv() {
                return ufEnv;
            }

            /**
             * Sets uf env.
             *
             * @param value the value
             */
            public void setUFEnv(TUf value) {
                this.ufEnv = value;
            }

            /**
             * Gets modal.
             *
             * @return the modal
             */
            public String getModal() {
                return modal;
            }

            /**
             * Sets modal.
             *
             * @param value the value
             */
            public void setModal(String value) {
                this.modal = value;
            }

            /**
             * Gets tp serv.
             *
             * @return the tp serv
             */
            public String getTpServ() {
                return tpServ;
            }

            /**
             * Sets tp serv.
             *
             * @param value the value
             */
            public void setTpServ(String value) {
                this.tpServ = value;
            }

            /**
             * Gets c mun ini.
             *
             * @return the c mun ini
             */
            public String getCMunIni() {
                return cMunIni;
            }

            /**
             * Sets c mun ini.
             *
             * @param value the value
             */
            public void setCMunIni(String value) {
                this.cMunIni = value;
            }

            /**
             * Gets x mun ini.
             *
             * @return the x mun ini
             */
            public String getXMunIni() {
                return xMunIni;
            }

            /**
             * Sets x mun ini.
             *
             * @param value the value
             */
            public void setXMunIni(String value) {
                this.xMunIni = value;
            }

            /**
             * Gets uf ini.
             *
             * @return the uf ini
             */
            public TUf getUFIni() {
                return ufIni;
            }

            /**
             * Sets uf ini.
             *
             * @param value the value
             */
            public void setUFIni(TUf value) {
                this.ufIni = value;
            }

            /**
             * Gets c mun fim.
             *
             * @return the c mun fim
             */
            public String getCMunFim() {
                return cMunFim;
            }

            /**
             * Sets c mun fim.
             *
             * @param value the value
             */
            public void setCMunFim(String value) {
                this.cMunFim = value;
            }

            /**
             * Gets x mun fim.
             *
             * @return the x mun fim
             */
            public String getXMunFim() {
                return xMunFim;
            }

            /**
             * Sets x mun fim.
             *
             * @param value the value
             */
            public void setXMunFim(String value) {
                this.xMunFim = value;
            }

            /**
             * Gets uf fim.
             *
             * @return the uf fim
             */
            public TUf getUFFim() {
                return ufFim;
            }

            /**
             * Sets uf fim.
             *
             * @param value the value
             */
            public void setUFFim(TUf value) {
                this.ufFim = value;
            }

            /**
             * Gets retira.
             *
             * @return the retira
             */
            public String getRetira() {
                return retira;
            }

            /**
             * Sets retira.
             *
             * @param value the value
             */
            public void setRetira(String value) {
                this.retira = value;
            }

            /**
             * Gets x det retira.
             *
             * @return the x det retira
             */
            public String getXDetRetira() {
                return xDetRetira;
            }

            /**
             * Sets x det retira.
             *
             * @param value the value
             */
            public void setXDetRetira(String value) {
                this.xDetRetira = value;
            }

            /**
             * Gets ind ie toma.
             *
             * @return the ind ie toma
             */
            public String getIndIEToma() {
                return indIEToma;
            }

            /**
             * Sets ind ie toma.
             *
             * @param value the value
             */
            public void setIndIEToma(String value) {
                this.indIEToma = value;
            }

            /**
             * Gets toma 3.
             *
             * @return the toma 3
             */
            public TCTe.InfCte.Ide.Toma3 getToma3() {
                return toma3;
            }

            /**
             * Sets toma 3.
             *
             * @param value the value
             */
            public void setToma3(TCTe.InfCte.Ide.Toma3 value) {
                this.toma3 = value;
            }

            /**
             * Gets toma 4.
             *
             * @return the toma 4
             */
            public TCTe.InfCte.Ide.Toma4 getToma4() {
                return toma4;
            }

            /**
             * Sets toma 4.
             *
             * @param value the value
             */
            public void setToma4(TCTe.InfCte.Ide.Toma4 value) {
                this.toma4 = value;
            }

            /**
             * Gets dh cont.
             *
             * @return the dh cont
             */
            public String getDhCont() {
                return dhCont;
            }

            /**
             * Sets dh cont.
             *
             * @param value the value
             */
            public void setDhCont(String value) {
                this.dhCont = value;
            }

            /**
             * Gets x just.
             *
             * @return the x just
             */
            public String getXJust() {
                return xJust;
            }

            /**
             * Sets x just.
             *
             * @param value the value
             */
            public void setXJust(String value) {
                this.xJust = value;
            }


            /**
             * The type Toma 3.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "toma"
            })
            public static class Toma3 {

                /**
                 * The Toma.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String toma;

                /**
                 * Gets toma.
                 *
                 * @return the toma
                 */
                public String getToma() {
                    return toma;
                }

                /**
                 * Sets toma.
                 *
                 * @param value the value
                 */
                public void setToma(String value) {
                    this.toma = value;
                }

            }


            /**
             * The type Toma 4.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "toma",
                    "cnpj",
                    "cpf",
                    "ie",
                    "xNome",
                    "xFant",
                    "fone",
                    "enderToma",
                    "email"
            })
            public static class Toma4 {

                /**
                 * The Toma.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String toma;
                /**
                 * The Cnpj.
                 */
                @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String cnpj;
                /**
                 * The Cpf.
                 */
                @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String cpf;
                /**
                 * The Ie.
                 */
                @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String ie;
                /**
                 * The X nome.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xNome;
                /**
                 * The X fant.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xFant;
                /**
                 * The Fone.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String fone;
                /**
                 * The Ender toma.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected TEndereco enderToma;
                /**
                 * The Email.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String email;

                /**
                 * Gets toma.
                 *
                 * @return the toma
                 */
                public String getToma() {
                    return toma;
                }

                /**
                 * Sets toma.
                 *
                 * @param value the value
                 */
                public void setToma(String value) {
                    this.toma = value;
                }

                /**
                 * Gets cnpj.
                 *
                 * @return the cnpj
                 */
                public String getCNPJ() {
                    return cnpj;
                }

                /**
                 * Sets cnpj.
                 *
                 * @param value the value
                 */
                public void setCNPJ(String value) {
                    this.cnpj = value;
                }

                /**
                 * Gets cpf.
                 *
                 * @return the cpf
                 */
                public String getCPF() {
                    return cpf;
                }

                /**
                 * Sets cpf.
                 *
                 * @param value the value
                 */
                public void setCPF(String value) {
                    this.cpf = value;
                }

                /**
                 * Gets ie.
                 *
                 * @return the ie
                 */
                public String getIE() {
                    return ie;
                }

                /**
                 * Sets ie.
                 *
                 * @param value the value
                 */
                public void setIE(String value) {
                    this.ie = value;
                }

                /**
                 * Gets x nome.
                 *
                 * @return the x nome
                 */
                public String getXNome() {
                    return xNome;
                }

                /**
                 * Sets x nome.
                 *
                 * @param value the value
                 */
                public void setXNome(String value) {
                    this.xNome = value;
                }

                /**
                 * Gets x fant.
                 *
                 * @return the x fant
                 */
                public String getXFant() {
                    return xFant;
                }

                /**
                 * Sets x fant.
                 *
                 * @param value the value
                 */
                public void setXFant(String value) {
                    this.xFant = value;
                }

                /**
                 * Gets fone.
                 *
                 * @return the fone
                 */
                public String getFone() {
                    return fone;
                }

                /**
                 * Sets fone.
                 *
                 * @param value the value
                 */
                public void setFone(String value) {
                    this.fone = value;
                }

                /**
                 * Gets ender toma.
                 *
                 * @return the ender toma
                 */
                public TEndereco getEnderToma() {
                    return enderToma;
                }

                /**
                 * Sets ender toma.
                 *
                 * @param value the value
                 */
                public void setEnderToma(TEndereco value) {
                    this.enderToma = value;
                }

                /**
                 * Gets email.
                 *
                 * @return the email
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets email.
                 *
                 * @param value the value
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

            }

        }


        /**
         * The type Imp.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "icms",
                "vTotTrib",
                "infAdFisco",
                "icmsufFim"
        })
        public static class Imp {

            /**
             * The Icms.
             */
            @XmlElement(name = "ICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TImp icms;
            /**
             * The V tot trib.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String vTotTrib;
            /**
             * The Inf ad fisco.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String infAdFisco;
            /**
             * The Icmsuf fim.
             */
            @XmlElement(name = "ICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.Imp.ICMSUFFim icmsufFim;

            /**
             * Gets icms.
             *
             * @return the icms
             */
            public TImp getICMS() {
                return icms;
            }

            /**
             * Sets icms.
             *
             * @param value the value
             */
            public void setICMS(TImp value) {
                this.icms = value;
            }

            /**
             * Gets v tot trib.
             *
             * @return the v tot trib
             */
            public String getVTotTrib() {
                return vTotTrib;
            }

            /**
             * Sets v tot trib.
             *
             * @param value the value
             */
            public void setVTotTrib(String value) {
                this.vTotTrib = value;
            }

            /**
             * Gets inf ad fisco.
             *
             * @return the inf ad fisco
             */
            public String getInfAdFisco() {
                return infAdFisco;
            }

            /**
             * Sets inf ad fisco.
             *
             * @param value the value
             */
            public void setInfAdFisco(String value) {
                this.infAdFisco = value;
            }

            /**
             * Gets icmsuf fim.
             *
             * @return the icmsuf fim
             */
            public TCTe.InfCte.Imp.ICMSUFFim getICMSUFFim() {
                return icmsufFim;
            }

            /**
             * Sets icmsuf fim.
             *
             * @param value the value
             */
            public void setICMSUFFim(TCTe.InfCte.Imp.ICMSUFFim value) {
                this.icmsufFim = value;
            }


            /**
             * The type Icmsuf fim.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vbcufFim",
                    "pfcpufFim",
                    "picmsufFim",
                    "picmsInter",
                    "vfcpufFim",
                    "vicmsufFim",
                    "vicmsufIni"
            })
            public static class ICMSUFFim {

                /**
                 * The Vbcuf fim.
                 */
                @XmlElement(name = "vBCUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vbcufFim;
                /**
                 * The Pfcpuf fim.
                 */
                @XmlElement(name = "pFCPUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String pfcpufFim;
                /**
                 * The Picmsuf fim.
                 */
                @XmlElement(name = "pICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String picmsufFim;
                /**
                 * The Picms inter.
                 */
                @XmlElement(name = "pICMSInter", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String picmsInter;
                /**
                 * The Vfcpuf fim.
                 */
                @XmlElement(name = "vFCPUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vfcpufFim;
                /**
                 * The Vicmsuf fim.
                 */
                @XmlElement(name = "vICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vicmsufFim;
                /**
                 * The Vicmsuf ini.
                 */
                @XmlElement(name = "vICMSUFIni", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vicmsufIni;

                /**
                 * Gets vbcuf fim.
                 *
                 * @return the vbcuf fim
                 */
                public String getVBCUFFim() {
                    return vbcufFim;
                }

                /**
                 * Sets vbcuf fim.
                 *
                 * @param value the value
                 */
                public void setVBCUFFim(String value) {
                    this.vbcufFim = value;
                }

                /**
                 * Gets pfcpuf fim.
                 *
                 * @return the pfcpuf fim
                 */
                public String getPFCPUFFim() {
                    return pfcpufFim;
                }

                /**
                 * Sets pfcpuf fim.
                 *
                 * @param value the value
                 */
                public void setPFCPUFFim(String value) {
                    this.pfcpufFim = value;
                }

                /**
                 * Gets picmsuf fim.
                 *
                 * @return the picmsuf fim
                 */
                public String getPICMSUFFim() {
                    return picmsufFim;
                }

                /**
                 * Sets picmsuf fim.
                 *
                 * @param value the value
                 */
                public void setPICMSUFFim(String value) {
                    this.picmsufFim = value;
                }

                /**
                 * Gets picms inter.
                 *
                 * @return the picms inter
                 */
                public String getPICMSInter() {
                    return picmsInter;
                }

                /**
                 * Sets picms inter.
                 *
                 * @param value the value
                 */
                public void setPICMSInter(String value) {
                    this.picmsInter = value;
                }

                /**
                 * Gets vfcpuf fim.
                 *
                 * @return the vfcpuf fim
                 */
                public String getVFCPUFFim() {
                    return vfcpufFim;
                }

                /**
                 * Sets vfcpuf fim.
                 *
                 * @param value the value
                 */
                public void setVFCPUFFim(String value) {
                    this.vfcpufFim = value;
                }

                /**
                 * Gets vicmsuf fim.
                 *
                 * @return the vicmsuf fim
                 */
                public String getVICMSUFFim() {
                    return vicmsufFim;
                }

                /**
                 * Sets vicmsuf fim.
                 *
                 * @param value the value
                 */
                public void setVICMSUFFim(String value) {
                    this.vicmsufFim = value;
                }

                /**
                 * Gets vicmsuf ini.
                 *
                 * @return the vicmsuf ini
                 */
                public String getVICMSUFIni() {
                    return vicmsufIni;
                }

                /**
                 * Sets vicmsuf ini.
                 *
                 * @param value the value
                 */
                public void setVICMSUFIni(String value) {
                    this.vicmsufIni = value;
                }

            }

        }


        /**
         * The type Inf c te norm.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "infCarga",
                "infDoc",
                "docAnt",
                "infModal",
                "veicNovos",
                "cobr",
                "infCteSub",
                "infGlobalizado",
                "infServVinc"
        })
        public static class InfCTeNorm {

            /**
             * The Inf carga.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TCTe.InfCte.InfCTeNorm.InfCarga infCarga;
            /**
             * The Inf doc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.InfDoc infDoc;
            /**
             * The Doc ant.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.DocAnt docAnt;
            /**
             * The Inf modal.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TCTe.InfCte.InfCTeNorm.InfModal infModal;
            /**
             * The Veic novos.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<VeicNovos> veicNovos;
            /**
             * The Cobr.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.Cobr cobr;
            /**
             * The Inf cte sub.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.InfCteSub infCteSub;
            /**
             * The Inf globalizado.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.InfGlobalizado infGlobalizado;
            /**
             * The Inf serv vinc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTe.InfCte.InfCTeNorm.InfServVinc infServVinc;

            /**
             * Gets inf carga.
             *
             * @return the inf carga
             */
            public TCTe.InfCte.InfCTeNorm.InfCarga getInfCarga() {
                return infCarga;
            }

            /**
             * Sets inf carga.
             *
             * @param value the value
             */
            public void setInfCarga(TCTe.InfCte.InfCTeNorm.InfCarga value) {
                this.infCarga = value;
            }

            /**
             * Gets inf doc.
             *
             * @return the inf doc
             */
            public TCTe.InfCte.InfCTeNorm.InfDoc getInfDoc() {
                return infDoc;
            }

            /**
             * Sets inf doc.
             *
             * @param value the value
             */
            public void setInfDoc(TCTe.InfCte.InfCTeNorm.InfDoc value) {
                this.infDoc = value;
            }

            /**
             * Gets doc ant.
             *
             * @return the doc ant
             */
            public TCTe.InfCte.InfCTeNorm.DocAnt getDocAnt() {
                return docAnt;
            }

            /**
             * Sets doc ant.
             *
             * @param value the value
             */
            public void setDocAnt(TCTe.InfCte.InfCTeNorm.DocAnt value) {
                this.docAnt = value;
            }

            /**
             * Gets inf modal.
             *
             * @return the inf modal
             */
            public TCTe.InfCte.InfCTeNorm.InfModal getInfModal() {
                return infModal;
            }

            /**
             * Sets inf modal.
             *
             * @param value the value
             */
            public void setInfModal(TCTe.InfCte.InfCTeNorm.InfModal value) {
                this.infModal = value;
            }

            /**
             * Gets veic novos.
             *
             * @return the veic novos
             */
            public List<VeicNovos> getVeicNovos() {
                if (veicNovos == null) {
                    veicNovos = new ArrayList<VeicNovos>();
                }
                return this.veicNovos;
            }

            /**
             * Gets cobr.
             *
             * @return the cobr
             */
            public TCTe.InfCte.InfCTeNorm.Cobr getCobr() {
                return cobr;
            }

            /**
             * Sets cobr.
             *
             * @param value the value
             */
            public void setCobr(TCTe.InfCte.InfCTeNorm.Cobr value) {
                this.cobr = value;
            }

            /**
             * Gets inf cte sub.
             *
             * @return the inf cte sub
             */
            public TCTe.InfCte.InfCTeNorm.InfCteSub getInfCteSub() {
                return infCteSub;
            }

            /**
             * Sets inf cte sub.
             *
             * @param value the value
             */
            public void setInfCteSub(TCTe.InfCte.InfCTeNorm.InfCteSub value) {
                this.infCteSub = value;
            }

            /**
             * Gets inf globalizado.
             *
             * @return the inf globalizado
             */
            public TCTe.InfCte.InfCTeNorm.InfGlobalizado getInfGlobalizado() {
                return infGlobalizado;
            }

            /**
             * Sets inf globalizado.
             *
             * @param value the value
             */
            public void setInfGlobalizado(TCTe.InfCte.InfCTeNorm.InfGlobalizado value) {
                this.infGlobalizado = value;
            }

            /**
             * Gets inf serv vinc.
             *
             * @return the inf serv vinc
             */
            public TCTe.InfCte.InfCTeNorm.InfServVinc getInfServVinc() {
                return infServVinc;
            }

            /**
             * Sets inf serv vinc.
             *
             * @param value the value
             */
            public void setInfServVinc(TCTe.InfCte.InfCTeNorm.InfServVinc value) {
                this.infServVinc = value;
            }


            /**
             * The type Cobr.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "fat",
                    "dup"
            })
            public static class Cobr {

                /**
                 * The Fat.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.InfCTeNorm.Cobr.Fat fat;
                /**
                 * The Dup.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<Dup> dup;

                /**
                 * Gets fat.
                 *
                 * @return the fat
                 */
                public TCTe.InfCte.InfCTeNorm.Cobr.Fat getFat() {
                    return fat;
                }

                /**
                 * Sets fat.
                 *
                 * @param value the value
                 */
                public void setFat(TCTe.InfCte.InfCTeNorm.Cobr.Fat value) {
                    this.fat = value;
                }

                /**
                 * Gets dup.
                 *
                 * @return the dup
                 */
                public List<Dup> getDup() {
                    if (dup == null) {
                        dup = new ArrayList<Dup>();
                    }
                    return this.dup;
                }


                /**
                 * The type Dup.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "nDup",
                        "dVenc",
                        "vDup"
                })
                public static class Dup {

                    /**
                     * The N dup.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nDup;
                    /**
                     * The D venc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dVenc;
                    /**
                     * The V dup.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vDup;

                    /**
                     * Gets n dup.
                     *
                     * @return the n dup
                     */
                    public String getNDup() {
                        return nDup;
                    }

                    /**
                     * Sets n dup.
                     *
                     * @param value the value
                     */
                    public void setNDup(String value) {
                        this.nDup = value;
                    }

                    /**
                     * Gets d venc.
                     *
                     * @return the d venc
                     */
                    public String getDVenc() {
                        return dVenc;
                    }

                    /**
                     * Sets d venc.
                     *
                     * @param value the value
                     */
                    public void setDVenc(String value) {
                        this.dVenc = value;
                    }

                    /**
                     * Gets v dup.
                     *
                     * @return the v dup
                     */
                    public String getVDup() {
                        return vDup;
                    }

                    /**
                     * Sets v dup.
                     *
                     * @param value the value
                     */
                    public void setVDup(String value) {
                        this.vDup = value;
                    }

                }


                /**
                 * The type Fat.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "nFat",
                        "vOrig",
                        "vDesc",
                        "vLiq"
                })
                public static class Fat {

                    /**
                     * The N fat.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nFat;
                    /**
                     * The V orig.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vOrig;
                    /**
                     * The V desc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vDesc;
                    /**
                     * The V liq.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vLiq;

                    /**
                     * Gets n fat.
                     *
                     * @return the n fat
                     */
                    public String getNFat() {
                        return nFat;
                    }

                    /**
                     * Sets n fat.
                     *
                     * @param value the value
                     */
                    public void setNFat(String value) {
                        this.nFat = value;
                    }

                    /**
                     * Gets v orig.
                     *
                     * @return the v orig
                     */
                    public String getVOrig() {
                        return vOrig;
                    }

                    /**
                     * Sets v orig.
                     *
                     * @param value the value
                     */
                    public void setVOrig(String value) {
                        this.vOrig = value;
                    }

                    /**
                     * Gets v desc.
                     *
                     * @return the v desc
                     */
                    public String getVDesc() {
                        return vDesc;
                    }

                    /**
                     * Sets v desc.
                     *
                     * @param value the value
                     */
                    public void setVDesc(String value) {
                        this.vDesc = value;
                    }

                    /**
                     * Gets v liq.
                     *
                     * @return the v liq
                     */
                    public String getVLiq() {
                        return vLiq;
                    }

                    /**
                     * Sets v liq.
                     *
                     * @param value the value
                     */
                    public void setVLiq(String value) {
                        this.vLiq = value;
                    }

                }

            }


            /**
             * The type Doc ant.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "emiDocAnt"
            })
            public static class DocAnt {

                /**
                 * The Emi doc ant.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected List<EmiDocAnt> emiDocAnt;

                /**
                 * Gets emi doc ant.
                 *
                 * @return the emi doc ant
                 */
                public List<EmiDocAnt> getEmiDocAnt() {
                    if (emiDocAnt == null) {
                        emiDocAnt = new ArrayList<EmiDocAnt>();
                    }
                    return this.emiDocAnt;
                }


                /**
                 * The type Emi doc ant.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "cnpj",
                        "cpf",
                        "ie",
                        "uf",
                        "xNome",
                        "idDocAnt"
                })
                public static class EmiDocAnt {

                    /**
                     * The Cnpj.
                     */
                    @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String cnpj;
                    /**
                     * The Cpf.
                     */
                    @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String cpf;
                    /**
                     * The Ie.
                     */
                    @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String ie;
                    /**
                     * The Uf.
                     */
                    @XmlElement(name = "UF", namespace = "http://www.portalfiscal.inf.br/cte")
                    @XmlSchemaType(name = "string")
                    protected TUf uf;
                    /**
                     * The X nome.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String xNome;
                    /**
                     * The Id doc ant.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected List<IdDocAnt> idDocAnt;

                    /**
                     * Gets cnpj.
                     *
                     * @return the cnpj
                     */
                    public String getCNPJ() {
                        return cnpj;
                    }

                    /**
                     * Sets cnpj.
                     *
                     * @param value the value
                     */
                    public void setCNPJ(String value) {
                        this.cnpj = value;
                    }

                    /**
                     * Gets cpf.
                     *
                     * @return the cpf
                     */
                    public String getCPF() {
                        return cpf;
                    }

                    /**
                     * Sets cpf.
                     *
                     * @param value the value
                     */
                    public void setCPF(String value) {
                        this.cpf = value;
                    }

                    /**
                     * Gets ie.
                     *
                     * @return the ie
                     */
                    public String getIE() {
                        return ie;
                    }

                    /**
                     * Sets ie.
                     *
                     * @param value the value
                     */
                    public void setIE(String value) {
                        this.ie = value;
                    }

                    /**
                     * Gets uf.
                     *
                     * @return the uf
                     */
                    public TUf getUF() {
                        return uf;
                    }

                    /**
                     * Sets uf.
                     *
                     * @param value the value
                     */
                    public void setUF(TUf value) {
                        this.uf = value;
                    }

                    /**
                     * Gets x nome.
                     *
                     * @return the x nome
                     */
                    public String getXNome() {
                        return xNome;
                    }

                    /**
                     * Sets x nome.
                     *
                     * @param value the value
                     */
                    public void setXNome(String value) {
                        this.xNome = value;
                    }

                    /**
                     * Gets id doc ant.
                     *
                     * @return the id doc ant
                     */
                    public List<IdDocAnt> getIdDocAnt() {
                        if (idDocAnt == null) {
                            idDocAnt = new ArrayList<IdDocAnt>();
                        }
                        return this.idDocAnt;
                    }


                    /**
                     * The type Id doc ant.
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                            "idDocAntPap",
                            "idDocAntEle"
                    })
                    public static class IdDocAnt {

                        /**
                         * The Id doc ant pap.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                        protected List<IdDocAntPap> idDocAntPap;
                        /**
                         * The Id doc ant ele.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                        protected List<IdDocAntEle> idDocAntEle;

                        /**
                         * Gets id doc ant pap.
                         *
                         * @return the id doc ant pap
                         */
                        public List<IdDocAntPap> getIdDocAntPap() {
                            if (idDocAntPap == null) {
                                idDocAntPap = new ArrayList<IdDocAntPap>();
                            }
                            return this.idDocAntPap;
                        }

                        /**
                         * Gets id doc ant ele.
                         *
                         * @return the id doc ant ele
                         */
                        public List<IdDocAntEle> getIdDocAntEle() {
                            if (idDocAntEle == null) {
                                idDocAntEle = new ArrayList<IdDocAntEle>();
                            }
                            return this.idDocAntEle;
                        }


                        /**
                         * The type Id doc ant ele.
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                                "chCTe"
                        })
                        public static class IdDocAntEle {

                            /**
                             * The Ch c te.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                            protected String chCTe;

                            /**
                             * Gets ch c te.
                             *
                             * @return the ch c te
                             */
                            public String getChCTe() {
                                return chCTe;
                            }

                            /**
                             * Sets ch c te.
                             *
                             * @param value the value
                             */
                            public void setChCTe(String value) {
                                this.chCTe = value;
                            }

                        }


                        /**
                         * The type Id doc ant pap.
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                                "tpDoc",
                                "serie",
                                "subser",
                                "nDoc",
                                "dEmi"
                        })
                        public static class IdDocAntPap {

                            /**
                             * The Tp doc.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                            protected String tpDoc;
                            /**
                             * The Serie.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                            protected String serie;
                            /**
                             * The Subser.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                            protected String subser;
                            /**
                             * The N doc.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                            protected String nDoc;
                            /**
                             * The D emi.
                             */
                            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                            protected String dEmi;

                            /**
                             * Gets tp doc.
                             *
                             * @return the tp doc
                             */
                            public String getTpDoc() {
                                return tpDoc;
                            }

                            /**
                             * Sets tp doc.
                             *
                             * @param value the value
                             */
                            public void setTpDoc(String value) {
                                this.tpDoc = value;
                            }

                            /**
                             * Gets serie.
                             *
                             * @return the serie
                             */
                            public String getSerie() {
                                return serie;
                            }

                            /**
                             * Sets serie.
                             *
                             * @param value the value
                             */
                            public void setSerie(String value) {
                                this.serie = value;
                            }

                            /**
                             * Gets subser.
                             *
                             * @return the subser
                             */
                            public String getSubser() {
                                return subser;
                            }

                            /**
                             * Sets subser.
                             *
                             * @param value the value
                             */
                            public void setSubser(String value) {
                                this.subser = value;
                            }

                            /**
                             * Gets n doc.
                             *
                             * @return the n doc
                             */
                            public String getNDoc() {
                                return nDoc;
                            }

                            /**
                             * Sets n doc.
                             *
                             * @param value the value
                             */
                            public void setNDoc(String value) {
                                this.nDoc = value;
                            }

                            /**
                             * Gets d emi.
                             *
                             * @return the d emi
                             */
                            public String getDEmi() {
                                return dEmi;
                            }

                            /**
                             * Sets d emi.
                             *
                             * @param value the value
                             */
                            public void setDEmi(String value) {
                                this.dEmi = value;
                            }

                        }

                    }

                }

            }


            /**
             * The type Inf carga.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vCarga",
                    "proPred",
                    "xOutCat",
                    "infQ",
                    "vCargaAverb"
            })
            public static class InfCarga {

                /**
                 * The V carga.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vCarga;
                /**
                 * The Pro pred.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String proPred;
                /**
                 * The X out cat.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xOutCat;
                /**
                 * The Inf q.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected List<InfQ> infQ;
                /**
                 * The V carga averb.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vCargaAverb;

                /**
                 * Gets v carga.
                 *
                 * @return the v carga
                 */
                public String getVCarga() {
                    return vCarga;
                }

                /**
                 * Sets v carga.
                 *
                 * @param value the value
                 */
                public void setVCarga(String value) {
                    this.vCarga = value;
                }

                /**
                 * Gets pro pred.
                 *
                 * @return the pro pred
                 */
                public String getProPred() {
                    return proPred;
                }

                /**
                 * Sets pro pred.
                 *
                 * @param value the value
                 */
                public void setProPred(String value) {
                    this.proPred = value;
                }

                /**
                 * Gets x out cat.
                 *
                 * @return the x out cat
                 */
                public String getXOutCat() {
                    return xOutCat;
                }

                /**
                 * Sets x out cat.
                 *
                 * @param value the value
                 */
                public void setXOutCat(String value) {
                    this.xOutCat = value;
                }

                /**
                 * Gets inf q.
                 *
                 * @return the inf q
                 */
                public List<InfQ> getInfQ() {
                    if (infQ == null) {
                        infQ = new ArrayList<InfQ>();
                    }
                    return this.infQ;
                }

                /**
                 * Gets v carga averb.
                 *
                 * @return the v carga averb
                 */
                public String getVCargaAverb() {
                    return vCargaAverb;
                }

                /**
                 * Sets v carga averb.
                 *
                 * @param value the value
                 */
                public void setVCargaAverb(String value) {
                    this.vCargaAverb = value;
                }


                /**
                 * The type Inf q.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "cUnid",
                        "tpMed",
                        "qCarga"
                })
                public static class InfQ {

                    /**
                     * The C unid.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String cUnid;
                    /**
                     * The Tp med.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpMed;
                    /**
                     * The Q carga.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String qCarga;

                    /**
                     * Gets c unid.
                     *
                     * @return the c unid
                     */
                    public String getCUnid() {
                        return cUnid;
                    }

                    /**
                     * Sets c unid.
                     *
                     * @param value the value
                     */
                    public void setCUnid(String value) {
                        this.cUnid = value;
                    }

                    /**
                     * Gets tp med.
                     *
                     * @return the tp med
                     */
                    public String getTpMed() {
                        return tpMed;
                    }

                    /**
                     * Sets tp med.
                     *
                     * @param value the value
                     */
                    public void setTpMed(String value) {
                        this.tpMed = value;
                    }

                    /**
                     * Gets q carga.
                     *
                     * @return the q carga
                     */
                    public String getQCarga() {
                        return qCarga;
                    }

                    /**
                     * Sets q carga.
                     *
                     * @param value the value
                     */
                    public void setQCarga(String value) {
                        this.qCarga = value;
                    }

                }

            }


            /**
             * The type Inf cte sub.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "chCte",
                    "refCteAnu",
                    "tomaICMS",
                    "indAlteraToma"
            })
            public static class InfCteSub {

                /**
                 * The Ch cte.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String chCte;
                /**
                 * The Ref cte anu.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String refCteAnu;
                /**
                 * The Toma icms.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS tomaICMS;
                /**
                 * The Ind altera toma.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String indAlteraToma;

                /**
                 * Gets ch cte.
                 *
                 * @return the ch cte
                 */
                public String getChCte() {
                    return chCte;
                }

                /**
                 * Sets ch cte.
                 *
                 * @param value the value
                 */
                public void setChCte(String value) {
                    this.chCte = value;
                }

                /**
                 * Gets ref cte anu.
                 *
                 * @return the ref cte anu
                 */
                public String getRefCteAnu() {
                    return refCteAnu;
                }

                /**
                 * Sets ref cte anu.
                 *
                 * @param value the value
                 */
                public void setRefCteAnu(String value) {
                    this.refCteAnu = value;
                }

                /**
                 * Gets toma icms.
                 *
                 * @return the toma icms
                 */
                public TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS getTomaICMS() {
                    return tomaICMS;
                }

                /**
                 * Sets toma icms.
                 *
                 * @param value the value
                 */
                public void setTomaICMS(TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS value) {
                    this.tomaICMS = value;
                }

                /**
                 * Gets ind altera toma.
                 *
                 * @return the ind altera toma
                 */
                public String getIndAlteraToma() {
                    return indAlteraToma;
                }

                /**
                 * Sets ind altera toma.
                 *
                 * @param value the value
                 */
                public void setIndAlteraToma(String value) {
                    this.indAlteraToma = value;
                }


                /**
                 * The type Toma icms.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "refNFe",
                        "refNF",
                        "refCte"
                })
                public static class TomaICMS {

                    /**
                     * The Ref n fe.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String refNFe;
                    /**
                     * The Ref nf.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF refNF;
                    /**
                     * The Ref cte.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String refCte;

                    /**
                     * Gets ref n fe.
                     *
                     * @return the ref n fe
                     */
                    public String getRefNFe() {
                        return refNFe;
                    }

                    /**
                     * Sets ref n fe.
                     *
                     * @param value the value
                     */
                    public void setRefNFe(String value) {
                        this.refNFe = value;
                    }

                    /**
                     * Gets ref nf.
                     *
                     * @return the ref nf
                     */
                    public TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF getRefNF() {
                        return refNF;
                    }

                    /**
                     * Sets ref nf.
                     *
                     * @param value the value
                     */
                    public void setRefNF(TCTe.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF value) {
                        this.refNF = value;
                    }

                    /**
                     * Gets ref cte.
                     *
                     * @return the ref cte
                     */
                    public String getRefCte() {
                        return refCte;
                    }

                    /**
                     * Sets ref cte.
                     *
                     * @param value the value
                     */
                    public void setRefCte(String value) {
                        this.refCte = value;
                    }


                    /**
                     * The type Ref nf.
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                            "cnpj",
                            "cpf",
                            "mod",
                            "serie",
                            "subserie",
                            "nro",
                            "valor",
                            "dEmi"
                    })
                    public static class RefNF {

                        /**
                         * The Cnpj.
                         */
                        @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String cnpj;
                        /**
                         * The Cpf.
                         */
                        @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String cpf;
                        /**
                         * The Mod.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String mod;
                        /**
                         * The Serie.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String serie;
                        /**
                         * The Subserie.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String subserie;
                        /**
                         * The Nro.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String nro;
                        /**
                         * The Valor.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String valor;
                        /**
                         * The D emi.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String dEmi;

                        /**
                         * Gets cnpj.
                         *
                         * @return the cnpj
                         */
                        public String getCNPJ() {
                            return cnpj;
                        }

                        /**
                         * Sets cnpj.
                         *
                         * @param value the value
                         */
                        public void setCNPJ(String value) {
                            this.cnpj = value;
                        }

                        /**
                         * Gets cpf.
                         *
                         * @return the cpf
                         */
                        public String getCPF() {
                            return cpf;
                        }

                        /**
                         * Sets cpf.
                         *
                         * @param value the value
                         */
                        public void setCPF(String value) {
                            this.cpf = value;
                        }

                        /**
                         * Gets mod.
                         *
                         * @return the mod
                         */
                        public String getMod() {
                            return mod;
                        }

                        /**
                         * Sets mod.
                         *
                         * @param value the value
                         */
                        public void setMod(String value) {
                            this.mod = value;
                        }

                        /**
                         * Gets serie.
                         *
                         * @return the serie
                         */
                        public String getSerie() {
                            return serie;
                        }

                        /**
                         * Sets serie.
                         *
                         * @param value the value
                         */
                        public void setSerie(String value) {
                            this.serie = value;
                        }

                        /**
                         * Gets subserie.
                         *
                         * @return the subserie
                         */
                        public String getSubserie() {
                            return subserie;
                        }

                        /**
                         * Sets subserie.
                         *
                         * @param value the value
                         */
                        public void setSubserie(String value) {
                            this.subserie = value;
                        }

                        /**
                         * Gets nro.
                         *
                         * @return the nro
                         */
                        public String getNro() {
                            return nro;
                        }

                        /**
                         * Sets nro.
                         *
                         * @param value the value
                         */
                        public void setNro(String value) {
                            this.nro = value;
                        }

                        /**
                         * Gets valor.
                         *
                         * @return the valor
                         */
                        public String getValor() {
                            return valor;
                        }

                        /**
                         * Sets valor.
                         *
                         * @param value the value
                         */
                        public void setValor(String value) {
                            this.valor = value;
                        }

                        /**
                         * Gets d emi.
                         *
                         * @return the d emi
                         */
                        public String getDEmi() {
                            return dEmi;
                        }

                        /**
                         * Sets d emi.
                         *
                         * @param value the value
                         */
                        public void setDEmi(String value) {
                            this.dEmi = value;
                        }

                    }

                }

            }


            /**
             * The type Inf doc.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "infNF",
                    "infNFe",
                    "infOutros"
            })
            public static class InfDoc {

                /**
                 * The Inf nf.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<InfNF> infNF;
                /**
                 * The Inf n fe.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<InfNFe> infNFe;
                /**
                 * The Inf outros.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<InfOutros> infOutros;

                /**
                 * Gets inf nf.
                 *
                 * @return the inf nf
                 */
                public List<InfNF> getInfNF() {
                    if (infNF == null) {
                        infNF = new ArrayList<InfNF>();
                    }
                    return this.infNF;
                }

                /**
                 * Gets inf n fe.
                 *
                 * @return the inf n fe
                 */
                public List<InfNFe> getInfNFe() {
                    if (infNFe == null) {
                        infNFe = new ArrayList<InfNFe>();
                    }
                    return this.infNFe;
                }

                /**
                 * Gets inf outros.
                 *
                 * @return the inf outros
                 */
                public List<InfOutros> getInfOutros() {
                    if (infOutros == null) {
                        infOutros = new ArrayList<InfOutros>();
                    }
                    return this.infOutros;
                }


                /**
                 * The type Inf nf.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "nRoma",
                        "nPed",
                        "mod",
                        "serie",
                        "nDoc",
                        "dEmi",
                        "vbc",
                        "vicms",
                        "vbcst",
                        "vst",
                        "vProd",
                        "vnf",
                        "ncfop",
                        "nPeso",
                        "pin",
                        "dPrev",
                        "infUnidCarga",
                        "infUnidTransp"
                })
                public static class InfNF {

                    /**
                     * The N roma.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nRoma;
                    /**
                     * The N ped.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nPed;
                    /**
                     * The Mod.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String mod;
                    /**
                     * The Serie.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String serie;
                    /**
                     * The N doc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String nDoc;
                    /**
                     * The D emi.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String dEmi;
                    /**
                     * The Vbc.
                     */
                    @XmlElement(name = "vBC", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vbc;
                    /**
                     * The Vicms.
                     */
                    @XmlElement(name = "vICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vicms;
                    /**
                     * The Vbcst.
                     */
                    @XmlElement(name = "vBCST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vbcst;
                    /**
                     * The Vst.
                     */
                    @XmlElement(name = "vST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vst;
                    /**
                     * The V prod.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vProd;
                    /**
                     * The Vnf.
                     */
                    @XmlElement(name = "vNF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String vnf;
                    /**
                     * The Ncfop.
                     */
                    @XmlElement(name = "nCFOP", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String ncfop;
                    /**
                     * The N peso.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nPeso;
                    /**
                     * The Pin.
                     */
                    @XmlElement(name = "PIN", namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String pin;
                    /**
                     * The D prev.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dPrev;
                    /**
                     * The Inf unid carga.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidCarga> infUnidCarga;
                    /**
                     * The Inf unid transp.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidadeTransp> infUnidTransp;

                    /**
                     * Gets n roma.
                     *
                     * @return the n roma
                     */
                    public String getNRoma() {
                        return nRoma;
                    }

                    /**
                     * Sets n roma.
                     *
                     * @param value the value
                     */
                    public void setNRoma(String value) {
                        this.nRoma = value;
                    }

                    /**
                     * Gets n ped.
                     *
                     * @return the n ped
                     */
                    public String getNPed() {
                        return nPed;
                    }

                    /**
                     * Sets n ped.
                     *
                     * @param value the value
                     */
                    public void setNPed(String value) {
                        this.nPed = value;
                    }

                    /**
                     * Gets mod.
                     *
                     * @return the mod
                     */
                    public String getMod() {
                        return mod;
                    }

                    /**
                     * Sets mod.
                     *
                     * @param value the value
                     */
                    public void setMod(String value) {
                        this.mod = value;
                    }

                    /**
                     * Gets serie.
                     *
                     * @return the serie
                     */
                    public String getSerie() {
                        return serie;
                    }

                    /**
                     * Sets serie.
                     *
                     * @param value the value
                     */
                    public void setSerie(String value) {
                        this.serie = value;
                    }

                    /**
                     * Gets n doc.
                     *
                     * @return the n doc
                     */
                    public String getNDoc() {
                        return nDoc;
                    }

                    /**
                     * Sets n doc.
                     *
                     * @param value the value
                     */
                    public void setNDoc(String value) {
                        this.nDoc = value;
                    }

                    /**
                     * Gets d emi.
                     *
                     * @return the d emi
                     */
                    public String getDEmi() {
                        return dEmi;
                    }

                    /**
                     * Sets d emi.
                     *
                     * @param value the value
                     */
                    public void setDEmi(String value) {
                        this.dEmi = value;
                    }

                    /**
                     * Gets vbc.
                     *
                     * @return the vbc
                     */
                    public String getVBC() {
                        return vbc;
                    }

                    /**
                     * Sets vbc.
                     *
                     * @param value the value
                     */
                    public void setVBC(String value) {
                        this.vbc = value;
                    }

                    /**
                     * Gets vicms.
                     *
                     * @return the vicms
                     */
                    public String getVICMS() {
                        return vicms;
                    }

                    /**
                     * Sets vicms.
                     *
                     * @param value the value
                     */
                    public void setVICMS(String value) {
                        this.vicms = value;
                    }

                    /**
                     * Gets vbcst.
                     *
                     * @return the vbcst
                     */
                    public String getVBCST() {
                        return vbcst;
                    }

                    /**
                     * Sets vbcst.
                     *
                     * @param value the value
                     */
                    public void setVBCST(String value) {
                        this.vbcst = value;
                    }

                    /**
                     * Gets vst.
                     *
                     * @return the vst
                     */
                    public String getVST() {
                        return vst;
                    }

                    /**
                     * Sets vst.
                     *
                     * @param value the value
                     */
                    public void setVST(String value) {
                        this.vst = value;
                    }

                    /**
                     * Gets v prod.
                     *
                     * @return the v prod
                     */
                    public String getVProd() {
                        return vProd;
                    }

                    /**
                     * Sets v prod.
                     *
                     * @param value the value
                     */
                    public void setVProd(String value) {
                        this.vProd = value;
                    }

                    /**
                     * Gets vnf.
                     *
                     * @return the vnf
                     */
                    public String getVNF() {
                        return vnf;
                    }

                    /**
                     * Sets vnf.
                     *
                     * @param value the value
                     */
                    public void setVNF(String value) {
                        this.vnf = value;
                    }

                    /**
                     * Gets ncfop.
                     *
                     * @return the ncfop
                     */
                    public String getNCFOP() {
                        return ncfop;
                    }

                    /**
                     * Sets ncfop.
                     *
                     * @param value the value
                     */
                    public void setNCFOP(String value) {
                        this.ncfop = value;
                    }

                    /**
                     * Gets n peso.
                     *
                     * @return the n peso
                     */
                    public String getNPeso() {
                        return nPeso;
                    }

                    /**
                     * Sets n peso.
                     *
                     * @param value the value
                     */
                    public void setNPeso(String value) {
                        this.nPeso = value;
                    }

                    /**
                     * Gets pin.
                     *
                     * @return the pin
                     */
                    public String getPIN() {
                        return pin;
                    }

                    /**
                     * Sets pin.
                     *
                     * @param value the value
                     */
                    public void setPIN(String value) {
                        this.pin = value;
                    }

                    /**
                     * Gets d prev.
                     *
                     * @return the d prev
                     */
                    public String getDPrev() {
                        return dPrev;
                    }

                    /**
                     * Sets d prev.
                     *
                     * @param value the value
                     */
                    public void setDPrev(String value) {
                        this.dPrev = value;
                    }

                    /**
                     * Gets inf unid carga.
                     *
                     * @return the inf unid carga
                     */
                    public List<TUnidCarga> getInfUnidCarga() {
                        if (infUnidCarga == null) {
                            infUnidCarga = new ArrayList<TUnidCarga>();
                        }
                        return this.infUnidCarga;
                    }

                    /**
                     * Gets inf unid transp.
                     *
                     * @return the inf unid transp
                     */
                    public List<TUnidadeTransp> getInfUnidTransp() {
                        if (infUnidTransp == null) {
                            infUnidTransp = new ArrayList<TUnidadeTransp>();
                        }
                        return this.infUnidTransp;
                    }

                }


                /**
                 * The type Inf n fe.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "chave",
                        "pin",
                        "dPrev",
                        "infUnidCarga",
                        "infUnidTransp"
                })
                public static class InfNFe {

                    /**
                     * The Chave.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String chave;
                    /**
                     * The Pin.
                     */
                    @XmlElement(name = "PIN", namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String pin;
                    /**
                     * The D prev.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dPrev;
                    /**
                     * The Inf unid carga.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidCarga> infUnidCarga;
                    /**
                     * The Inf unid transp.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidadeTransp> infUnidTransp;

                    /**
                     * Gets chave.
                     *
                     * @return the chave
                     */
                    public String getChave() {
                        return chave;
                    }

                    /**
                     * Sets chave.
                     *
                     * @param value the value
                     */
                    public void setChave(String value) {
                        this.chave = value;
                    }

                    /**
                     * Gets pin.
                     *
                     * @return the pin
                     */
                    public String getPIN() {
                        return pin;
                    }

                    /**
                     * Sets pin.
                     *
                     * @param value the value
                     */
                    public void setPIN(String value) {
                        this.pin = value;
                    }

                    /**
                     * Gets d prev.
                     *
                     * @return the d prev
                     */
                    public String getDPrev() {
                        return dPrev;
                    }

                    /**
                     * Sets d prev.
                     *
                     * @param value the value
                     */
                    public void setDPrev(String value) {
                        this.dPrev = value;
                    }

                    /**
                     * Gets inf unid carga.
                     *
                     * @return the inf unid carga
                     */
                    public List<TUnidCarga> getInfUnidCarga() {
                        if (infUnidCarga == null) {
                            infUnidCarga = new ArrayList<TUnidCarga>();
                        }
                        return this.infUnidCarga;
                    }

                    /**
                     * Gets inf unid transp.
                     *
                     * @return the inf unid transp
                     */
                    public List<TUnidadeTransp> getInfUnidTransp() {
                        if (infUnidTransp == null) {
                            infUnidTransp = new ArrayList<TUnidadeTransp>();
                        }
                        return this.infUnidTransp;
                    }

                }


                /**
                 * The type Inf outros.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "tpDoc",
                        "descOutros",
                        "nDoc",
                        "dEmi",
                        "vDocFisc",
                        "dPrev",
                        "infUnidCarga",
                        "infUnidTransp"
                })
                public static class InfOutros {

                    /**
                     * The Tp doc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String tpDoc;
                    /**
                     * The Desc outros.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String descOutros;
                    /**
                     * The N doc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nDoc;
                    /**
                     * The D emi.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dEmi;
                    /**
                     * The V doc fisc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vDocFisc;
                    /**
                     * The D prev.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dPrev;
                    /**
                     * The Inf unid carga.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidCarga> infUnidCarga;
                    /**
                     * The Inf unid transp.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected List<TUnidadeTransp> infUnidTransp;

                    /**
                     * Gets tp doc.
                     *
                     * @return the tp doc
                     */
                    public String getTpDoc() {
                        return tpDoc;
                    }

                    /**
                     * Sets tp doc.
                     *
                     * @param value the value
                     */
                    public void setTpDoc(String value) {
                        this.tpDoc = value;
                    }

                    /**
                     * Gets desc outros.
                     *
                     * @return the desc outros
                     */
                    public String getDescOutros() {
                        return descOutros;
                    }

                    /**
                     * Sets desc outros.
                     *
                     * @param value the value
                     */
                    public void setDescOutros(String value) {
                        this.descOutros = value;
                    }

                    /**
                     * Gets n doc.
                     *
                     * @return the n doc
                     */
                    public String getNDoc() {
                        return nDoc;
                    }

                    /**
                     * Sets n doc.
                     *
                     * @param value the value
                     */
                    public void setNDoc(String value) {
                        this.nDoc = value;
                    }

                    /**
                     * Gets d emi.
                     *
                     * @return the d emi
                     */
                    public String getDEmi() {
                        return dEmi;
                    }

                    /**
                     * Sets d emi.
                     *
                     * @param value the value
                     */
                    public void setDEmi(String value) {
                        this.dEmi = value;
                    }

                    /**
                     * Gets v doc fisc.
                     *
                     * @return the v doc fisc
                     */
                    public String getVDocFisc() {
                        return vDocFisc;
                    }

                    /**
                     * Sets v doc fisc.
                     *
                     * @param value the value
                     */
                    public void setVDocFisc(String value) {
                        this.vDocFisc = value;
                    }

                    /**
                     * Gets d prev.
                     *
                     * @return the d prev
                     */
                    public String getDPrev() {
                        return dPrev;
                    }

                    /**
                     * Sets d prev.
                     *
                     * @param value the value
                     */
                    public void setDPrev(String value) {
                        this.dPrev = value;
                    }

                    /**
                     * Gets inf unid carga.
                     *
                     * @return the inf unid carga
                     */
                    public List<TUnidCarga> getInfUnidCarga() {
                        if (infUnidCarga == null) {
                            infUnidCarga = new ArrayList<TUnidCarga>();
                        }
                        return this.infUnidCarga;
                    }

                    /**
                     * Gets inf unid transp.
                     *
                     * @return the inf unid transp
                     */
                    public List<TUnidadeTransp> getInfUnidTransp() {
                        if (infUnidTransp == null) {
                            infUnidTransp = new ArrayList<TUnidadeTransp>();
                        }
                        return this.infUnidTransp;
                    }

                }

            }


            /**
             * The type Inf globalizado.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xObs"
            })
            public static class InfGlobalizado {

                /**
                 * The X obs.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xObs;

                /**
                 * Gets x obs.
                 *
                 * @return the x obs
                 */
                public String getXObs() {
                    return xObs;
                }

                /**
                 * Sets x obs.
                 *
                 * @param value the value
                 */
                public void setXObs(String value) {
                    this.xObs = value;
                }

            }


            /**
             * The type Inf modal.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "any"
            })
            public static class InfModal {

                /**
                 * The Any.
                 */
                @XmlAnyElement
                protected Element any;
                /**
                 * The Versao modal.
                 */
                @XmlAttribute(name = "versaoModal", required = true)
                protected String versaoModal;

                /**
                 * Gets any.
                 *
                 * @return the any
                 */
                public Element getAny() {
                    return any;
                }

                /**
                 * Sets any.
                 *
                 * @param value the value
                 */
                public void setAny(Element value) {
                    this.any = value;
                }

                /**
                 * Gets versao modal.
                 *
                 * @return the versao modal
                 */
                public String getVersaoModal() {
                    return versaoModal;
                }

                /**
                 * Sets versao modal.
                 *
                 * @param value the value
                 */
                public void setVersaoModal(String value) {
                    this.versaoModal = value;
                }

            }


            /**
             * The type Inf serv vinc.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "infCTeMultimodal"
            })
            public static class InfServVinc {

                /**
                 * The Inf c te multimodal.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected List<InfCTeMultimodal> infCTeMultimodal;

                /**
                 * Gets inf c te multimodal.
                 *
                 * @return the inf c te multimodal
                 */
                public List<InfCTeMultimodal> getInfCTeMultimodal() {
                    if (infCTeMultimodal == null) {
                        infCTeMultimodal = new ArrayList<InfCTeMultimodal>();
                    }
                    return this.infCTeMultimodal;
                }


                /**
                 * The type Inf c te multimodal.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "chCTeMultimodal"
                })
                public static class InfCTeMultimodal {

                    /**
                     * The Ch c te multimodal.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String chCTeMultimodal;

                    /**
                     * Gets ch c te multimodal.
                     *
                     * @return the ch c te multimodal
                     */
                    public String getChCTeMultimodal() {
                        return chCTeMultimodal;
                    }

                    /**
                     * Sets ch c te multimodal.
                     *
                     * @param value the value
                     */
                    public void setChCTeMultimodal(String value) {
                        this.chCTeMultimodal = value;
                    }

                }

            }


            /**
             * The type Veic novos.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "chassi",
                    "cCor",
                    "xCor",
                    "cMod",
                    "vUnit",
                    "vFrete"
            })
            public static class VeicNovos {

                /**
                 * The Chassi.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String chassi;
                /**
                 * The C cor.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String cCor;
                /**
                 * The X cor.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xCor;
                /**
                 * The C mod.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String cMod;
                /**
                 * The V unit.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vUnit;
                /**
                 * The V frete.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vFrete;

                /**
                 * Gets chassi.
                 *
                 * @return the chassi
                 */
                public String getChassi() {
                    return chassi;
                }

                /**
                 * Sets chassi.
                 *
                 * @param value the value
                 */
                public void setChassi(String value) {
                    this.chassi = value;
                }

                /**
                 * Gets c cor.
                 *
                 * @return the c cor
                 */
                public String getCCor() {
                    return cCor;
                }

                /**
                 * Sets c cor.
                 *
                 * @param value the value
                 */
                public void setCCor(String value) {
                    this.cCor = value;
                }

                /**
                 * Gets x cor.
                 *
                 * @return the x cor
                 */
                public String getXCor() {
                    return xCor;
                }

                /**
                 * Sets x cor.
                 *
                 * @param value the value
                 */
                public void setXCor(String value) {
                    this.xCor = value;
                }

                /**
                 * Gets c mod.
                 *
                 * @return the c mod
                 */
                public String getCMod() {
                    return cMod;
                }

                /**
                 * Sets c mod.
                 *
                 * @param value the value
                 */
                public void setCMod(String value) {
                    this.cMod = value;
                }

                /**
                 * Gets v unit.
                 *
                 * @return the v unit
                 */
                public String getVUnit() {
                    return vUnit;
                }

                /**
                 * Sets v unit.
                 *
                 * @param value the value
                 */
                public void setVUnit(String value) {
                    this.vUnit = value;
                }

                /**
                 * Gets v frete.
                 *
                 * @return the v frete
                 */
                public String getVFrete() {
                    return vFrete;
                }

                /**
                 * Sets v frete.
                 *
                 * @param value the value
                 */
                public void setVFrete(String value) {
                    this.vFrete = value;
                }

            }

        }


        /**
         * The type Inf cte anu.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "chCte",
                "dEmi"
        })
        public static class InfCteAnu {

            /**
             * The Ch cte.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String chCte;
            /**
             * The D emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String dEmi;

            /**
             * Gets ch cte.
             *
             * @return the ch cte
             */
            public String getChCte() {
                return chCte;
            }

            /**
             * Sets ch cte.
             *
             * @param value the value
             */
            public void setChCte(String value) {
                this.chCte = value;
            }

            /**
             * Gets d emi.
             *
             * @return the d emi
             */
            public String getDEmi() {
                return dEmi;
            }

            /**
             * Sets d emi.
             *
             * @param value the value
             */
            public void setDEmi(String value) {
                this.dEmi = value;
            }

        }


        /**
         * The type Inf cte comp.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "chCTe"
        })
        public static class InfCteComp {

            /**
             * The Ch c te.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String chCTe;

            /**
             * Gets ch c te.
             *
             * @return the ch c te
             */
            public String getChCTe() {
                return chCTe;
            }

            /**
             * Sets ch c te.
             *
             * @param value the value
             */
            public void setChCTe(String value) {
                this.chCTe = value;
            }

        }


        /**
         * The type Receb.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf",
                "ie",
                "xNome",
                "fone",
                "enderReceb",
                "email"
        })
        public static class Receb {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String ie;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;
            /**
             * The Ender receb.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndereco enderReceb;
            /**
             * The Email.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String email;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

            /**
             * Gets ender receb.
             *
             * @return the ender receb
             */
            public TEndereco getEnderReceb() {
                return enderReceb;
            }

            /**
             * Sets ender receb.
             *
             * @param value the value
             */
            public void setEnderReceb(TEndereco value) {
                this.enderReceb = value;
            }

            /**
             * Gets email.
             *
             * @return the email
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets email.
             *
             * @param value the value
             */
            public void setEmail(String value) {
                this.email = value;
            }

        }


        /**
         * The type Rem.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf",
                "ie",
                "xNome",
                "xFant",
                "fone",
                "enderReme",
                "email"
        })
        public static class Rem {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String ie;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The X fant.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xFant;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;
            /**
             * The Ender reme.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndereco enderReme;
            /**
             * The Email.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String email;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets x fant.
             *
             * @return the x fant
             */
            public String getXFant() {
                return xFant;
            }

            /**
             * Sets x fant.
             *
             * @param value the value
             */
            public void setXFant(String value) {
                this.xFant = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

            /**
             * Gets ender reme.
             *
             * @return the ender reme
             */
            public TEndereco getEnderReme() {
                return enderReme;
            }

            /**
             * Sets ender reme.
             *
             * @param value the value
             */
            public void setEnderReme(TEndereco value) {
                this.enderReme = value;
            }

            /**
             * Gets email.
             *
             * @return the email
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets email.
             *
             * @param value the value
             */
            public void setEmail(String value) {
                this.email = value;
            }

        }


        /**
         * The type V prest.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "vtPrest",
                "vRec",
                "comp"
        })
        public static class VPrest {

            /**
             * The Vt prest.
             */
            @XmlElement(name = "vTPrest", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String vtPrest;
            /**
             * The V rec.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String vRec;
            /**
             * The Comp.
             */
            @XmlElement(name = "Comp", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<Comp> comp;

            /**
             * Gets vt prest.
             *
             * @return the vt prest
             */
            public String getVTPrest() {
                return vtPrest;
            }

            /**
             * Sets vt prest.
             *
             * @param value the value
             */
            public void setVTPrest(String value) {
                this.vtPrest = value;
            }

            /**
             * Gets v rec.
             *
             * @return the v rec
             */
            public String getVRec() {
                return vRec;
            }

            /**
             * Sets v rec.
             *
             * @param value the value
             */
            public void setVRec(String value) {
                this.vRec = value;
            }

            /**
             * Gets comp.
             *
             * @return the comp
             */
            public List<Comp> getComp() {
                if (comp == null) {
                    comp = new ArrayList<Comp>();
                }
                return this.comp;
            }


            /**
             * The type Comp.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xNome",
                    "vComp"
            })
            public static class Comp {

                /**
                 * The X nome.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xNome;
                /**
                 * The V comp.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vComp;

                /**
                 * Gets x nome.
                 *
                 * @return the x nome
                 */
                public String getXNome() {
                    return xNome;
                }

                /**
                 * Sets x nome.
                 *
                 * @param value the value
                 */
                public void setXNome(String value) {
                    this.xNome = value;
                }

                /**
                 * Gets v comp.
                 *
                 * @return the v comp
                 */
                public String getVComp() {
                    return vComp;
                }

                /**
                 * Sets v comp.
                 *
                 * @param value the value
                 */
                public void setVComp(String value) {
                    this.vComp = value;
                }

            }

        }

    }

}
