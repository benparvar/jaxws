//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The type T envi c te.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TEnviCTe", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "idLote",
        "cTe"
})
public class TEnviCTe {

    /**
     * The Id lote.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String idLote;
    /**
     * The C te.
     */
    @XmlElement(name = "CTe", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected List<TCTe> cTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets id lote.
     *
     * @return the id lote
     */
    public String getIdLote() {
        return idLote;
    }

    /**
     * Sets id lote.
     *
     * @param value the value
     */
    public void setIdLote(String value) {
        this.idLote = value;
    }

    /**
     * Gets c te.
     *
     * @return the c te
     */
    public List<TCTe> getCTe() {
        if (cTe == null) {
            cTe = new ArrayList<TCTe>();
        }
        return this.cTe;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
