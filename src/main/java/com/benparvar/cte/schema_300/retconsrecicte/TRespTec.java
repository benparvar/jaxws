//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The type T resp tec.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRespTec", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "cnpj",
        "xContato",
        "email",
        "fone",
        "idCSRT",
        "hashCSRT"
})
public class TRespTec {

    /**
     * The Cnpj.
     */
    @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cnpj;
    /**
     * The X contato.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xContato;
    /**
     * The Email.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String email;
    /**
     * The Fone.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String fone;
    /**
     * The Id csrt.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String idCSRT;
    /**
     * The Hash csrt.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected byte[] hashCSRT;

    /**
     * Gets cnpj.
     *
     * @return the cnpj
     */
    public String getCNPJ() {
        return cnpj;
    }

    /**
     * Sets cnpj.
     *
     * @param value the value
     */
    public void setCNPJ(String value) {
        this.cnpj = value;
    }

    /**
     * Gets x contato.
     *
     * @return the x contato
     */
    public String getXContato() {
        return xContato;
    }

    /**
     * Sets x contato.
     *
     * @param value the value
     */
    public void setXContato(String value) {
        this.xContato = value;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param value the value
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets fone.
     *
     * @return the fone
     */
    public String getFone() {
        return fone;
    }

    /**
     * Sets fone.
     *
     * @param value the value
     */
    public void setFone(String value) {
        this.fone = value;
    }

    /**
     * Gets id csrt.
     *
     * @return the id csrt
     */
    public String getIdCSRT() {
        return idCSRT;
    }

    /**
     * Sets id csrt.
     *
     * @param value the value
     */
    public void setIdCSRT(String value) {
        this.idCSRT = value;
    }

    /**
     * Get hash csrt byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getHashCSRT() {
        return hashCSRT;
    }

    /**
     * Sets hash csrt.
     *
     * @param value the value
     */
    public void setHashCSRT(byte[] value) {
        this.hashCSRT = value;
    }

}
