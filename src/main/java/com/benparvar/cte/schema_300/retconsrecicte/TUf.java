//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * The enum T uf.
 */
@XmlType(name = "TUf", namespace = "http://www.portalfiscal.inf.br/cte")
@XmlEnum
public enum TUf {

    /**
     * Ac t uf.
     */
    AC,
    /**
     * Al t uf.
     */
    AL,
    /**
     * Am t uf.
     */
    AM,
    /**
     * Ap t uf.
     */
    AP,
    /**
     * Ba t uf.
     */
    BA,
    /**
     * Ce t uf.
     */
    CE,
    /**
     * Df t uf.
     */
    DF,
    /**
     * Es t uf.
     */
    ES,
    /**
     * Go t uf.
     */
    GO,
    /**
     * Ma t uf.
     */
    MA,
    /**
     * Mg t uf.
     */
    MG,
    /**
     * Ms t uf.
     */
    MS,
    /**
     * Mt t uf.
     */
    MT,
    /**
     * Pa t uf.
     */
    PA,
    /**
     * Pb t uf.
     */
    PB,
    /**
     * Pe t uf.
     */
    PE,
    /**
     * Pi t uf.
     */
    PI,
    /**
     * Pr t uf.
     */
    PR,
    /**
     * Rj t uf.
     */
    RJ,
    /**
     * Rn t uf.
     */
    RN,
    /**
     * Ro t uf.
     */
    RO,
    /**
     * Rr t uf.
     */
    RR,
    /**
     * Rs t uf.
     */
    RS,
    /**
     * Sc t uf.
     */
    SC,
    /**
     * Se t uf.
     */
    SE,
    /**
     * Sp t uf.
     */
    SP,
    /**
     * To t uf.
     */
    TO,
    /**
     * Ex t uf.
     */
    EX;

    /**
     * From value t uf.
     *
     * @param v the v
     * @return the t uf
     */
    public static TUf fromValue(String v) {
        return valueOf(v);
    }

    /**
     * Value string.
     *
     * @return the string
     */
    public String value() {
        return name();
    }

}
