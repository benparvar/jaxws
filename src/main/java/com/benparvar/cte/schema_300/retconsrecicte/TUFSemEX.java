//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:54:47 PM BRT 
//


package com.benparvar.cte.schema_300.retconsrecicte;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * The enum Tuf sem ex.
 */
@XmlType(name = "TUF_sem_EX", namespace = "http://www.portalfiscal.inf.br/cte")
@XmlEnum
public enum TUFSemEX {

    /**
     * Ac tuf sem ex.
     */
    AC,
    /**
     * Al tuf sem ex.
     */
    AL,
    /**
     * Am tuf sem ex.
     */
    AM,
    /**
     * Ap tuf sem ex.
     */
    AP,
    /**
     * Ba tuf sem ex.
     */
    BA,
    /**
     * Ce tuf sem ex.
     */
    CE,
    /**
     * Df tuf sem ex.
     */
    DF,
    /**
     * Es tuf sem ex.
     */
    ES,
    /**
     * Go tuf sem ex.
     */
    GO,
    /**
     * Ma tuf sem ex.
     */
    MA,
    /**
     * Mg tuf sem ex.
     */
    MG,
    /**
     * Ms tuf sem ex.
     */
    MS,
    /**
     * Mt tuf sem ex.
     */
    MT,
    /**
     * Pa tuf sem ex.
     */
    PA,
    /**
     * Pb tuf sem ex.
     */
    PB,
    /**
     * Pe tuf sem ex.
     */
    PE,
    /**
     * Pi tuf sem ex.
     */
    PI,
    /**
     * Pr tuf sem ex.
     */
    PR,
    /**
     * Rj tuf sem ex.
     */
    RJ,
    /**
     * Rn tuf sem ex.
     */
    RN,
    /**
     * Ro tuf sem ex.
     */
    RO,
    /**
     * Rr tuf sem ex.
     */
    RR,
    /**
     * Rs tuf sem ex.
     */
    RS,
    /**
     * Sc tuf sem ex.
     */
    SC,
    /**
     * Se tuf sem ex.
     */
    SE,
    /**
     * Sp tuf sem ex.
     */
    SP,
    /**
     * To tuf sem ex.
     */
    TO;

    /**
     * From value tuf sem ex.
     *
     * @param v the v
     * @return the tuf sem ex
     */
    public static TUFSemEX fromValue(String v) {
        return valueOf(v);
    }

    /**
     * Value string.
     *
     * @return the string
     */
    public String value() {
        return name();
    }

}
