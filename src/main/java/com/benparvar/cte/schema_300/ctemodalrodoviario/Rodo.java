//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:43:52 PM BRT 
//


package com.benparvar.cte.schema_300.ctemodalrodoviario;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Rodo.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "rntrc",
        "occ"
})
@XmlRootElement(name = "rodo", namespace = "http://www.portalfiscal.inf.br/cte")
public class Rodo {

    /**
     * The Rntrc.
     */
    @XmlElement(name = "RNTRC", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String rntrc;
    /**
     * The Occ.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<Occ> occ;

    /**
     * Gets rntrc.
     *
     * @return the rntrc
     */
    public String getRNTRC() {
        return rntrc;
    }

    /**
     * Sets rntrc.
     *
     * @param value the value
     */
    public void setRNTRC(String value) {
        this.rntrc = value;
    }

    /**
     * Gets occ.
     *
     * @return the occ
     */
    public List<Occ> getOcc() {
        if (occ == null) {
            occ = new ArrayList<Occ>();
        }
        return this.occ;
    }


    /**
     * The type Occ.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "serie",
            "nOcc",
            "dEmi",
            "emiOcc"
    })
    public static class Occ {

        /**
         * The Serie.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String serie;
        /**
         * The N occ.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nOcc;
        /**
         * The D emi.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String dEmi;
        /**
         * The Emi occ.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected Rodo.Occ.EmiOcc emiOcc;

        /**
         * Gets serie.
         *
         * @return the serie
         */
        public String getSerie() {
            return serie;
        }

        /**
         * Sets serie.
         *
         * @param value the value
         */
        public void setSerie(String value) {
            this.serie = value;
        }

        /**
         * Gets n occ.
         *
         * @return the n occ
         */
        public String getNOcc() {
            return nOcc;
        }

        /**
         * Sets n occ.
         *
         * @param value the value
         */
        public void setNOcc(String value) {
            this.nOcc = value;
        }

        /**
         * Gets d emi.
         *
         * @return the d emi
         */
        public String getDEmi() {
            return dEmi;
        }

        /**
         * Sets d emi.
         *
         * @param value the value
         */
        public void setDEmi(String value) {
            this.dEmi = value;
        }

        /**
         * Gets emi occ.
         *
         * @return the emi occ
         */
        public Rodo.Occ.EmiOcc getEmiOcc() {
            return emiOcc;
        }

        /**
         * Sets emi occ.
         *
         * @param value the value
         */
        public void setEmiOcc(Rodo.Occ.EmiOcc value) {
            this.emiOcc = value;
        }


        /**
         * The type Emi occ.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cInt",
                "ie",
                "uf",
                "fone"
        })
        public static class EmiOcc {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cnpj;
            /**
             * The C int.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cInt;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String ie;
            /**
             * The Uf.
             */
            @XmlElement(name = "UF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            @XmlSchemaType(name = "string")
            protected TUf uf;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets c int.
             *
             * @return the c int
             */
            public String getCInt() {
                return cInt;
            }

            /**
             * Sets c int.
             *
             * @param value the value
             */
            public void setCInt(String value) {
                this.cInt = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets uf.
             *
             * @return the uf
             */
            public TUf getUF() {
                return uf;
            }

            /**
             * Sets uf.
             *
             * @param value the value
             */
            public void setUF(TUf value) {
                this.uf = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

        }

    }

}
