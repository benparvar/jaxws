//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:47:55 PM BRT 
//


package com.benparvar.cte.schema_300.evccecte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type T ret evento.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetEvento", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infEvento",
        "signature"
})
public class TRetEvento {

    /**
     * The Inf evento.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TRetEvento.InfEvento infEvento;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf evento.
     *
     * @return the inf evento
     */
    public TRetEvento.InfEvento getInfEvento() {
        return infEvento;
    }

    /**
     * Sets inf evento.
     *
     * @param value the value
     */
    public void setInfEvento(TRetEvento.InfEvento value) {
        this.infEvento = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf evento.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "tpAmb",
            "verAplic",
            "cOrgao",
            "cStat",
            "xMotivo",
            "chCTe",
            "tpEvento",
            "xEvento",
            "nSeqEvento",
            "dhRegEvento",
            "nProt"
    })
    public static class InfEvento {

        /**
         * The Tp amb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpAmb;
        /**
         * The Ver aplic.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String verAplic;
        /**
         * The C orgao.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cOrgao;
        /**
         * The C stat.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cStat;
        /**
         * The X motivo.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xMotivo;
        /**
         * The Ch c te.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String chCTe;
        /**
         * The Tp evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String tpEvento;
        /**
         * The X evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String xEvento;
        /**
         * The N seq evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nSeqEvento;
        /**
         * The Dh reg evento.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String dhRegEvento;
        /**
         * The N prot.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nProt;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;

        /**
         * Gets tp amb.
         *
         * @return the tp amb
         */
        public String getTpAmb() {
            return tpAmb;
        }

        /**
         * Sets tp amb.
         *
         * @param value the value
         */
        public void setTpAmb(String value) {
            this.tpAmb = value;
        }

        /**
         * Gets ver aplic.
         *
         * @return the ver aplic
         */
        public String getVerAplic() {
            return verAplic;
        }

        /**
         * Sets ver aplic.
         *
         * @param value the value
         */
        public void setVerAplic(String value) {
            this.verAplic = value;
        }

        /**
         * Gets c orgao.
         *
         * @return the c orgao
         */
        public String getCOrgao() {
            return cOrgao;
        }

        /**
         * Sets c orgao.
         *
         * @param value the value
         */
        public void setCOrgao(String value) {
            this.cOrgao = value;
        }

        /**
         * Gets c stat.
         *
         * @return the c stat
         */
        public String getCStat() {
            return cStat;
        }

        /**
         * Sets c stat.
         *
         * @param value the value
         */
        public void setCStat(String value) {
            this.cStat = value;
        }

        /**
         * Gets x motivo.
         *
         * @return the x motivo
         */
        public String getXMotivo() {
            return xMotivo;
        }

        /**
         * Sets x motivo.
         *
         * @param value the value
         */
        public void setXMotivo(String value) {
            this.xMotivo = value;
        }

        /**
         * Gets ch c te.
         *
         * @return the ch c te
         */
        public String getChCTe() {
            return chCTe;
        }

        /**
         * Sets ch c te.
         *
         * @param value the value
         */
        public void setChCTe(String value) {
            this.chCTe = value;
        }

        /**
         * Gets tp evento.
         *
         * @return the tp evento
         */
        public String getTpEvento() {
            return tpEvento;
        }

        /**
         * Sets tp evento.
         *
         * @param value the value
         */
        public void setTpEvento(String value) {
            this.tpEvento = value;
        }

        /**
         * Gets x evento.
         *
         * @return the x evento
         */
        public String getXEvento() {
            return xEvento;
        }

        /**
         * Sets x evento.
         *
         * @param value the value
         */
        public void setXEvento(String value) {
            this.xEvento = value;
        }

        /**
         * Gets n seq evento.
         *
         * @return the n seq evento
         */
        public String getNSeqEvento() {
            return nSeqEvento;
        }

        /**
         * Sets n seq evento.
         *
         * @param value the value
         */
        public void setNSeqEvento(String value) {
            this.nSeqEvento = value;
        }

        /**
         * Gets dh reg evento.
         *
         * @return the dh reg evento
         */
        public String getDhRegEvento() {
            return dhRegEvento;
        }

        /**
         * Sets dh reg evento.
         *
         * @param value the value
         */
        public void setDhRegEvento(String value) {
            this.dhRegEvento = value;
        }

        /**
         * Gets n prot.
         *
         * @return the n prot
         */
        public String getNProt() {
            return nProt;
        }

        /**
         * Sets n prot.
         *
         * @param value the value
         */
        public void setNProt(String value) {
            this.nProt = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }

    }

}
