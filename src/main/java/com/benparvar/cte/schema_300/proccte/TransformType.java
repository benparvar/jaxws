//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 09:58:13 PM BRT 
//


package com.benparvar.cte.schema_300.proccte;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Transform type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransformType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "xPath"
})
public class TransformType {

    /**
     * The X path.
     */
    @XmlElement(name = "XPath", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected List<String> xPath;
    /**
     * The Algorithm.
     */
    @XmlAttribute(name = "Algorithm", required = true)
    protected String algorithm;

    /**
     * Gets x path.
     *
     * @return the x path
     */
    public List<String> getXPath() {
        if (xPath == null) {
            xPath = new ArrayList<String>();
        }
        return this.xPath;
    }

    /**
     * Gets algorithm.
     *
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * Sets algorithm.
     *
     * @param value the value
     */
    public void setAlgorithm(String value) {
        this.algorithm = value;
    }

}
