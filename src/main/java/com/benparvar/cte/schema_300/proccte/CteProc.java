//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 09:58:13 PM BRT 
//


package com.benparvar.cte.schema_300.proccte;

import javax.xml.bind.annotation.*;


/**
 * The type Cte proc.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "cTe",
        "protCTe"
})
@XmlRootElement(name = "cteProc", namespace = "http://www.portalfiscal.inf.br/cte")
public class CteProc {

    /**
     * The C te.
     */
    @XmlElement(name = "CTe", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TCTe cTe;
    /**
     * The Prot c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TProtCTe protCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;
    /**
     * The Ip transmissor.
     */
    @XmlAttribute(name = "ipTransmissor")
    protected String ipTransmissor;

    /**
     * Gets c te.
     *
     * @return the c te
     */
    public TCTe getCTe() {
        return cTe;
    }

    /**
     * Sets c te.
     *
     * @param value the value
     */
    public void setCTe(TCTe value) {
        this.cTe = value;
    }

    /**
     * Gets prot c te.
     *
     * @return the prot c te
     */
    public TProtCTe getProtCTe() {
        return protCTe;
    }

    /**
     * Sets prot c te.
     *
     * @param value the value
     */
    public void setProtCTe(TProtCTe value) {
        this.protCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

    /**
     * Gets ip transmissor.
     *
     * @return the ip transmissor
     */
    public String getIpTransmissor() {
        return ipTransmissor;
    }

    /**
     * Sets ip transmissor.
     *
     * @param value the value
     */
    public void setIpTransmissor(String value) {
        this.ipTransmissor = value;
    }

}
