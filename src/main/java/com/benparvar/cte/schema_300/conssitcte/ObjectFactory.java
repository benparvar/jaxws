//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:39:54 PM BRT 
//


package com.benparvar.cte.schema_300.conssitcte;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Signature_QNAME = new QName("http://www.w3.org/2000/09/xmldsig#", "Signature");
    private final static QName _ConsSitCTe_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "consSitCTe");

    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create reference type reference type.
     *
     * @return the reference type
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create signed info type signed info type.
     *
     * @return the signed info type
     */
    public SignedInfoType createSignedInfoType() {
        return new SignedInfoType();
    }

    /**
     * Create t ret cons sit c te t ret cons sit c te.
     *
     * @return the t ret cons sit c te
     */
    public TRetConsSitCTe createTRetConsSitCTe() {
        return new TRetConsSitCTe();
    }

    /**
     * Create t cons sit c te t cons sit c te.
     *
     * @return the t cons sit c te
     */
    public TConsSitCTe createTConsSitCTe() {
        return new TConsSitCTe();
    }

    /**
     * Create signature type signature type.
     *
     * @return the signature type
     */
    public SignatureType createSignatureType() {
        return new SignatureType();
    }

    /**
     * Create x 509 data type x 509 data type.
     *
     * @return the x 509 data type
     */
    public X509DataType createX509DataType() {
        return new X509DataType();
    }

    /**
     * Create signature value type signature value type.
     *
     * @return the signature value type
     */
    public SignatureValueType createSignatureValueType() {
        return new SignatureValueType();
    }

    /**
     * Create transforms type transforms type.
     *
     * @return the transforms type
     */
    public TransformsType createTransformsType() {
        return new TransformsType();
    }

    /**
     * Create transform type transform type.
     *
     * @return the transform type
     */
    public TransformType createTransformType() {
        return new TransformType();
    }

    /**
     * Create key info type key info type.
     *
     * @return the key info type
     */
    public KeyInfoType createKeyInfoType() {
        return new KeyInfoType();
    }

    /**
     * Create reference type digest method reference type . digest method.
     *
     * @return the reference type . digest method
     */
    public ReferenceType.DigestMethod createReferenceTypeDigestMethod() {
        return new ReferenceType.DigestMethod();
    }

    /**
     * Create signed info type canonicalization method signed info type . canonicalization method.
     *
     * @return the signed info type . canonicalization method
     */
    public SignedInfoType.CanonicalizationMethod createSignedInfoTypeCanonicalizationMethod() {
        return new SignedInfoType.CanonicalizationMethod();
    }

    /**
     * Create signed info type signature method signed info type . signature method.
     *
     * @return the signed info type . signature method
     */
    public SignedInfoType.SignatureMethod createSignedInfoTypeSignatureMethod() {
        return new SignedInfoType.SignatureMethod();
    }

    /**
     * Create t ret cons sit c te prot c te t ret cons sit c te . prot c te.
     *
     * @return the t ret cons sit c te . prot c te
     */
    public TRetConsSitCTe.ProtCTe createTRetConsSitCTeProtCTe() {
        return new TRetConsSitCTe.ProtCTe();
    }

    /**
     * Create t ret cons sit c te proc evento c te t ret cons sit c te . proc evento c te.
     *
     * @return the t ret cons sit c te . proc evento c te
     */
    public TRetConsSitCTe.ProcEventoCTe createTRetConsSitCTeProcEventoCTe() {
        return new TRetConsSitCTe.ProcEventoCTe();
    }

    /**
     * Create signature jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2000/09/xmldsig#", name = "Signature")
    public JAXBElement<SignatureType> createSignature(SignatureType value) {
        return new JAXBElement<SignatureType>(_Signature_QNAME, SignatureType.class, null, value);
    }

    /**
     * Create cons sit c te jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "consSitCTe")
    public JAXBElement<TConsSitCTe> createConsSitCTe(TConsSitCTe value) {
        return new JAXBElement<TConsSitCTe>(_ConsSitCTe_QNAME, TConsSitCTe.class, null, value);
    }

}
