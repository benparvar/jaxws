package com.benparvar.cte.schema_300.consstatservcte;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * The type Object factory.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsStatServCte_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "consStatServCte");

    /**
     * Instantiates a new Object factory.
     */
    public ObjectFactory() {
    }

    /**
     * Create t cons stat serv t cons stat serv.
     *
     * @return the t cons stat serv
     */
    public TConsStatServ createTConsStatServ() {
        return new TConsStatServ();
    }

    /**
     * Create t ret cons stat serv t ret cons stat serv.
     *
     * @return the t ret cons stat serv
     */
    public TRetConsStatServ createTRetConsStatServ() {
        return new TRetConsStatServ();
    }

    /**
     * Create cons stat serv cte jaxb element.
     *
     * @param value the value
     * @return the jaxb element
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "consStatServCte")
    public JAXBElement<TConsStatServ> createConsStatServCte(TConsStatServ value) {
        return new JAXBElement<TConsStatServ>(_ConsStatServCte_QNAME, TConsStatServ.class, null, value);
    }

}
