package com.benparvar.cte.schema_300.consstatservcte;

import javax.xml.bind.annotation.*;


/**
 * The type T cons stat serv.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TConsStatServ", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "xServ"
})
public class TConsStatServ {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The X serv.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xServ;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets x serv.
     *
     * @return the x serv
     */
    public String getXServ() {
        return xServ;
    }

    /**
     * Sets x serv.
     *
     * @param value the value
     */
    public void setXServ(String value) {
        this.xServ = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
