package com.benparvar.cte.schema_300.consstatservcte;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * The type T ret cons stat serv.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetConsStatServ", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "verAplic",
        "cStat",
        "xMotivo",
        "cuf",
        "dhRecbto",
        "tMed",
        "dhRetorno",
        "xObs"
})
public class TRetConsStatServ {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The Ver aplic.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String verAplic;
    /**
     * The C stat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cStat;
    /**
     * The X motivo.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMotivo;
    /**
     * The Cuf.
     */
    @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cuf;
    /**
     * The Dh recbto.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String dhRecbto;
    /**
     * The T med.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected BigInteger tMed;
    /**
     * The Dh retorno.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String dhRetorno;
    /**
     * The X obs.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String xObs;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets ver aplic.
     *
     * @return the ver aplic
     */
    public String getVerAplic() {
        return verAplic;
    }

    /**
     * Sets ver aplic.
     *
     * @param value the value
     */
    public void setVerAplic(String value) {
        this.verAplic = value;
    }

    /**
     * Gets c stat.
     *
     * @return the c stat
     */
    public String getCStat() {
        return cStat;
    }

    /**
     * Sets c stat.
     *
     * @param value the value
     */
    public void setCStat(String value) {
        this.cStat = value;
    }

    /**
     * Gets x motivo.
     *
     * @return the x motivo
     */
    public String getXMotivo() {
        return xMotivo;
    }

    /**
     * Sets x motivo.
     *
     * @param value the value
     */
    public void setXMotivo(String value) {
        this.xMotivo = value;
    }

    /**
     * Gets cuf.
     *
     * @return the cuf
     */
    public String getCUF() {
        return cuf;
    }

    /**
     * Sets cuf.
     *
     * @param value the value
     */
    public void setCUF(String value) {
        this.cuf = value;
    }

    /**
     * Gets dh recbto.
     *
     * @return the dh recbto
     */
    public String getDhRecbto() {
        return dhRecbto;
    }

    /**
     * Sets dh recbto.
     *
     * @param value the value
     */
    public void setDhRecbto(String value) {
        this.dhRecbto = value;
    }

    /**
     * Gets t med.
     *
     * @return the t med
     */
    public BigInteger getTMed() {
        return tMed;
    }

    /**
     * Sets t med.
     *
     * @param value the value
     */
    public void setTMed(BigInteger value) {
        this.tMed = value;
    }

    /**
     * Gets dh retorno.
     *
     * @return the dh retorno
     */
    public String getDhRetorno() {
        return dhRetorno;
    }

    /**
     * Sets dh retorno.
     *
     * @param value the value
     */
    public void setDhRetorno(String value) {
        this.dhRetorno = value;
    }

    /**
     * Gets x obs.
     *
     * @return the x obs
     */
    public String getXObs() {
        return xObs;
    }

    /**
     * Sets x obs.
     *
     * @param value the value
     */
    public void setXObs(String value) {
        this.xObs = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
