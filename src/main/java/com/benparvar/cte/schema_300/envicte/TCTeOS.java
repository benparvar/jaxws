//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:46:03 PM BRT 
//


package com.benparvar.cte.schema_300.envicte;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Tc te os.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCTeOS", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infCte",
        "infCTeSupl",
        "signature"
})
public class TCTeOS {

    /**
     * The Inf cte.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TCTeOS.InfCte infCte;
    /**
     * The Inf c te supl.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected TCTeOS.InfCTeSupl infCTeSupl;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf cte.
     *
     * @return the inf cte
     */
    public TCTeOS.InfCte getInfCte() {
        return infCte;
    }

    /**
     * Sets inf cte.
     *
     * @param value the value
     */
    public void setInfCte(TCTeOS.InfCte value) {
        this.infCte = value;
    }

    /**
     * Gets inf c te supl.
     *
     * @return the inf c te supl
     */
    public TCTeOS.InfCTeSupl getInfCTeSupl() {
        return infCTeSupl;
    }

    /**
     * Sets inf c te supl.
     *
     * @param value the value
     */
    public void setInfCTeSupl(TCTeOS.InfCTeSupl value) {
        this.infCTeSupl = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf c te supl.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "qrCodCTe"
    })
    public static class InfCTeSupl {

        /**
         * The Qr cod c te.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String qrCodCTe;

        /**
         * Gets qr cod c te.
         *
         * @return the qr cod c te
         */
        public String getQrCodCTe() {
            return qrCodCTe;
        }

        /**
         * Sets qr cod c te.
         *
         * @param value the value
         */
        public void setQrCodCTe(String value) {
            this.qrCodCTe = value;
        }

    }


    /**
     * The type Inf cte.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ide",
            "compl",
            "emit",
            "toma",
            "vPrest",
            "imp",
            "infCTeNorm",
            "infCteComp",
            "infCteAnu",
            "autXML",
            "infRespTec"
    })
    public static class InfCte {

        /**
         * The Ide.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTeOS.InfCte.Ide ide;
        /**
         * The Compl.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTeOS.InfCte.Compl compl;
        /**
         * The Emit.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTeOS.InfCte.Emit emit;
        /**
         * The Toma.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTeOS.InfCte.Toma toma;
        /**
         * The V prest.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTeOS.InfCte.VPrest vPrest;
        /**
         * The Imp.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected TCTeOS.InfCte.Imp imp;
        /**
         * The Inf c te norm.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTeOS.InfCte.InfCTeNorm infCTeNorm;
        /**
         * The Inf cte comp.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTeOS.InfCte.InfCteComp infCteComp;
        /**
         * The Inf cte anu.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TCTeOS.InfCte.InfCteAnu infCteAnu;
        /**
         * The Aut xml.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected List<AutXML> autXML;
        /**
         * The Inf resp tec.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected TRespTec infRespTec;
        /**
         * The Versao.
         */
        @XmlAttribute(name = "versao", required = true)
        protected String versao;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;

        /**
         * Gets ide.
         *
         * @return the ide
         */
        public TCTeOS.InfCte.Ide getIde() {
            return ide;
        }

        /**
         * Sets ide.
         *
         * @param value the value
         */
        public void setIde(TCTeOS.InfCte.Ide value) {
            this.ide = value;
        }

        /**
         * Gets compl.
         *
         * @return the compl
         */
        public TCTeOS.InfCte.Compl getCompl() {
            return compl;
        }

        /**
         * Sets compl.
         *
         * @param value the value
         */
        public void setCompl(TCTeOS.InfCte.Compl value) {
            this.compl = value;
        }

        /**
         * Gets emit.
         *
         * @return the emit
         */
        public TCTeOS.InfCte.Emit getEmit() {
            return emit;
        }

        /**
         * Sets emit.
         *
         * @param value the value
         */
        public void setEmit(TCTeOS.InfCte.Emit value) {
            this.emit = value;
        }

        /**
         * Gets toma.
         *
         * @return the toma
         */
        public TCTeOS.InfCte.Toma getToma() {
            return toma;
        }

        /**
         * Sets toma.
         *
         * @param value the value
         */
        public void setToma(TCTeOS.InfCte.Toma value) {
            this.toma = value;
        }

        /**
         * Gets v prest.
         *
         * @return the v prest
         */
        public TCTeOS.InfCte.VPrest getVPrest() {
            return vPrest;
        }

        /**
         * Sets v prest.
         *
         * @param value the value
         */
        public void setVPrest(TCTeOS.InfCte.VPrest value) {
            this.vPrest = value;
        }

        /**
         * Gets imp.
         *
         * @return the imp
         */
        public TCTeOS.InfCte.Imp getImp() {
            return imp;
        }

        /**
         * Sets imp.
         *
         * @param value the value
         */
        public void setImp(TCTeOS.InfCte.Imp value) {
            this.imp = value;
        }

        /**
         * Gets inf c te norm.
         *
         * @return the inf c te norm
         */
        public TCTeOS.InfCte.InfCTeNorm getInfCTeNorm() {
            return infCTeNorm;
        }

        /**
         * Sets inf c te norm.
         *
         * @param value the value
         */
        public void setInfCTeNorm(TCTeOS.InfCte.InfCTeNorm value) {
            this.infCTeNorm = value;
        }

        /**
         * Gets inf cte comp.
         *
         * @return the inf cte comp
         */
        public TCTeOS.InfCte.InfCteComp getInfCteComp() {
            return infCteComp;
        }

        /**
         * Sets inf cte comp.
         *
         * @param value the value
         */
        public void setInfCteComp(TCTeOS.InfCte.InfCteComp value) {
            this.infCteComp = value;
        }

        /**
         * Gets inf cte anu.
         *
         * @return the inf cte anu
         */
        public TCTeOS.InfCte.InfCteAnu getInfCteAnu() {
            return infCteAnu;
        }

        /**
         * Sets inf cte anu.
         *
         * @param value the value
         */
        public void setInfCteAnu(TCTeOS.InfCte.InfCteAnu value) {
            this.infCteAnu = value;
        }

        /**
         * Gets aut xml.
         *
         * @return the aut xml
         */
        public List<AutXML> getAutXML() {
            if (autXML == null) {
                autXML = new ArrayList<AutXML>();
            }
            return this.autXML;
        }

        /**
         * Gets inf resp tec.
         *
         * @return the inf resp tec
         */
        public TRespTec getInfRespTec() {
            return infRespTec;
        }

        /**
         * Sets inf resp tec.
         *
         * @param value the value
         */
        public void setInfRespTec(TRespTec value) {
            this.infRespTec = value;
        }

        /**
         * Gets versao.
         *
         * @return the versao
         */
        public String getVersao() {
            return versao;
        }

        /**
         * Sets versao.
         *
         * @param value the value
         */
        public void setVersao(String value) {
            this.versao = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * The type Aut xml.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf"
        })
        public static class AutXML {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

        }


        /**
         * The type Compl.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "xCaracAd",
                "xCaracSer",
                "xEmi",
                "xObs",
                "obsCont",
                "obsFisco"
        })
        public static class Compl {

            /**
             * The X carac ad.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xCaracAd;
            /**
             * The X carac ser.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xCaracSer;
            /**
             * The X emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xEmi;
            /**
             * The X obs.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xObs;
            /**
             * The Obs cont.
             */
            @XmlElement(name = "ObsCont", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<ObsCont> obsCont;
            /**
             * The Obs fisco.
             */
            @XmlElement(name = "ObsFisco", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<ObsFisco> obsFisco;

            /**
             * Gets x carac ad.
             *
             * @return the x carac ad
             */
            public String getXCaracAd() {
                return xCaracAd;
            }

            /**
             * Sets x carac ad.
             *
             * @param value the value
             */
            public void setXCaracAd(String value) {
                this.xCaracAd = value;
            }

            /**
             * Gets x carac ser.
             *
             * @return the x carac ser
             */
            public String getXCaracSer() {
                return xCaracSer;
            }

            /**
             * Sets x carac ser.
             *
             * @param value the value
             */
            public void setXCaracSer(String value) {
                this.xCaracSer = value;
            }

            /**
             * Gets x emi.
             *
             * @return the x emi
             */
            public String getXEmi() {
                return xEmi;
            }

            /**
             * Sets x emi.
             *
             * @param value the value
             */
            public void setXEmi(String value) {
                this.xEmi = value;
            }

            /**
             * Gets x obs.
             *
             * @return the x obs
             */
            public String getXObs() {
                return xObs;
            }

            /**
             * Sets x obs.
             *
             * @param value the value
             */
            public void setXObs(String value) {
                this.xObs = value;
            }

            /**
             * Gets obs cont.
             *
             * @return the obs cont
             */
            public List<ObsCont> getObsCont() {
                if (obsCont == null) {
                    obsCont = new ArrayList<ObsCont>();
                }
                return this.obsCont;
            }

            /**
             * Gets obs fisco.
             *
             * @return the obs fisco
             */
            public List<ObsFisco> getObsFisco() {
                if (obsFisco == null) {
                    obsFisco = new ArrayList<ObsFisco>();
                }
                return this.obsFisco;
            }


            /**
             * The type Obs cont.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xTexto"
            })
            public static class ObsCont {

                /**
                 * The X texto.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xTexto;
                /**
                 * The X campo.
                 */
                @XmlAttribute(name = "xCampo", required = true)
                protected String xCampo;

                /**
                 * Gets x texto.
                 *
                 * @return the x texto
                 */
                public String getXTexto() {
                    return xTexto;
                }

                /**
                 * Sets x texto.
                 *
                 * @param value the value
                 */
                public void setXTexto(String value) {
                    this.xTexto = value;
                }

                /**
                 * Gets x campo.
                 *
                 * @return the x campo
                 */
                public String getXCampo() {
                    return xCampo;
                }

                /**
                 * Sets x campo.
                 *
                 * @param value the value
                 */
                public void setXCampo(String value) {
                    this.xCampo = value;
                }

            }


            /**
             * The type Obs fisco.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xTexto"
            })
            public static class ObsFisco {

                /**
                 * The X texto.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xTexto;
                /**
                 * The X campo.
                 */
                @XmlAttribute(name = "xCampo", required = true)
                protected String xCampo;

                /**
                 * Gets x texto.
                 *
                 * @return the x texto
                 */
                public String getXTexto() {
                    return xTexto;
                }

                /**
                 * Sets x texto.
                 *
                 * @param value the value
                 */
                public void setXTexto(String value) {
                    this.xTexto = value;
                }

                /**
                 * Gets x campo.
                 *
                 * @return the x campo
                 */
                public String getXCampo() {
                    return xCampo;
                }

                /**
                 * Sets x campo.
                 *
                 * @param value the value
                 */
                public void setXCampo(String value) {
                    this.xCampo = value;
                }

            }

        }


        /**
         * The type Emit.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "ie",
                "iest",
                "xNome",
                "xFant",
                "enderEmit"
        })
        public static class Emit {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cnpj;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String ie;
            /**
             * The Iest.
             */
            @XmlElement(name = "IEST", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String iest;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The X fant.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xFant;
            /**
             * The Ender emit.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndeEmi enderEmit;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets iest.
             *
             * @return the iest
             */
            public String getIEST() {
                return iest;
            }

            /**
             * Sets iest.
             *
             * @param value the value
             */
            public void setIEST(String value) {
                this.iest = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets x fant.
             *
             * @return the x fant
             */
            public String getXFant() {
                return xFant;
            }

            /**
             * Sets x fant.
             *
             * @param value the value
             */
            public void setXFant(String value) {
                this.xFant = value;
            }

            /**
             * Gets ender emit.
             *
             * @return the ender emit
             */
            public TEndeEmi getEnderEmit() {
                return enderEmit;
            }

            /**
             * Sets ender emit.
             *
             * @param value the value
             */
            public void setEnderEmit(TEndeEmi value) {
                this.enderEmit = value;
            }

        }


        /**
         * The type Ide.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cuf",
                "cct",
                "cfop",
                "natOp",
                "mod",
                "serie",
                "nct",
                "dhEmi",
                "tpImp",
                "tpEmis",
                "cdv",
                "tpAmb",
                "tpCTe",
                "procEmi",
                "verProc",
                "cMunEnv",
                "xMunEnv",
                "ufEnv",
                "modal",
                "tpServ",
                "indIEToma",
                "cMunIni",
                "xMunIni",
                "ufIni",
                "cMunFim",
                "xMunFim",
                "ufFim",
                "infPercurso",
                "dhCont",
                "xJust"
        })
        public static class Ide {

            /**
             * The Cuf.
             */
            @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cuf;
            /**
             * The Cct.
             */
            @XmlElement(name = "cCT", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cct;
            /**
             * The Cfop.
             */
            @XmlElement(name = "CFOP", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cfop;
            /**
             * The Nat op.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String natOp;
            /**
             * The Mod.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String mod;
            /**
             * The Serie.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String serie;
            /**
             * The Nct.
             */
            @XmlElement(name = "nCT", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String nct;
            /**
             * The Dh emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String dhEmi;
            /**
             * The Tp imp.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpImp;
            /**
             * The Tp emis.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpEmis;
            /**
             * The Cdv.
             */
            @XmlElement(name = "cDV", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cdv;
            /**
             * The Tp amb.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpAmb;
            /**
             * The Tp c te.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpCTe;
            /**
             * The Proc emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String procEmi;
            /**
             * The Ver proc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String verProc;
            /**
             * The C mun env.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String cMunEnv;
            /**
             * The X mun env.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xMunEnv;
            /**
             * The Uf env.
             */
            @XmlElement(name = "UFEnv", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            @XmlSchemaType(name = "string")
            protected TUf ufEnv;
            /**
             * The Modal.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String modal;
            /**
             * The Tp serv.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String tpServ;
            /**
             * The Ind ie toma.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String indIEToma;
            /**
             * The C mun ini.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cMunIni;
            /**
             * The X mun ini.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xMunIni;
            /**
             * The Uf ini.
             */
            @XmlElement(name = "UFIni", namespace = "http://www.portalfiscal.inf.br/cte")
            @XmlSchemaType(name = "string")
            protected TUf ufIni;
            /**
             * The C mun fim.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cMunFim;
            /**
             * The X mun fim.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xMunFim;
            /**
             * The Uf fim.
             */
            @XmlElement(name = "UFFim", namespace = "http://www.portalfiscal.inf.br/cte")
            @XmlSchemaType(name = "string")
            protected TUf ufFim;
            /**
             * The Inf percurso.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<InfPercurso> infPercurso;
            /**
             * The Dh cont.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String dhCont;
            /**
             * The X just.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xJust;

            /**
             * Gets cuf.
             *
             * @return the cuf
             */
            public String getCUF() {
                return cuf;
            }

            /**
             * Sets cuf.
             *
             * @param value the value
             */
            public void setCUF(String value) {
                this.cuf = value;
            }

            /**
             * Gets cct.
             *
             * @return the cct
             */
            public String getCCT() {
                return cct;
            }

            /**
             * Sets cct.
             *
             * @param value the value
             */
            public void setCCT(String value) {
                this.cct = value;
            }

            /**
             * Gets cfop.
             *
             * @return the cfop
             */
            public String getCFOP() {
                return cfop;
            }

            /**
             * Sets cfop.
             *
             * @param value the value
             */
            public void setCFOP(String value) {
                this.cfop = value;
            }

            /**
             * Gets nat op.
             *
             * @return the nat op
             */
            public String getNatOp() {
                return natOp;
            }

            /**
             * Sets nat op.
             *
             * @param value the value
             */
            public void setNatOp(String value) {
                this.natOp = value;
            }

            /**
             * Gets mod.
             *
             * @return the mod
             */
            public String getMod() {
                return mod;
            }

            /**
             * Sets mod.
             *
             * @param value the value
             */
            public void setMod(String value) {
                this.mod = value;
            }

            /**
             * Gets serie.
             *
             * @return the serie
             */
            public String getSerie() {
                return serie;
            }

            /**
             * Sets serie.
             *
             * @param value the value
             */
            public void setSerie(String value) {
                this.serie = value;
            }

            /**
             * Gets nct.
             *
             * @return the nct
             */
            public String getNCT() {
                return nct;
            }

            /**
             * Sets nct.
             *
             * @param value the value
             */
            public void setNCT(String value) {
                this.nct = value;
            }

            /**
             * Gets dh emi.
             *
             * @return the dh emi
             */
            public String getDhEmi() {
                return dhEmi;
            }

            /**
             * Sets dh emi.
             *
             * @param value the value
             */
            public void setDhEmi(String value) {
                this.dhEmi = value;
            }

            /**
             * Gets tp imp.
             *
             * @return the tp imp
             */
            public String getTpImp() {
                return tpImp;
            }

            /**
             * Sets tp imp.
             *
             * @param value the value
             */
            public void setTpImp(String value) {
                this.tpImp = value;
            }

            /**
             * Gets tp emis.
             *
             * @return the tp emis
             */
            public String getTpEmis() {
                return tpEmis;
            }

            /**
             * Sets tp emis.
             *
             * @param value the value
             */
            public void setTpEmis(String value) {
                this.tpEmis = value;
            }

            /**
             * Gets cdv.
             *
             * @return the cdv
             */
            public String getCDV() {
                return cdv;
            }

            /**
             * Sets cdv.
             *
             * @param value the value
             */
            public void setCDV(String value) {
                this.cdv = value;
            }

            /**
             * Gets tp amb.
             *
             * @return the tp amb
             */
            public String getTpAmb() {
                return tpAmb;
            }

            /**
             * Sets tp amb.
             *
             * @param value the value
             */
            public void setTpAmb(String value) {
                this.tpAmb = value;
            }

            /**
             * Gets tp c te.
             *
             * @return the tp c te
             */
            public String getTpCTe() {
                return tpCTe;
            }

            /**
             * Sets tp c te.
             *
             * @param value the value
             */
            public void setTpCTe(String value) {
                this.tpCTe = value;
            }

            /**
             * Gets proc emi.
             *
             * @return the proc emi
             */
            public String getProcEmi() {
                return procEmi;
            }

            /**
             * Sets proc emi.
             *
             * @param value the value
             */
            public void setProcEmi(String value) {
                this.procEmi = value;
            }

            /**
             * Gets ver proc.
             *
             * @return the ver proc
             */
            public String getVerProc() {
                return verProc;
            }

            /**
             * Sets ver proc.
             *
             * @param value the value
             */
            public void setVerProc(String value) {
                this.verProc = value;
            }

            /**
             * Gets c mun env.
             *
             * @return the c mun env
             */
            public String getCMunEnv() {
                return cMunEnv;
            }

            /**
             * Sets c mun env.
             *
             * @param value the value
             */
            public void setCMunEnv(String value) {
                this.cMunEnv = value;
            }

            /**
             * Gets x mun env.
             *
             * @return the x mun env
             */
            public String getXMunEnv() {
                return xMunEnv;
            }

            /**
             * Sets x mun env.
             *
             * @param value the value
             */
            public void setXMunEnv(String value) {
                this.xMunEnv = value;
            }

            /**
             * Gets uf env.
             *
             * @return the uf env
             */
            public TUf getUFEnv() {
                return ufEnv;
            }

            /**
             * Sets uf env.
             *
             * @param value the value
             */
            public void setUFEnv(TUf value) {
                this.ufEnv = value;
            }

            /**
             * Gets modal.
             *
             * @return the modal
             */
            public String getModal() {
                return modal;
            }

            /**
             * Sets modal.
             *
             * @param value the value
             */
            public void setModal(String value) {
                this.modal = value;
            }

            /**
             * Gets tp serv.
             *
             * @return the tp serv
             */
            public String getTpServ() {
                return tpServ;
            }

            /**
             * Sets tp serv.
             *
             * @param value the value
             */
            public void setTpServ(String value) {
                this.tpServ = value;
            }

            /**
             * Gets ind ie toma.
             *
             * @return the ind ie toma
             */
            public String getIndIEToma() {
                return indIEToma;
            }

            /**
             * Sets ind ie toma.
             *
             * @param value the value
             */
            public void setIndIEToma(String value) {
                this.indIEToma = value;
            }

            /**
             * Gets c mun ini.
             *
             * @return the c mun ini
             */
            public String getCMunIni() {
                return cMunIni;
            }

            /**
             * Sets c mun ini.
             *
             * @param value the value
             */
            public void setCMunIni(String value) {
                this.cMunIni = value;
            }

            /**
             * Gets x mun ini.
             *
             * @return the x mun ini
             */
            public String getXMunIni() {
                return xMunIni;
            }

            /**
             * Sets x mun ini.
             *
             * @param value the value
             */
            public void setXMunIni(String value) {
                this.xMunIni = value;
            }

            /**
             * Gets uf ini.
             *
             * @return the uf ini
             */
            public TUf getUFIni() {
                return ufIni;
            }

            /**
             * Sets uf ini.
             *
             * @param value the value
             */
            public void setUFIni(TUf value) {
                this.ufIni = value;
            }

            /**
             * Gets c mun fim.
             *
             * @return the c mun fim
             */
            public String getCMunFim() {
                return cMunFim;
            }

            /**
             * Sets c mun fim.
             *
             * @param value the value
             */
            public void setCMunFim(String value) {
                this.cMunFim = value;
            }

            /**
             * Gets x mun fim.
             *
             * @return the x mun fim
             */
            public String getXMunFim() {
                return xMunFim;
            }

            /**
             * Sets x mun fim.
             *
             * @param value the value
             */
            public void setXMunFim(String value) {
                this.xMunFim = value;
            }

            /**
             * Gets uf fim.
             *
             * @return the uf fim
             */
            public TUf getUFFim() {
                return ufFim;
            }

            /**
             * Sets uf fim.
             *
             * @param value the value
             */
            public void setUFFim(TUf value) {
                this.ufFim = value;
            }

            /**
             * Gets inf percurso.
             *
             * @return the inf percurso
             */
            public List<InfPercurso> getInfPercurso() {
                if (infPercurso == null) {
                    infPercurso = new ArrayList<InfPercurso>();
                }
                return this.infPercurso;
            }

            /**
             * Gets dh cont.
             *
             * @return the dh cont
             */
            public String getDhCont() {
                return dhCont;
            }

            /**
             * Sets dh cont.
             *
             * @param value the value
             */
            public void setDhCont(String value) {
                this.dhCont = value;
            }

            /**
             * Gets x just.
             *
             * @return the x just
             */
            public String getXJust() {
                return xJust;
            }

            /**
             * Sets x just.
             *
             * @param value the value
             */
            public void setXJust(String value) {
                this.xJust = value;
            }


            /**
             * The type Inf percurso.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "ufPer"
            })
            public static class InfPercurso {

                /**
                 * The Uf per.
                 */
                @XmlElement(name = "UFPer", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                @XmlSchemaType(name = "string")
                protected TUf ufPer;

                /**
                 * Gets uf per.
                 *
                 * @return the uf per
                 */
                public TUf getUFPer() {
                    return ufPer;
                }

                /**
                 * Sets uf per.
                 *
                 * @param value the value
                 */
                public void setUFPer(TUf value) {
                    this.ufPer = value;
                }

            }

        }


        /**
         * The type Imp.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "icms",
                "vTotTrib",
                "infAdFisco",
                "icmsufFim",
                "infTribFed"
        })
        public static class Imp {

            /**
             * The Icms.
             */
            @XmlElement(name = "ICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TImpOS icms;
            /**
             * The V tot trib.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String vTotTrib;
            /**
             * The Inf ad fisco.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String infAdFisco;
            /**
             * The Icmsuf fim.
             */
            @XmlElement(name = "ICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTeOS.InfCte.Imp.ICMSUFFim icmsufFim;
            /**
             * The Inf trib fed.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTeOS.InfCte.Imp.InfTribFed infTribFed;

            /**
             * Gets icms.
             *
             * @return the icms
             */
            public TImpOS getICMS() {
                return icms;
            }

            /**
             * Sets icms.
             *
             * @param value the value
             */
            public void setICMS(TImpOS value) {
                this.icms = value;
            }

            /**
             * Gets v tot trib.
             *
             * @return the v tot trib
             */
            public String getVTotTrib() {
                return vTotTrib;
            }

            /**
             * Sets v tot trib.
             *
             * @param value the value
             */
            public void setVTotTrib(String value) {
                this.vTotTrib = value;
            }

            /**
             * Gets inf ad fisco.
             *
             * @return the inf ad fisco
             */
            public String getInfAdFisco() {
                return infAdFisco;
            }

            /**
             * Sets inf ad fisco.
             *
             * @param value the value
             */
            public void setInfAdFisco(String value) {
                this.infAdFisco = value;
            }

            /**
             * Gets icmsuf fim.
             *
             * @return the icmsuf fim
             */
            public TCTeOS.InfCte.Imp.ICMSUFFim getICMSUFFim() {
                return icmsufFim;
            }

            /**
             * Sets icmsuf fim.
             *
             * @param value the value
             */
            public void setICMSUFFim(TCTeOS.InfCte.Imp.ICMSUFFim value) {
                this.icmsufFim = value;
            }

            /**
             * Gets inf trib fed.
             *
             * @return the inf trib fed
             */
            public TCTeOS.InfCte.Imp.InfTribFed getInfTribFed() {
                return infTribFed;
            }

            /**
             * Sets inf trib fed.
             *
             * @param value the value
             */
            public void setInfTribFed(TCTeOS.InfCte.Imp.InfTribFed value) {
                this.infTribFed = value;
            }


            /**
             * The type Icmsuf fim.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vbcufFim",
                    "pfcpufFim",
                    "picmsufFim",
                    "picmsInter",
                    "vfcpufFim",
                    "vicmsufFim",
                    "vicmsufIni"
            })
            public static class ICMSUFFim {

                /**
                 * The Vbcuf fim.
                 */
                @XmlElement(name = "vBCUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vbcufFim;
                /**
                 * The Pfcpuf fim.
                 */
                @XmlElement(name = "pFCPUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String pfcpufFim;
                /**
                 * The Picmsuf fim.
                 */
                @XmlElement(name = "pICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String picmsufFim;
                /**
                 * The Picms inter.
                 */
                @XmlElement(name = "pICMSInter", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String picmsInter;
                /**
                 * The Vfcpuf fim.
                 */
                @XmlElement(name = "vFCPUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vfcpufFim;
                /**
                 * The Vicmsuf fim.
                 */
                @XmlElement(name = "vICMSUFFim", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vicmsufFim;
                /**
                 * The Vicmsuf ini.
                 */
                @XmlElement(name = "vICMSUFIni", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vicmsufIni;

                /**
                 * Gets vbcuf fim.
                 *
                 * @return the vbcuf fim
                 */
                public String getVBCUFFim() {
                    return vbcufFim;
                }

                /**
                 * Sets vbcuf fim.
                 *
                 * @param value the value
                 */
                public void setVBCUFFim(String value) {
                    this.vbcufFim = value;
                }

                /**
                 * Gets pfcpuf fim.
                 *
                 * @return the pfcpuf fim
                 */
                public String getPFCPUFFim() {
                    return pfcpufFim;
                }

                /**
                 * Sets pfcpuf fim.
                 *
                 * @param value the value
                 */
                public void setPFCPUFFim(String value) {
                    this.pfcpufFim = value;
                }

                /**
                 * Gets picmsuf fim.
                 *
                 * @return the picmsuf fim
                 */
                public String getPICMSUFFim() {
                    return picmsufFim;
                }

                /**
                 * Sets picmsuf fim.
                 *
                 * @param value the value
                 */
                public void setPICMSUFFim(String value) {
                    this.picmsufFim = value;
                }

                /**
                 * Gets picms inter.
                 *
                 * @return the picms inter
                 */
                public String getPICMSInter() {
                    return picmsInter;
                }

                /**
                 * Sets picms inter.
                 *
                 * @param value the value
                 */
                public void setPICMSInter(String value) {
                    this.picmsInter = value;
                }

                /**
                 * Gets vfcpuf fim.
                 *
                 * @return the vfcpuf fim
                 */
                public String getVFCPUFFim() {
                    return vfcpufFim;
                }

                /**
                 * Sets vfcpuf fim.
                 *
                 * @param value the value
                 */
                public void setVFCPUFFim(String value) {
                    this.vfcpufFim = value;
                }

                /**
                 * Gets vicmsuf fim.
                 *
                 * @return the vicmsuf fim
                 */
                public String getVICMSUFFim() {
                    return vicmsufFim;
                }

                /**
                 * Sets vicmsuf fim.
                 *
                 * @param value the value
                 */
                public void setVICMSUFFim(String value) {
                    this.vicmsufFim = value;
                }

                /**
                 * Gets vicmsuf ini.
                 *
                 * @return the vicmsuf ini
                 */
                public String getVICMSUFIni() {
                    return vicmsufIni;
                }

                /**
                 * Sets vicmsuf ini.
                 *
                 * @param value the value
                 */
                public void setVICMSUFIni(String value) {
                    this.vicmsufIni = value;
                }

            }


            /**
             * The type Inf trib fed.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vpis",
                    "vcofins",
                    "vir",
                    "vinss",
                    "vcsll"
            })
            public static class InfTribFed {

                /**
                 * The Vpis.
                 */
                @XmlElement(name = "vPIS", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vpis;
                /**
                 * The Vcofins.
                 */
                @XmlElement(name = "vCOFINS", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vcofins;
                /**
                 * The Vir.
                 */
                @XmlElement(name = "vIR", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vir;
                /**
                 * The Vinss.
                 */
                @XmlElement(name = "vINSS", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vinss;
                /**
                 * The Vcsll.
                 */
                @XmlElement(name = "vCSLL", namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vcsll;

                /**
                 * Gets vpis.
                 *
                 * @return the vpis
                 */
                public String getVPIS() {
                    return vpis;
                }

                /**
                 * Sets vpis.
                 *
                 * @param value the value
                 */
                public void setVPIS(String value) {
                    this.vpis = value;
                }

                /**
                 * Gets vcofins.
                 *
                 * @return the vcofins
                 */
                public String getVCOFINS() {
                    return vcofins;
                }

                /**
                 * Sets vcofins.
                 *
                 * @param value the value
                 */
                public void setVCOFINS(String value) {
                    this.vcofins = value;
                }

                /**
                 * Gets vir.
                 *
                 * @return the vir
                 */
                public String getVIR() {
                    return vir;
                }

                /**
                 * Sets vir.
                 *
                 * @param value the value
                 */
                public void setVIR(String value) {
                    this.vir = value;
                }

                /**
                 * Gets vinss.
                 *
                 * @return the vinss
                 */
                public String getVINSS() {
                    return vinss;
                }

                /**
                 * Sets vinss.
                 *
                 * @param value the value
                 */
                public void setVINSS(String value) {
                    this.vinss = value;
                }

                /**
                 * Gets vcsll.
                 *
                 * @return the vcsll
                 */
                public String getVCSLL() {
                    return vcsll;
                }

                /**
                 * Sets vcsll.
                 *
                 * @param value the value
                 */
                public void setVCSLL(String value) {
                    this.vcsll = value;
                }

            }

        }


        /**
         * The type Inf c te norm.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "infServico",
                "infDocRef",
                "seg",
                "infModal",
                "infCteSub",
                "refCTeCanc",
                "cobr"
        })
        public static class InfCTeNorm {

            /**
             * The Inf servico.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TCTeOS.InfCte.InfCTeNorm.InfServico infServico;
            /**
             * The Inf doc ref.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<InfDocRef> infDocRef;
            /**
             * The Seg.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<Seg> seg;
            /**
             * The Inf modal.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTeOS.InfCte.InfCTeNorm.InfModal infModal;
            /**
             * The Inf cte sub.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTeOS.InfCte.InfCTeNorm.InfCteSub infCteSub;
            /**
             * The Ref c te canc.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String refCTeCanc;
            /**
             * The Cobr.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected TCTeOS.InfCte.InfCTeNorm.Cobr cobr;

            /**
             * Gets inf servico.
             *
             * @return the inf servico
             */
            public TCTeOS.InfCte.InfCTeNorm.InfServico getInfServico() {
                return infServico;
            }

            /**
             * Sets inf servico.
             *
             * @param value the value
             */
            public void setInfServico(TCTeOS.InfCte.InfCTeNorm.InfServico value) {
                this.infServico = value;
            }

            /**
             * Gets inf doc ref.
             *
             * @return the inf doc ref
             */
            public List<InfDocRef> getInfDocRef() {
                if (infDocRef == null) {
                    infDocRef = new ArrayList<InfDocRef>();
                }
                return this.infDocRef;
            }

            /**
             * Gets seg.
             *
             * @return the seg
             */
            public List<Seg> getSeg() {
                if (seg == null) {
                    seg = new ArrayList<Seg>();
                }
                return this.seg;
            }

            /**
             * Gets inf modal.
             *
             * @return the inf modal
             */
            public TCTeOS.InfCte.InfCTeNorm.InfModal getInfModal() {
                return infModal;
            }

            /**
             * Sets inf modal.
             *
             * @param value the value
             */
            public void setInfModal(TCTeOS.InfCte.InfCTeNorm.InfModal value) {
                this.infModal = value;
            }

            /**
             * Gets inf cte sub.
             *
             * @return the inf cte sub
             */
            public TCTeOS.InfCte.InfCTeNorm.InfCteSub getInfCteSub() {
                return infCteSub;
            }

            /**
             * Sets inf cte sub.
             *
             * @param value the value
             */
            public void setInfCteSub(TCTeOS.InfCte.InfCTeNorm.InfCteSub value) {
                this.infCteSub = value;
            }

            /**
             * Gets ref c te canc.
             *
             * @return the ref c te canc
             */
            public String getRefCTeCanc() {
                return refCTeCanc;
            }

            /**
             * Sets ref c te canc.
             *
             * @param value the value
             */
            public void setRefCTeCanc(String value) {
                this.refCTeCanc = value;
            }

            /**
             * Gets cobr.
             *
             * @return the cobr
             */
            public TCTeOS.InfCte.InfCTeNorm.Cobr getCobr() {
                return cobr;
            }

            /**
             * Sets cobr.
             *
             * @param value the value
             */
            public void setCobr(TCTeOS.InfCte.InfCTeNorm.Cobr value) {
                this.cobr = value;
            }


            /**
             * The type Cobr.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "fat",
                    "dup"
            })
            public static class Cobr {

                /**
                 * The Fat.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTeOS.InfCte.InfCTeNorm.Cobr.Fat fat;
                /**
                 * The Dup.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected List<Dup> dup;

                /**
                 * Gets fat.
                 *
                 * @return the fat
                 */
                public TCTeOS.InfCte.InfCTeNorm.Cobr.Fat getFat() {
                    return fat;
                }

                /**
                 * Sets fat.
                 *
                 * @param value the value
                 */
                public void setFat(TCTeOS.InfCte.InfCTeNorm.Cobr.Fat value) {
                    this.fat = value;
                }

                /**
                 * Gets dup.
                 *
                 * @return the dup
                 */
                public List<Dup> getDup() {
                    if (dup == null) {
                        dup = new ArrayList<Dup>();
                    }
                    return this.dup;
                }


                /**
                 * The type Dup.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "nDup",
                        "dVenc",
                        "vDup"
                })
                public static class Dup {

                    /**
                     * The N dup.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nDup;
                    /**
                     * The D venc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String dVenc;
                    /**
                     * The V dup.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vDup;

                    /**
                     * Gets n dup.
                     *
                     * @return the n dup
                     */
                    public String getNDup() {
                        return nDup;
                    }

                    /**
                     * Sets n dup.
                     *
                     * @param value the value
                     */
                    public void setNDup(String value) {
                        this.nDup = value;
                    }

                    /**
                     * Gets d venc.
                     *
                     * @return the d venc
                     */
                    public String getDVenc() {
                        return dVenc;
                    }

                    /**
                     * Sets d venc.
                     *
                     * @param value the value
                     */
                    public void setDVenc(String value) {
                        this.dVenc = value;
                    }

                    /**
                     * Gets v dup.
                     *
                     * @return the v dup
                     */
                    public String getVDup() {
                        return vDup;
                    }

                    /**
                     * Sets v dup.
                     *
                     * @param value the value
                     */
                    public void setVDup(String value) {
                        this.vDup = value;
                    }

                }


                /**
                 * The type Fat.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "nFat",
                        "vOrig",
                        "vDesc",
                        "vLiq"
                })
                public static class Fat {

                    /**
                     * The N fat.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String nFat;
                    /**
                     * The V orig.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vOrig;
                    /**
                     * The V desc.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vDesc;
                    /**
                     * The V liq.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String vLiq;

                    /**
                     * Gets n fat.
                     *
                     * @return the n fat
                     */
                    public String getNFat() {
                        return nFat;
                    }

                    /**
                     * Sets n fat.
                     *
                     * @param value the value
                     */
                    public void setNFat(String value) {
                        this.nFat = value;
                    }

                    /**
                     * Gets v orig.
                     *
                     * @return the v orig
                     */
                    public String getVOrig() {
                        return vOrig;
                    }

                    /**
                     * Sets v orig.
                     *
                     * @param value the value
                     */
                    public void setVOrig(String value) {
                        this.vOrig = value;
                    }

                    /**
                     * Gets v desc.
                     *
                     * @return the v desc
                     */
                    public String getVDesc() {
                        return vDesc;
                    }

                    /**
                     * Sets v desc.
                     *
                     * @param value the value
                     */
                    public void setVDesc(String value) {
                        this.vDesc = value;
                    }

                    /**
                     * Gets v liq.
                     *
                     * @return the v liq
                     */
                    public String getVLiq() {
                        return vLiq;
                    }

                    /**
                     * Sets v liq.
                     *
                     * @param value the value
                     */
                    public void setVLiq(String value) {
                        this.vLiq = value;
                    }

                }

            }


            /**
             * The type Inf cte sub.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "chCte",
                    "refCteAnu",
                    "tomaICMS"
            })
            public static class InfCteSub {

                /**
                 * The Ch cte.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String chCte;
                /**
                 * The Ref cte anu.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String refCteAnu;
                /**
                 * The Toma icms.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS tomaICMS;

                /**
                 * Gets ch cte.
                 *
                 * @return the ch cte
                 */
                public String getChCte() {
                    return chCte;
                }

                /**
                 * Sets ch cte.
                 *
                 * @param value the value
                 */
                public void setChCte(String value) {
                    this.chCte = value;
                }

                /**
                 * Gets ref cte anu.
                 *
                 * @return the ref cte anu
                 */
                public String getRefCteAnu() {
                    return refCteAnu;
                }

                /**
                 * Sets ref cte anu.
                 *
                 * @param value the value
                 */
                public void setRefCteAnu(String value) {
                    this.refCteAnu = value;
                }

                /**
                 * Gets toma icms.
                 *
                 * @return the toma icms
                 */
                public TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS getTomaICMS() {
                    return tomaICMS;
                }

                /**
                 * Sets toma icms.
                 *
                 * @param value the value
                 */
                public void setTomaICMS(TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS value) {
                    this.tomaICMS = value;
                }


                /**
                 * The type Toma icms.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "refNFe",
                        "refNF",
                        "refCte"
                })
                public static class TomaICMS {

                    /**
                     * The Ref n fe.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String refNFe;
                    /**
                     * The Ref nf.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF refNF;
                    /**
                     * The Ref cte.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                    protected String refCte;

                    /**
                     * Gets ref n fe.
                     *
                     * @return the ref n fe
                     */
                    public String getRefNFe() {
                        return refNFe;
                    }

                    /**
                     * Sets ref n fe.
                     *
                     * @param value the value
                     */
                    public void setRefNFe(String value) {
                        this.refNFe = value;
                    }

                    /**
                     * Gets ref nf.
                     *
                     * @return the ref nf
                     */
                    public TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF getRefNF() {
                        return refNF;
                    }

                    /**
                     * Sets ref nf.
                     *
                     * @param value the value
                     */
                    public void setRefNF(TCTeOS.InfCte.InfCTeNorm.InfCteSub.TomaICMS.RefNF value) {
                        this.refNF = value;
                    }

                    /**
                     * Gets ref cte.
                     *
                     * @return the ref cte
                     */
                    public String getRefCte() {
                        return refCte;
                    }

                    /**
                     * Sets ref cte.
                     *
                     * @param value the value
                     */
                    public void setRefCte(String value) {
                        this.refCte = value;
                    }


                    /**
                     * The type Ref nf.
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                            "cnpj",
                            "cpf",
                            "mod",
                            "serie",
                            "subserie",
                            "nro",
                            "valor",
                            "dEmi"
                    })
                    public static class RefNF {

                        /**
                         * The Cnpj.
                         */
                        @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String cnpj;
                        /**
                         * The Cpf.
                         */
                        @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String cpf;
                        /**
                         * The Mod.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String mod;
                        /**
                         * The Serie.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String serie;
                        /**
                         * The Subserie.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                        protected String subserie;
                        /**
                         * The Nro.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String nro;
                        /**
                         * The Valor.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String valor;
                        /**
                         * The D emi.
                         */
                        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                        protected String dEmi;

                        /**
                         * Gets cnpj.
                         *
                         * @return the cnpj
                         */
                        public String getCNPJ() {
                            return cnpj;
                        }

                        /**
                         * Sets cnpj.
                         *
                         * @param value the value
                         */
                        public void setCNPJ(String value) {
                            this.cnpj = value;
                        }

                        /**
                         * Gets cpf.
                         *
                         * @return the cpf
                         */
                        public String getCPF() {
                            return cpf;
                        }

                        /**
                         * Sets cpf.
                         *
                         * @param value the value
                         */
                        public void setCPF(String value) {
                            this.cpf = value;
                        }

                        /**
                         * Gets mod.
                         *
                         * @return the mod
                         */
                        public String getMod() {
                            return mod;
                        }

                        /**
                         * Sets mod.
                         *
                         * @param value the value
                         */
                        public void setMod(String value) {
                            this.mod = value;
                        }

                        /**
                         * Gets serie.
                         *
                         * @return the serie
                         */
                        public String getSerie() {
                            return serie;
                        }

                        /**
                         * Sets serie.
                         *
                         * @param value the value
                         */
                        public void setSerie(String value) {
                            this.serie = value;
                        }

                        /**
                         * Gets subserie.
                         *
                         * @return the subserie
                         */
                        public String getSubserie() {
                            return subserie;
                        }

                        /**
                         * Sets subserie.
                         *
                         * @param value the value
                         */
                        public void setSubserie(String value) {
                            this.subserie = value;
                        }

                        /**
                         * Gets nro.
                         *
                         * @return the nro
                         */
                        public String getNro() {
                            return nro;
                        }

                        /**
                         * Sets nro.
                         *
                         * @param value the value
                         */
                        public void setNro(String value) {
                            this.nro = value;
                        }

                        /**
                         * Gets valor.
                         *
                         * @return the valor
                         */
                        public String getValor() {
                            return valor;
                        }

                        /**
                         * Sets valor.
                         *
                         * @param value the value
                         */
                        public void setValor(String value) {
                            this.valor = value;
                        }

                        /**
                         * Gets d emi.
                         *
                         * @return the d emi
                         */
                        public String getDEmi() {
                            return dEmi;
                        }

                        /**
                         * Sets d emi.
                         *
                         * @param value the value
                         */
                        public void setDEmi(String value) {
                            this.dEmi = value;
                        }

                    }

                }

            }


            /**
             * The type Inf doc ref.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "nDoc",
                    "serie",
                    "subserie",
                    "dEmi",
                    "vDoc"
            })
            public static class InfDocRef {

                /**
                 * The N doc.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String nDoc;
                /**
                 * The Serie.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String serie;
                /**
                 * The Subserie.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String subserie;
                /**
                 * The D emi.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String dEmi;
                /**
                 * The V doc.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String vDoc;

                /**
                 * Gets n doc.
                 *
                 * @return the n doc
                 */
                public String getNDoc() {
                    return nDoc;
                }

                /**
                 * Sets n doc.
                 *
                 * @param value the value
                 */
                public void setNDoc(String value) {
                    this.nDoc = value;
                }

                /**
                 * Gets serie.
                 *
                 * @return the serie
                 */
                public String getSerie() {
                    return serie;
                }

                /**
                 * Sets serie.
                 *
                 * @param value the value
                 */
                public void setSerie(String value) {
                    this.serie = value;
                }

                /**
                 * Gets subserie.
                 *
                 * @return the subserie
                 */
                public String getSubserie() {
                    return subserie;
                }

                /**
                 * Sets subserie.
                 *
                 * @param value the value
                 */
                public void setSubserie(String value) {
                    this.subserie = value;
                }

                /**
                 * Gets d emi.
                 *
                 * @return the d emi
                 */
                public String getDEmi() {
                    return dEmi;
                }

                /**
                 * Sets d emi.
                 *
                 * @param value the value
                 */
                public void setDEmi(String value) {
                    this.dEmi = value;
                }

                /**
                 * Gets v doc.
                 *
                 * @return the v doc
                 */
                public String getVDoc() {
                    return vDoc;
                }

                /**
                 * Sets v doc.
                 *
                 * @param value the value
                 */
                public void setVDoc(String value) {
                    this.vDoc = value;
                }

            }


            /**
             * The type Inf modal.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "any"
            })
            public static class InfModal {

                /**
                 * The Any.
                 */
                @XmlAnyElement
                protected Element any;
                /**
                 * The Versao modal.
                 */
                @XmlAttribute(name = "versaoModal", required = true)
                protected String versaoModal;

                /**
                 * Gets any.
                 *
                 * @return the any
                 */
                public Element getAny() {
                    return any;
                }

                /**
                 * Sets any.
                 *
                 * @param value the value
                 */
                public void setAny(Element value) {
                    this.any = value;
                }

                /**
                 * Gets versao modal.
                 *
                 * @return the versao modal
                 */
                public String getVersaoModal() {
                    return versaoModal;
                }

                /**
                 * Sets versao modal.
                 *
                 * @param value the value
                 */
                public void setVersaoModal(String value) {
                    this.versaoModal = value;
                }

            }


            /**
             * The type Inf servico.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xDescServ",
                    "infQ"
            })
            public static class InfServico {

                /**
                 * The X desc serv.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xDescServ;
                /**
                 * The Inf q.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected TCTeOS.InfCte.InfCTeNorm.InfServico.InfQ infQ;

                /**
                 * Gets x desc serv.
                 *
                 * @return the x desc serv
                 */
                public String getXDescServ() {
                    return xDescServ;
                }

                /**
                 * Sets x desc serv.
                 *
                 * @param value the value
                 */
                public void setXDescServ(String value) {
                    this.xDescServ = value;
                }

                /**
                 * Gets inf q.
                 *
                 * @return the inf q
                 */
                public TCTeOS.InfCte.InfCTeNorm.InfServico.InfQ getInfQ() {
                    return infQ;
                }

                /**
                 * Sets inf q.
                 *
                 * @param value the value
                 */
                public void setInfQ(TCTeOS.InfCte.InfCTeNorm.InfServico.InfQ value) {
                    this.infQ = value;
                }


                /**
                 * The type Inf q.
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "qCarga"
                })
                public static class InfQ {

                    /**
                     * The Q carga.
                     */
                    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                    protected String qCarga;

                    /**
                     * Gets q carga.
                     *
                     * @return the q carga
                     */
                    public String getQCarga() {
                        return qCarga;
                    }

                    /**
                     * Sets q carga.
                     *
                     * @param value the value
                     */
                    public void setQCarga(String value) {
                        this.qCarga = value;
                    }

                }

            }


            /**
             * The type Seg.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "respSeg",
                    "xSeg",
                    "nApol"
            })
            public static class Seg {

                /**
                 * The Resp seg.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String respSeg;
                /**
                 * The X seg.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String xSeg;
                /**
                 * The N apol.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
                protected String nApol;

                /**
                 * Gets resp seg.
                 *
                 * @return the resp seg
                 */
                public String getRespSeg() {
                    return respSeg;
                }

                /**
                 * Sets resp seg.
                 *
                 * @param value the value
                 */
                public void setRespSeg(String value) {
                    this.respSeg = value;
                }

                /**
                 * Gets x seg.
                 *
                 * @return the x seg
                 */
                public String getXSeg() {
                    return xSeg;
                }

                /**
                 * Sets x seg.
                 *
                 * @param value the value
                 */
                public void setXSeg(String value) {
                    this.xSeg = value;
                }

                /**
                 * Gets n apol.
                 *
                 * @return the n apol
                 */
                public String getNApol() {
                    return nApol;
                }

                /**
                 * Sets n apol.
                 *
                 * @param value the value
                 */
                public void setNApol(String value) {
                    this.nApol = value;
                }

            }

        }


        /**
         * The type Inf cte anu.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "chCte",
                "dEmi"
        })
        public static class InfCteAnu {

            /**
             * The Ch cte.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String chCte;
            /**
             * The D emi.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String dEmi;

            /**
             * Gets ch cte.
             *
             * @return the ch cte
             */
            public String getChCte() {
                return chCte;
            }

            /**
             * Sets ch cte.
             *
             * @param value the value
             */
            public void setChCte(String value) {
                this.chCte = value;
            }

            /**
             * Gets d emi.
             *
             * @return the d emi
             */
            public String getDEmi() {
                return dEmi;
            }

            /**
             * Sets d emi.
             *
             * @param value the value
             */
            public void setDEmi(String value) {
                this.dEmi = value;
            }

        }


        /**
         * The type Inf cte comp.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "chCTe"
        })
        public static class InfCteComp {

            /**
             * The Ch c te.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String chCTe;

            /**
             * Gets ch c te.
             *
             * @return the ch c te
             */
            public String getChCTe() {
                return chCTe;
            }

            /**
             * Sets ch c te.
             *
             * @param value the value
             */
            public void setChCTe(String value) {
                this.chCTe = value;
            }

        }


        /**
         * The type Toma.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "cnpj",
                "cpf",
                "ie",
                "xNome",
                "xFant",
                "fone",
                "enderToma",
                "email"
        })
        public static class Toma {

            /**
             * The Cnpj.
             */
            @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cnpj;
            /**
             * The Cpf.
             */
            @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String cpf;
            /**
             * The Ie.
             */
            @XmlElement(name = "IE", namespace = "http://www.portalfiscal.inf.br/cte")
            protected String ie;
            /**
             * The X nome.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String xNome;
            /**
             * The X fant.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String xFant;
            /**
             * The Fone.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String fone;
            /**
             * The Ender toma.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected TEndereco enderToma;
            /**
             * The Email.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
            protected String email;

            /**
             * Gets cnpj.
             *
             * @return the cnpj
             */
            public String getCNPJ() {
                return cnpj;
            }

            /**
             * Sets cnpj.
             *
             * @param value the value
             */
            public void setCNPJ(String value) {
                this.cnpj = value;
            }

            /**
             * Gets cpf.
             *
             * @return the cpf
             */
            public String getCPF() {
                return cpf;
            }

            /**
             * Sets cpf.
             *
             * @param value the value
             */
            public void setCPF(String value) {
                this.cpf = value;
            }

            /**
             * Gets ie.
             *
             * @return the ie
             */
            public String getIE() {
                return ie;
            }

            /**
             * Sets ie.
             *
             * @param value the value
             */
            public void setIE(String value) {
                this.ie = value;
            }

            /**
             * Gets x nome.
             *
             * @return the x nome
             */
            public String getXNome() {
                return xNome;
            }

            /**
             * Sets x nome.
             *
             * @param value the value
             */
            public void setXNome(String value) {
                this.xNome = value;
            }

            /**
             * Gets x fant.
             *
             * @return the x fant
             */
            public String getXFant() {
                return xFant;
            }

            /**
             * Sets x fant.
             *
             * @param value the value
             */
            public void setXFant(String value) {
                this.xFant = value;
            }

            /**
             * Gets fone.
             *
             * @return the fone
             */
            public String getFone() {
                return fone;
            }

            /**
             * Sets fone.
             *
             * @param value the value
             */
            public void setFone(String value) {
                this.fone = value;
            }

            /**
             * Gets ender toma.
             *
             * @return the ender toma
             */
            public TEndereco getEnderToma() {
                return enderToma;
            }

            /**
             * Sets ender toma.
             *
             * @param value the value
             */
            public void setEnderToma(TEndereco value) {
                this.enderToma = value;
            }

            /**
             * Gets email.
             *
             * @return the email
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets email.
             *
             * @param value the value
             */
            public void setEmail(String value) {
                this.email = value;
            }

        }


        /**
         * The type V prest.
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "vtPrest",
                "vRec",
                "comp"
        })
        public static class VPrest {

            /**
             * The Vt prest.
             */
            @XmlElement(name = "vTPrest", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String vtPrest;
            /**
             * The V rec.
             */
            @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
            protected String vRec;
            /**
             * The Comp.
             */
            @XmlElement(name = "Comp", namespace = "http://www.portalfiscal.inf.br/cte")
            protected List<Comp> comp;

            /**
             * Gets vt prest.
             *
             * @return the vt prest
             */
            public String getVTPrest() {
                return vtPrest;
            }

            /**
             * Sets vt prest.
             *
             * @param value the value
             */
            public void setVTPrest(String value) {
                this.vtPrest = value;
            }

            /**
             * Gets v rec.
             *
             * @return the v rec
             */
            public String getVRec() {
                return vRec;
            }

            /**
             * Sets v rec.
             *
             * @param value the value
             */
            public void setVRec(String value) {
                this.vRec = value;
            }

            /**
             * Gets comp.
             *
             * @return the comp
             */
            public List<Comp> getComp() {
                if (comp == null) {
                    comp = new ArrayList<Comp>();
                }
                return this.comp;
            }


            /**
             * The type Comp.
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "xNome",
                    "vComp"
            })
            public static class Comp {

                /**
                 * The X nome.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String xNome;
                /**
                 * The V comp.
                 */
                @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
                protected String vComp;

                /**
                 * Gets x nome.
                 *
                 * @return the x nome
                 */
                public String getXNome() {
                    return xNome;
                }

                /**
                 * Sets x nome.
                 *
                 * @param value the value
                 */
                public void setXNome(String value) {
                    this.xNome = value;
                }

                /**
                 * Gets v comp.
                 *
                 * @return the v comp
                 */
                public String getVComp() {
                    return vComp;
                }

                /**
                 * Sets v comp.
                 *
                 * @param value the value
                 */
                public void setVComp(String value) {
                    this.vComp = value;
                }

            }

        }

    }

}
