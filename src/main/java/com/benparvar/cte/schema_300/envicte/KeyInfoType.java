//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:46:03 PM BRT 
//


package com.benparvar.cte.schema_300.envicte;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type Key info type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyInfoType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "x509Data"
})
public class KeyInfoType {

    /**
     * The X 509 data.
     */
    @XmlElement(name = "X509Data", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected X509DataType x509Data;
    /**
     * The Id.
     */
    @XmlAttribute(name = "Id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets x 509 data.
     *
     * @return the x 509 data
     */
    public X509DataType getX509Data() {
        return x509Data;
    }

    /**
     * Sets x 509 data.
     *
     * @param value the value
     */
    public void setX509Data(X509DataType value) {
        this.x509Data = value;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param value the value
     */
    public void setId(String value) {
        this.id = value;
    }

}
