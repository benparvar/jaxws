//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:46:03 PM BRT 
//


package com.benparvar.cte.schema_300.envicte;

import javax.xml.bind.annotation.*;


/**
 * The type T end re ent.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TEndReEnt", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "cnpj",
        "cpf",
        "xNome",
        "xLgr",
        "nro",
        "xCpl",
        "xBairro",
        "cMun",
        "xMun",
        "uf"
})
public class TEndReEnt {

    /**
     * The Cnpj.
     */
    @XmlElement(name = "CNPJ", namespace = "http://www.portalfiscal.inf.br/cte")
    protected String cnpj;
    /**
     * The Cpf.
     */
    @XmlElement(name = "CPF", namespace = "http://www.portalfiscal.inf.br/cte")
    protected String cpf;
    /**
     * The X nome.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xNome;
    /**
     * The X lgr.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xLgr;
    /**
     * The Nro.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String nro;
    /**
     * The X cpl.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String xCpl;
    /**
     * The X bairro.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xBairro;
    /**
     * The C mun.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cMun;
    /**
     * The X mun.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMun;
    /**
     * The Uf.
     */
    @XmlElement(name = "UF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    @XmlSchemaType(name = "string")
    protected TUf uf;

    /**
     * Gets cnpj.
     *
     * @return the cnpj
     */
    public String getCNPJ() {
        return cnpj;
    }

    /**
     * Sets cnpj.
     *
     * @param value the value
     */
    public void setCNPJ(String value) {
        this.cnpj = value;
    }

    /**
     * Gets cpf.
     *
     * @return the cpf
     */
    public String getCPF() {
        return cpf;
    }

    /**
     * Sets cpf.
     *
     * @param value the value
     */
    public void setCPF(String value) {
        this.cpf = value;
    }

    /**
     * Gets x nome.
     *
     * @return the x nome
     */
    public String getXNome() {
        return xNome;
    }

    /**
     * Sets x nome.
     *
     * @param value the value
     */
    public void setXNome(String value) {
        this.xNome = value;
    }

    /**
     * Gets x lgr.
     *
     * @return the x lgr
     */
    public String getXLgr() {
        return xLgr;
    }

    /**
     * Sets x lgr.
     *
     * @param value the value
     */
    public void setXLgr(String value) {
        this.xLgr = value;
    }

    /**
     * Gets nro.
     *
     * @return the nro
     */
    public String getNro() {
        return nro;
    }

    /**
     * Sets nro.
     *
     * @param value the value
     */
    public void setNro(String value) {
        this.nro = value;
    }

    /**
     * Gets x cpl.
     *
     * @return the x cpl
     */
    public String getXCpl() {
        return xCpl;
    }

    /**
     * Sets x cpl.
     *
     * @param value the value
     */
    public void setXCpl(String value) {
        this.xCpl = value;
    }

    /**
     * Gets x bairro.
     *
     * @return the x bairro
     */
    public String getXBairro() {
        return xBairro;
    }

    /**
     * Sets x bairro.
     *
     * @param value the value
     */
    public void setXBairro(String value) {
        this.xBairro = value;
    }

    /**
     * Gets c mun.
     *
     * @return the c mun
     */
    public String getCMun() {
        return cMun;
    }

    /**
     * Sets c mun.
     *
     * @param value the value
     */
    public void setCMun(String value) {
        this.cMun = value;
    }

    /**
     * Gets x mun.
     *
     * @return the x mun
     */
    public String getXMun() {
        return xMun;
    }

    /**
     * Sets x mun.
     *
     * @param value the value
     */
    public void setXMun(String value) {
        this.xMun = value;
    }

    /**
     * Gets uf.
     *
     * @return the uf
     */
    public TUf getUF() {
        return uf;
    }

    /**
     * Sets uf.
     *
     * @param value the value
     */
    public void setUF(TUf value) {
        this.uf = value;
    }

}
