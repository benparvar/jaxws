//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:46:03 PM BRT 
//


package com.benparvar.cte.schema_300.envicte;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * The type T unid carga.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TUnidCarga", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpUnidCarga",
        "idUnidCarga",
        "lacUnidCarga",
        "qtdRat"
})
public class TUnidCarga {

    /**
     * The Tp unid carga.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpUnidCarga;
    /**
     * The Id unid carga.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String idUnidCarga;
    /**
     * The Lac unid carga.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<LacUnidCarga> lacUnidCarga;
    /**
     * The Qtd rat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String qtdRat;

    /**
     * Gets tp unid carga.
     *
     * @return the tp unid carga
     */
    public String getTpUnidCarga() {
        return tpUnidCarga;
    }

    /**
     * Sets tp unid carga.
     *
     * @param value the value
     */
    public void setTpUnidCarga(String value) {
        this.tpUnidCarga = value;
    }

    /**
     * Gets id unid carga.
     *
     * @return the id unid carga
     */
    public String getIdUnidCarga() {
        return idUnidCarga;
    }

    /**
     * Sets id unid carga.
     *
     * @param value the value
     */
    public void setIdUnidCarga(String value) {
        this.idUnidCarga = value;
    }

    /**
     * Gets lac unid carga.
     *
     * @return the lac unid carga
     */
    public List<LacUnidCarga> getLacUnidCarga() {
        if (lacUnidCarga == null) {
            lacUnidCarga = new ArrayList<LacUnidCarga>();
        }
        return this.lacUnidCarga;
    }

    /**
     * Gets qtd rat.
     *
     * @return the qtd rat
     */
    public String getQtdRat() {
        return qtdRat;
    }

    /**
     * Sets qtd rat.
     *
     * @param value the value
     */
    public void setQtdRat(String value) {
        this.qtdRat = value;
    }


    /**
     * The type Lac unid carga.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "nLacre"
    })
    public static class LacUnidCarga {

        /**
         * The N lacre.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nLacre;

        /**
         * Gets n lacre.
         *
         * @return the n lacre
         */
        public String getNLacre() {
            return nLacre;
        }

        /**
         * Sets n lacre.
         *
         * @param value the value
         */
        public void setNLacre(String value) {
            this.nLacre = value;
        }

    }

}
