//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:46:03 PM BRT 
//


package com.benparvar.cte.schema_300.envicte;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The type T imp.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TImp", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "icms00",
        "icms20",
        "icms45",
        "icms60",
        "icms90",
        "icmsOutraUF",
        "icmssn"
})
public class TImp {

    /**
     * The Icms 00.
     */
    @XmlElement(name = "ICMS00", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMS00 icms00;
    /**
     * The Icms 20.
     */
    @XmlElement(name = "ICMS20", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMS20 icms20;
    /**
     * The Icms 45.
     */
    @XmlElement(name = "ICMS45", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMS45 icms45;
    /**
     * The Icms 60.
     */
    @XmlElement(name = "ICMS60", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMS60 icms60;
    /**
     * The Icms 90.
     */
    @XmlElement(name = "ICMS90", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMS90 icms90;
    /**
     * The Icms outra uf.
     */
    @XmlElement(name = "ICMSOutraUF", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMSOutraUF icmsOutraUF;
    /**
     * The Icmssn.
     */
    @XmlElement(name = "ICMSSN", namespace = "http://www.portalfiscal.inf.br/cte")
    protected TImp.ICMSSN icmssn;

    /**
     * Gets icms 00.
     *
     * @return the icms 00
     */
    public TImp.ICMS00 getICMS00() {
        return icms00;
    }

    /**
     * Sets icms 00.
     *
     * @param value the value
     */
    public void setICMS00(TImp.ICMS00 value) {
        this.icms00 = value;
    }

    /**
     * Gets icms 20.
     *
     * @return the icms 20
     */
    public TImp.ICMS20 getICMS20() {
        return icms20;
    }

    /**
     * Sets icms 20.
     *
     * @param value the value
     */
    public void setICMS20(TImp.ICMS20 value) {
        this.icms20 = value;
    }

    /**
     * Gets icms 45.
     *
     * @return the icms 45
     */
    public TImp.ICMS45 getICMS45() {
        return icms45;
    }

    /**
     * Sets icms 45.
     *
     * @param value the value
     */
    public void setICMS45(TImp.ICMS45 value) {
        this.icms45 = value;
    }

    /**
     * Gets icms 60.
     *
     * @return the icms 60
     */
    public TImp.ICMS60 getICMS60() {
        return icms60;
    }

    /**
     * Sets icms 60.
     *
     * @param value the value
     */
    public void setICMS60(TImp.ICMS60 value) {
        this.icms60 = value;
    }

    /**
     * Gets icms 90.
     *
     * @return the icms 90
     */
    public TImp.ICMS90 getICMS90() {
        return icms90;
    }

    /**
     * Sets icms 90.
     *
     * @param value the value
     */
    public void setICMS90(TImp.ICMS90 value) {
        this.icms90 = value;
    }

    /**
     * Gets icms outra uf.
     *
     * @return the icms outra uf
     */
    public TImp.ICMSOutraUF getICMSOutraUF() {
        return icmsOutraUF;
    }

    /**
     * Sets icms outra uf.
     *
     * @param value the value
     */
    public void setICMSOutraUF(TImp.ICMSOutraUF value) {
        this.icmsOutraUF = value;
    }

    /**
     * Gets icmssn.
     *
     * @return the icmssn
     */
    public TImp.ICMSSN getICMSSN() {
        return icmssn;
    }

    /**
     * Sets icmssn.
     *
     * @param value the value
     */
    public void setICMSSN(TImp.ICMSSN value) {
        this.icmssn = value;
    }


    /**
     * The type Icms 00.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "vbc",
            "picms",
            "vicms"
    })
    public static class ICMS00 {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The Vbc.
         */
        @XmlElement(name = "vBC", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vbc;
        /**
         * The Picms.
         */
        @XmlElement(name = "pICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String picms;
        /**
         * The Vicms.
         */
        @XmlElement(name = "vICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vicms;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets vbc.
         *
         * @return the vbc
         */
        public String getVBC() {
            return vbc;
        }

        /**
         * Sets vbc.
         *
         * @param value the value
         */
        public void setVBC(String value) {
            this.vbc = value;
        }

        /**
         * Gets picms.
         *
         * @return the picms
         */
        public String getPICMS() {
            return picms;
        }

        /**
         * Sets picms.
         *
         * @param value the value
         */
        public void setPICMS(String value) {
            this.picms = value;
        }

        /**
         * Gets vicms.
         *
         * @return the vicms
         */
        public String getVICMS() {
            return vicms;
        }

        /**
         * Sets vicms.
         *
         * @param value the value
         */
        public void setVICMS(String value) {
            this.vicms = value;
        }

    }


    /**
     * The type Icms 20.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "pRedBC",
            "vbc",
            "picms",
            "vicms"
    })
    public static class ICMS20 {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The P red bc.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String pRedBC;
        /**
         * The Vbc.
         */
        @XmlElement(name = "vBC", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vbc;
        /**
         * The Picms.
         */
        @XmlElement(name = "pICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String picms;
        /**
         * The Vicms.
         */
        @XmlElement(name = "vICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vicms;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets p red bc.
         *
         * @return the p red bc
         */
        public String getPRedBC() {
            return pRedBC;
        }

        /**
         * Sets p red bc.
         *
         * @param value the value
         */
        public void setPRedBC(String value) {
            this.pRedBC = value;
        }

        /**
         * Gets vbc.
         *
         * @return the vbc
         */
        public String getVBC() {
            return vbc;
        }

        /**
         * Sets vbc.
         *
         * @param value the value
         */
        public void setVBC(String value) {
            this.vbc = value;
        }

        /**
         * Gets picms.
         *
         * @return the picms
         */
        public String getPICMS() {
            return picms;
        }

        /**
         * Sets picms.
         *
         * @param value the value
         */
        public void setPICMS(String value) {
            this.picms = value;
        }

        /**
         * Gets vicms.
         *
         * @return the vicms
         */
        public String getVICMS() {
            return vicms;
        }

        /**
         * Sets vicms.
         *
         * @param value the value
         */
        public void setVICMS(String value) {
            this.vicms = value;
        }

    }


    /**
     * The type Icms 45.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst"
    })
    public static class ICMS45 {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

    }


    /**
     * The type Icms 60.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "vbcstRet",
            "vicmsstRet",
            "picmsstRet",
            "vCred"
    })
    public static class ICMS60 {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The Vbcst ret.
         */
        @XmlElement(name = "vBCSTRet", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vbcstRet;
        /**
         * The Vicmsst ret.
         */
        @XmlElement(name = "vICMSSTRet", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vicmsstRet;
        /**
         * The Picmsst ret.
         */
        @XmlElement(name = "pICMSSTRet", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String picmsstRet;
        /**
         * The V cred.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String vCred;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets vbcst ret.
         *
         * @return the vbcst ret
         */
        public String getVBCSTRet() {
            return vbcstRet;
        }

        /**
         * Sets vbcst ret.
         *
         * @param value the value
         */
        public void setVBCSTRet(String value) {
            this.vbcstRet = value;
        }

        /**
         * Gets vicmsst ret.
         *
         * @return the vicmsst ret
         */
        public String getVICMSSTRet() {
            return vicmsstRet;
        }

        /**
         * Sets vicmsst ret.
         *
         * @param value the value
         */
        public void setVICMSSTRet(String value) {
            this.vicmsstRet = value;
        }

        /**
         * Gets picmsst ret.
         *
         * @return the picmsst ret
         */
        public String getPICMSSTRet() {
            return picmsstRet;
        }

        /**
         * Sets picmsst ret.
         *
         * @param value the value
         */
        public void setPICMSSTRet(String value) {
            this.picmsstRet = value;
        }

        /**
         * Gets v cred.
         *
         * @return the v cred
         */
        public String getVCred() {
            return vCred;
        }

        /**
         * Sets v cred.
         *
         * @param value the value
         */
        public void setVCred(String value) {
            this.vCred = value;
        }

    }


    /**
     * The type Icms 90.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "pRedBC",
            "vbc",
            "picms",
            "vicms",
            "vCred"
    })
    public static class ICMS90 {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The P red bc.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String pRedBC;
        /**
         * The Vbc.
         */
        @XmlElement(name = "vBC", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vbc;
        /**
         * The Picms.
         */
        @XmlElement(name = "pICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String picms;
        /**
         * The Vicms.
         */
        @XmlElement(name = "vICMS", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vicms;
        /**
         * The V cred.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String vCred;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets p red bc.
         *
         * @return the p red bc
         */
        public String getPRedBC() {
            return pRedBC;
        }

        /**
         * Sets p red bc.
         *
         * @param value the value
         */
        public void setPRedBC(String value) {
            this.pRedBC = value;
        }

        /**
         * Gets vbc.
         *
         * @return the vbc
         */
        public String getVBC() {
            return vbc;
        }

        /**
         * Sets vbc.
         *
         * @param value the value
         */
        public void setVBC(String value) {
            this.vbc = value;
        }

        /**
         * Gets picms.
         *
         * @return the picms
         */
        public String getPICMS() {
            return picms;
        }

        /**
         * Sets picms.
         *
         * @param value the value
         */
        public void setPICMS(String value) {
            this.picms = value;
        }

        /**
         * Gets vicms.
         *
         * @return the vicms
         */
        public String getVICMS() {
            return vicms;
        }

        /**
         * Sets vicms.
         *
         * @param value the value
         */
        public void setVICMS(String value) {
            this.vicms = value;
        }

        /**
         * Gets v cred.
         *
         * @return the v cred
         */
        public String getVCred() {
            return vCred;
        }

        /**
         * Sets v cred.
         *
         * @param value the value
         */
        public void setVCred(String value) {
            this.vCred = value;
        }

    }


    /**
     * The type Icms outra uf.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "pRedBCOutraUF",
            "vbcOutraUF",
            "picmsOutraUF",
            "vicmsOutraUF"
    })
    public static class ICMSOutraUF {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The P red bc outra uf.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String pRedBCOutraUF;
        /**
         * The Vbc outra uf.
         */
        @XmlElement(name = "vBCOutraUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vbcOutraUF;
        /**
         * The Picms outra uf.
         */
        @XmlElement(name = "pICMSOutraUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String picmsOutraUF;
        /**
         * The Vicms outra uf.
         */
        @XmlElement(name = "vICMSOutraUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String vicmsOutraUF;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets p red bc outra uf.
         *
         * @return the p red bc outra uf
         */
        public String getPRedBCOutraUF() {
            return pRedBCOutraUF;
        }

        /**
         * Sets p red bc outra uf.
         *
         * @param value the value
         */
        public void setPRedBCOutraUF(String value) {
            this.pRedBCOutraUF = value;
        }

        /**
         * Gets vbc outra uf.
         *
         * @return the vbc outra uf
         */
        public String getVBCOutraUF() {
            return vbcOutraUF;
        }

        /**
         * Sets vbc outra uf.
         *
         * @param value the value
         */
        public void setVBCOutraUF(String value) {
            this.vbcOutraUF = value;
        }

        /**
         * Gets picms outra uf.
         *
         * @return the picms outra uf
         */
        public String getPICMSOutraUF() {
            return picmsOutraUF;
        }

        /**
         * Sets picms outra uf.
         *
         * @param value the value
         */
        public void setPICMSOutraUF(String value) {
            this.picmsOutraUF = value;
        }

        /**
         * Gets vicms outra uf.
         *
         * @return the vicms outra uf
         */
        public String getVICMSOutraUF() {
            return vicmsOutraUF;
        }

        /**
         * Sets vicms outra uf.
         *
         * @param value the value
         */
        public void setVICMSOutraUF(String value) {
            this.vicmsOutraUF = value;
        }

    }


    /**
     * The type Icmssn.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cst",
            "indSN"
    })
    public static class ICMSSN {

        /**
         * The Cst.
         */
        @XmlElement(name = "CST", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cst;
        /**
         * The Ind sn.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String indSN;

        /**
         * Gets cst.
         *
         * @return the cst
         */
        public String getCST() {
            return cst;
        }

        /**
         * Sets cst.
         *
         * @param value the value
         */
        public void setCST(String value) {
            this.cst = value;
        }

        /**
         * Gets ind sn.
         *
         * @return the ind sn
         */
        public String getIndSN() {
            return indSN;
        }

        /**
         * Sets ind sn.
         *
         * @param value the value
         */
        public void setIndSN(String value) {
            this.indSN = value;
        }

    }

}
