//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:47:26 PM BRT 
//


package com.benparvar.cte.schema_300.evcanccte;

import javax.xml.bind.annotation.*;


/**
 * The type T proc evento.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProcEvento", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "eventoCTe",
        "retEventoCTe"
})
public class TProcEvento {

    /**
     * The Evento c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TEvento eventoCTe;
    /**
     * The Ret evento c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TRetEvento retEventoCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;
    /**
     * The Ip transmissor.
     */
    @XmlAttribute(name = "ipTransmissor")
    protected String ipTransmissor;

    /**
     * Gets evento c te.
     *
     * @return the evento c te
     */
    public TEvento getEventoCTe() {
        return eventoCTe;
    }

    /**
     * Sets evento c te.
     *
     * @param value the value
     */
    public void setEventoCTe(TEvento value) {
        this.eventoCTe = value;
    }

    /**
     * Gets ret evento c te.
     *
     * @return the ret evento c te
     */
    public TRetEvento getRetEventoCTe() {
        return retEventoCTe;
    }

    /**
     * Sets ret evento c te.
     *
     * @param value the value
     */
    public void setRetEventoCTe(TRetEvento value) {
        this.retEventoCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

    /**
     * Gets ip transmissor.
     *
     * @return the ip transmissor
     */
    public String getIpTransmissor() {
        return ipTransmissor;
    }

    /**
     * Sets ip transmissor.
     *
     * @param value the value
     */
    public void setIpTransmissor(String value) {
        this.ipTransmissor = value;
    }

}
