//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:45:09 PM BRT 
//


package com.benparvar.cte.schema_300.cteos;

import javax.xml.bind.annotation.*;


/**
 * The type T ret c te os.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRetCTeOS", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpAmb",
        "cuf",
        "verAplic",
        "cStat",
        "xMotivo",
        "protCTe"
})
public class TRetCTeOS {

    /**
     * The Tp amb.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpAmb;
    /**
     * The Cuf.
     */
    @XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cuf;
    /**
     * The Ver aplic.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String verAplic;
    /**
     * The C stat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String cStat;
    /**
     * The X motivo.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String xMotivo;
    /**
     * The Prot c te.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected TProtCTeOS protCTe;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets tp amb.
     *
     * @return the tp amb
     */
    public String getTpAmb() {
        return tpAmb;
    }

    /**
     * Sets tp amb.
     *
     * @param value the value
     */
    public void setTpAmb(String value) {
        this.tpAmb = value;
    }

    /**
     * Gets cuf.
     *
     * @return the cuf
     */
    public String getCUF() {
        return cuf;
    }

    /**
     * Sets cuf.
     *
     * @param value the value
     */
    public void setCUF(String value) {
        this.cuf = value;
    }

    /**
     * Gets ver aplic.
     *
     * @return the ver aplic
     */
    public String getVerAplic() {
        return verAplic;
    }

    /**
     * Sets ver aplic.
     *
     * @param value the value
     */
    public void setVerAplic(String value) {
        this.verAplic = value;
    }

    /**
     * Gets c stat.
     *
     * @return the c stat
     */
    public String getCStat() {
        return cStat;
    }

    /**
     * Sets c stat.
     *
     * @param value the value
     */
    public void setCStat(String value) {
        this.cStat = value;
    }

    /**
     * Gets x motivo.
     *
     * @return the x motivo
     */
    public String getXMotivo() {
        return xMotivo;
    }

    /**
     * Sets x motivo.
     *
     * @param value the value
     */
    public void setXMotivo(String value) {
        this.xMotivo = value;
    }

    /**
     * Gets prot c te.
     *
     * @return the prot c te
     */
    public TProtCTeOS getProtCTe() {
        return protCTe;
    }

    /**
     * Sets prot c te.
     *
     * @param value the value
     */
    public void setProtCTe(TProtCTeOS value) {
        this.protCTe = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
