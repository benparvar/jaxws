//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:45:09 PM BRT 
//


package com.benparvar.cte.schema_300.cteos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * The type T unidade transp.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TUnidadeTransp", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "tpUnidTransp",
        "idUnidTransp",
        "lacUnidTransp",
        "infUnidCarga",
        "qtdRat"
})
public class TUnidadeTransp {

    /**
     * The Tp unid transp.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String tpUnidTransp;
    /**
     * The Id unid transp.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected String idUnidTransp;
    /**
     * The Lac unid transp.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<LacUnidTransp> lacUnidTransp;
    /**
     * The Inf unid carga.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected List<TUnidCarga> infUnidCarga;
    /**
     * The Qtd rat.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected String qtdRat;

    /**
     * Gets tp unid transp.
     *
     * @return the tp unid transp
     */
    public String getTpUnidTransp() {
        return tpUnidTransp;
    }

    /**
     * Sets tp unid transp.
     *
     * @param value the value
     */
    public void setTpUnidTransp(String value) {
        this.tpUnidTransp = value;
    }

    /**
     * Gets id unid transp.
     *
     * @return the id unid transp
     */
    public String getIdUnidTransp() {
        return idUnidTransp;
    }

    /**
     * Sets id unid transp.
     *
     * @param value the value
     */
    public void setIdUnidTransp(String value) {
        this.idUnidTransp = value;
    }

    /**
     * Gets lac unid transp.
     *
     * @return the lac unid transp
     */
    public List<LacUnidTransp> getLacUnidTransp() {
        if (lacUnidTransp == null) {
            lacUnidTransp = new ArrayList<LacUnidTransp>();
        }
        return this.lacUnidTransp;
    }

    /**
     * Gets inf unid carga.
     *
     * @return the inf unid carga
     */
    public List<TUnidCarga> getInfUnidCarga() {
        if (infUnidCarga == null) {
            infUnidCarga = new ArrayList<TUnidCarga>();
        }
        return this.infUnidCarga;
    }

    /**
     * Gets qtd rat.
     *
     * @return the qtd rat
     */
    public String getQtdRat() {
        return qtdRat;
    }

    /**
     * Sets qtd rat.
     *
     * @param value the value
     */
    public void setQtdRat(String value) {
        this.qtdRat = value;
    }


    /**
     * The type Lac unid transp.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "nLacre"
    })
    public static class LacUnidTransp {

        /**
         * The N lacre.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String nLacre;

        /**
         * Gets n lacre.
         *
         * @return the n lacre
         */
        public String getNLacre() {
            return nLacre;
        }

        /**
         * Sets n lacre.
         *
         * @param value the value
         */
        public void setNLacre(String value) {
            this.nLacre = value;
        }

    }

}
