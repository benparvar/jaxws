//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:45:09 PM BRT 
//


package com.benparvar.cte.schema_300.cteos;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type T prot c te os.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProtCTeOS", namespace = "http://www.portalfiscal.inf.br/cte", propOrder = {
        "infProt",
        "infFisco",
        "signature"
})
public class TProtCTeOS {

    /**
     * The Inf prot.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
    protected TProtCTeOS.InfProt infProt;
    /**
     * The Inf fisco.
     */
    @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
    protected TProtCTeOS.InfFisco infFisco;
    /**
     * The Signature.
     */
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;
    /**
     * The Versao.
     */
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Gets inf prot.
     *
     * @return the inf prot
     */
    public TProtCTeOS.InfProt getInfProt() {
        return infProt;
    }

    /**
     * Sets inf prot.
     *
     * @param value the value
     */
    public void setInfProt(TProtCTeOS.InfProt value) {
        this.infProt = value;
    }

    /**
     * Gets inf fisco.
     *
     * @return the inf fisco
     */
    public TProtCTeOS.InfFisco getInfFisco() {
        return infFisco;
    }

    /**
     * Sets inf fisco.
     *
     * @param value the value
     */
    public void setInfFisco(TProtCTeOS.InfFisco value) {
        this.infFisco = value;
    }

    /**
     * Gets signature.
     *
     * @return the signature
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets signature.
     *
     * @param value the value
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Sets versao.
     *
     * @param value the value
     */
    public void setVersao(String value) {
        this.versao = value;
    }


    /**
     * The type Inf fisco.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cMsg",
            "xMsg"
    })
    public static class InfFisco {

        /**
         * The C msg.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cMsg;
        /**
         * The X msg.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xMsg;

        /**
         * Gets c msg.
         *
         * @return the c msg
         */
        public String getCMsg() {
            return cMsg;
        }

        /**
         * Sets c msg.
         *
         * @param value the value
         */
        public void setCMsg(String value) {
            this.cMsg = value;
        }

        /**
         * Gets x msg.
         *
         * @return the x msg
         */
        public String getXMsg() {
            return xMsg;
        }

        /**
         * Sets x msg.
         *
         * @param value the value
         */
        public void setXMsg(String value) {
            this.xMsg = value;
        }

    }


    /**
     * The type Inf prot.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "tpAmb",
            "verAplic",
            "chCTe",
            "dhRecbto",
            "nProt",
            "digVal",
            "cStat",
            "xMotivo"
    })
    public static class InfProt {

        /**
         * The Tp amb.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String tpAmb;
        /**
         * The Ver aplic.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String verAplic;
        /**
         * The Ch c te.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String chCTe;
        /**
         * The Dh recbto.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String dhRecbto;
        /**
         * The N prot.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected String nProt;
        /**
         * The Dig val.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte")
        protected byte[] digVal;
        /**
         * The C stat.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String cStat;
        /**
         * The X motivo.
         */
        @XmlElement(namespace = "http://www.portalfiscal.inf.br/cte", required = true)
        protected String xMotivo;
        /**
         * The Id.
         */
        @XmlAttribute(name = "Id")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        @XmlSchemaType(name = "ID")
        protected String id;

        /**
         * Gets tp amb.
         *
         * @return the tp amb
         */
        public String getTpAmb() {
            return tpAmb;
        }

        /**
         * Sets tp amb.
         *
         * @param value the value
         */
        public void setTpAmb(String value) {
            this.tpAmb = value;
        }

        /**
         * Gets ver aplic.
         *
         * @return the ver aplic
         */
        public String getVerAplic() {
            return verAplic;
        }

        /**
         * Sets ver aplic.
         *
         * @param value the value
         */
        public void setVerAplic(String value) {
            this.verAplic = value;
        }

        /**
         * Gets ch c te.
         *
         * @return the ch c te
         */
        public String getChCTe() {
            return chCTe;
        }

        /**
         * Sets ch c te.
         *
         * @param value the value
         */
        public void setChCTe(String value) {
            this.chCTe = value;
        }

        /**
         * Gets dh recbto.
         *
         * @return the dh recbto
         */
        public String getDhRecbto() {
            return dhRecbto;
        }

        /**
         * Sets dh recbto.
         *
         * @param value the value
         */
        public void setDhRecbto(String value) {
            this.dhRecbto = value;
        }

        /**
         * Gets n prot.
         *
         * @return the n prot
         */
        public String getNProt() {
            return nProt;
        }

        /**
         * Sets n prot.
         *
         * @param value the value
         */
        public void setNProt(String value) {
            this.nProt = value;
        }

        /**
         * Get dig val byte [ ].
         *
         * @return the byte [ ]
         */
        public byte[] getDigVal() {
            return digVal;
        }

        /**
         * Sets dig val.
         *
         * @param value the value
         */
        public void setDigVal(byte[] value) {
            this.digVal = value;
        }

        /**
         * Gets c stat.
         *
         * @return the c stat
         */
        public String getCStat() {
            return cStat;
        }

        /**
         * Sets c stat.
         *
         * @param value the value
         */
        public void setCStat(String value) {
            this.cStat = value;
        }

        /**
         * Gets x motivo.
         *
         * @return the x motivo
         */
        public String getXMotivo() {
            return xMotivo;
        }

        /**
         * Sets x motivo.
         *
         * @param value the value
         */
        public void setXMotivo(String value) {
            this.xMotivo = value;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Sets id.
         *
         * @param value the value
         */
        public void setId(String value) {
            this.id = value;
        }

    }

}
