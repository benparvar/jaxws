//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 07:49:41 PM BRT 
//


package com.benparvar.cte.schema_300.evgtv;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The type Signature type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignatureType", namespace = "http://www.w3.org/2000/09/xmldsig#", propOrder = {
        "signedInfo",
        "signatureValue",
        "keyInfo"
})
public class SignatureType {

    /**
     * The Signed info.
     */
    @XmlElement(name = "SignedInfo", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignedInfoType signedInfo;
    /**
     * The Signature value.
     */
    @XmlElement(name = "SignatureValue", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureValueType signatureValue;
    /**
     * The Key info.
     */
    @XmlElement(name = "KeyInfo", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected KeyInfoType keyInfo;
    /**
     * The Id.
     */
    @XmlAttribute(name = "Id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets signed info.
     *
     * @return the signed info
     */
    public SignedInfoType getSignedInfo() {
        return signedInfo;
    }

    /**
     * Sets signed info.
     *
     * @param value the value
     */
    public void setSignedInfo(SignedInfoType value) {
        this.signedInfo = value;
    }

    /**
     * Gets signature value.
     *
     * @return the signature value
     */
    public SignatureValueType getSignatureValue() {
        return signatureValue;
    }

    /**
     * Sets signature value.
     *
     * @param value the value
     */
    public void setSignatureValue(SignatureValueType value) {
        this.signatureValue = value;
    }

    /**
     * Gets key info.
     *
     * @return the key info
     */
    public KeyInfoType getKeyInfo() {
        return keyInfo;
    }

    /**
     * Sets key info.
     *
     * @param value the value
     */
    public void setKeyInfo(KeyInfoType value) {
        this.keyInfo = value;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param value the value
     */
    public void setId(String value) {
        this.id = value;
    }

}
