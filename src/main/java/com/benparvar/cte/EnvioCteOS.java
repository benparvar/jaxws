package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.AssinaturaEnum;
import com.benparvar.cte.dom.enuns.EstadosEnum;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.cteos.TCTeOS;
import com.benparvar.cte.schema_300.retcteos.TRetCTeOS;
import com.benparvar.cte.util.ConstantesCte;
import com.benparvar.cte.util.LoggerUtil;
import com.benparvar.cte.util.WebServiceCteUtil;
import com.benparvar.cte.util.XmlCteUtil;
import com.benparvar.cte.wsdl.cterecepcaoos.CteRecepcaoOSStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;
import java.util.Iterator;

/**
 * The type Envio cte os.
 */
class EnvioCteOS {

    /**
     * Monta cte os tc te os.
     *
     * @param config  the config
     * @param enviCTe the envi c te
     * @param valida  the valida
     * @return the tc te os
     * @throws CteException the cte exception
     */
    static TCTeOS montaCteOS(ConfiguracoesCte config, TCTeOS enviCTe, boolean valida) throws CteException {
        try {

            /**
             * Cria o xml
             */
            String xml = XmlCteUtil.objectToXml(enviCTe);

            /**
             * Assina o Xml
             */
            xml = Assinar.assinaCte(config, xml, AssinaturaEnum.CTE_OS);

            //Retira Quebra de Linha
            xml = xml.replaceAll(System.lineSeparator(), "");

            LoggerUtil.log(EnvioCteOS.class, "[XML-ASSINADO]: " + xml);

            /**
             * Valida o Xml caso sejá selecionado True
             */
            if (valida) {
                new Validar().validaXml(config, xml, ServicosEnum.ENVIO_CTE_OS);
            }

            return XmlCteUtil.xmlToObject(xml, TCTeOS.class);
        } catch (JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }

    /**
     * Envia cte os t ret c te os.
     *
     * @param config  the config
     * @param enviCTe the envi c te
     * @return the t ret c te os
     * @throws CteException the cte exception
     */
    static TRetCTeOS enviaCteOS(ConfiguracoesCte config, TCTeOS enviCTe)
            throws CteException {

        try {

            String xml = XmlCteUtil.objectToXml(enviCTe);
            OMElement ome = AXIOMUtil.stringToOM(xml);

            if (config.getEstado().equals(EstadosEnum.PR)) {
                Iterator<?> children = ome.getChildrenWithLocalName("CTe");
                while (children.hasNext()) {
                    OMElement omElement = (OMElement) children.next();
                    if (omElement != null && "CTe".equals(omElement.getLocalName())) {
                        omElement.addAttribute("xmlns", "http://www.portalfiscal.inf.br/cte", null);
                    }
                }
            }

            LoggerUtil.log(EnvioCteOS.class, "[XML-ENVIO]: " + ome);

            CteRecepcaoOSStub.CteDadosMsg dadosMsg = new CteRecepcaoOSStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);
            CteRecepcaoOSStub.CteCabecMsg cteCabecMsg = new CteRecepcaoOSStub.CteCabecMsg();

            /**
             * Codigo do Estado.
             */
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));

            /**
             * Versao do XML
             */
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteRecepcaoOSStub.CteCabecMsgE cteCabecMsgE = new CteRecepcaoOSStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            CteRecepcaoOSStub stub = new CteRecepcaoOSStub(
                    WebServiceCteUtil.getUrl(config, ServicosEnum.ENVIO_CTE_OS));
            CteRecepcaoOSStub.CteRecepcaoOSResult result = stub.cteRecepcaoOS(dadosMsg, cteCabecMsgE);

            return XmlCteUtil.xmlToObject(result.getExtraElement().toString(), TRetCTeOS.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }

}
