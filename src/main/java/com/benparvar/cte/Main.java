package com.benparvar.cte;

import br.com.swconsultoria.certificado.Certificado;
import br.com.swconsultoria.certificado.CertificadoService;
import br.com.swconsultoria.certificado.exception.CertificadoException;
import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.AmbienteEnum;
import com.benparvar.cte.dom.enuns.ConsultaDFeEnum;
import com.benparvar.cte.dom.enuns.EstadosEnum;
import com.benparvar.cte.dom.enuns.PessoaEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.retdistdfeint.RetDistDFeInt;
import com.benparvar.cte.util.XmlCteUtil;

import java.io.IOException;
import java.util.List;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        try {

            // Certificado em arquivo PFX
//            String cert = System.getProperty("user.dir") + "\\src\\main\\resources\\certificados\\homologacao.pfx";
//            Certificado certificado = CertificadoService.certificadoPfx(cert, "senha");

            String cert = System.getProperty("user.dir") + "\\src\\main\\resources\\certificados\\associacao.pfx";
            Certificado certificado = CertificadoService.certificadoPfx(cert, "associacao");
            System.out.println("Alias Certificado :" + certificado.getNome());
            System.out.println("Dias Restantes Certificado :" + certificado.getDiasRestantes());
            System.out.println("Validade Certificado :" + certificado.getVencimento());

            // Certificado em Windows
            //Certificado certificado = CertificadoService.listaCertificadosWindows().get(0);

            //
            ConfiguracoesCte config = ConfiguracoesCte.criarConfiguracoes(EstadosEnum.SP, AmbienteEnum.HOMOLOGACAO,
                    certificado, "");

            String documento = "276.193.418-03";
            String nsu = "000000000000000";

            RetDistDFeInt retorno = Cte.distribuicaoDfe(config, PessoaEnum.FISICA, documento, ConsultaDFeEnum.NSU, nsu);
            System.out.println("Status:" + retorno.getCStat());
            System.out.println("Motivo:" + retorno.getXMotivo());
            System.out.println("Max NSU:" + retorno.getMaxNSU());
            System.out.println("Ult NSU:" + retorno.getUltNSU());

//            if (StatusEnum.DOC_LOCALIZADO_PARA_DESTINATARIO.equals(retorno.getCStat())) {
            if (retorno.getCStat() != null) {
                List<RetDistDFeInt.LoteDistDFeInt.DocZip> listaDoc = retorno.getLoteDistDFeInt().getDocZip();

                System.out.println("Encontrado " + listaDoc.size() + " Notas.");
                for (RetDistDFeInt.LoteDistDFeInt.DocZip docZip : listaDoc) {
                    System.out.println("Schema: " + docZip.getSchema());
                    System.out.println("NSU:" + docZip.getNSU());
                    System.out.println("XML: " + XmlCteUtil.gZipToXml(docZip.getValue()));
                }
            }

        } catch (CteException | IOException | CertificadoException e) {
            System.out.println("Erro:" + e.getMessage());
        }

    }
}
