package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.evepeccte.TEvento;
import com.benparvar.cte.schema_300.evepeccte.TRetEvento;
import com.benparvar.cte.util.XmlCteUtil;

import javax.xml.bind.JAXBException;

/**
 * The type Epec.
 */
class Epec {

    /**
     * Evento epec t ret evento.
     *
     * @param config     the config
     * @param enviEvento the envi evento
     * @param valida     the valida
     * @return the t ret evento
     * @throws CteException the cte exception
     */
    static TRetEvento eventoEpec(ConfiguracoesCte config, TEvento enviEvento, boolean valida) throws CteException {

        try {

            String xml = XmlCteUtil.objectToXml(enviEvento);
            xml = xml.replaceAll(" xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"", "");
            xml = xml.replaceAll("<evento v", "<evento xmlns=\"http://www.portalfiscal.inf.br/cte\" v");

            xml = Eventos.enviarEvento(config, xml, ServicosEnum.EPEC, valida);

            return XmlCteUtil.xmlToObject(xml, TRetEvento.class);

        } catch (JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }

}
