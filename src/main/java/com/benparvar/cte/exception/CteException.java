package com.benparvar.cte.exception;

/**
 * The type Cte exception.
 */
public class CteException extends Exception {

    private static final long serialVersionUID = -5054900660251852366L;

    /**
     * The Message.
     */
    String message;

    /**
     * Instantiates a new Cte exception.
     *
     * @param e the e
     */
    public CteException(Throwable e) {
        super(e);
    }


    /**
     * Instantiates a new Cte exception.
     *
     * @param message the message
     */
    public CteException(String message) {
        this((Throwable) null);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }


}