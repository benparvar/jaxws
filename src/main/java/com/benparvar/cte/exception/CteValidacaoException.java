package com.benparvar.cte.exception;

/**
 * The type Cte validacao exception.
 */
public class CteValidacaoException extends CteException {

    private static final long serialVersionUID = 2224963351733125955L;
    /**
     * The Message.
     */
    String message;

    /**
     * Instantiates a new Cte validacao exception.
     *
     * @param e the e
     */
    public CteValidacaoException(Throwable e) {
        super(e);
    }

    /**
     * Instantiates a new Cte validacao exception.
     *
     * @param message the message
     */
    public CteValidacaoException(String message) {
        this((Throwable) null);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}