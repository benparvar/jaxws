package com.benparvar.cte.util;


import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Logger util.
 */
public class LoggerUtil {

    private static Logger LOGGER = Logger.getLogger("");

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tD-%1$tT][%2$1s]%5$s %n");
    }

    /**
     * Log.
     *
     * @param classe the classe
     * @param msg    the msg
     */
    public static void log(Class classe, String msg) {
        LOGGER.logp(Level.INFO, classe.getName(), null, msg);
    }

}
