package com.benparvar.cte.util;

import com.benparvar.cte.schema_300.ctemodalrodoviario.Rodo;
import com.benparvar.cte.schema_300.evcanccte.TEvento;
import com.benparvar.cte.schema_300.evcanccte.TProcEvento;
import com.benparvar.cte.schema_300.evregmultimodal.TRetEvento;
import com.benparvar.cte.schema_300.inutcte.TProcInutCTe;
import com.benparvar.cte.schema_300.inutcte.TRetInutCTe;
import com.benparvar.cte.schema_300.proccte.CteProc;
import com.benparvar.cte.schema_300.proccte.TCTe;
import com.benparvar.cte.schema_300.proccte.TProtCTe;
import com.benparvar.cte.schema_300.proccteos.CteOSProc;
import com.benparvar.cte.schema_300.retcteos.TProtCTeOS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * The type Xsd cte util.
 */
@XmlRegistry
class XsdCteUtil {

    private final static QName _RetEvento_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "retEvento");
    private final static QName _CteProc_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "cteProc");
    private final static QName _Rodo_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "rodo");
    private final static QName _CteOSProc_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "cteOSProc");
    private final static QName _evento_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "eventoCTe");
    private final static QName _procEvento_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "procEvento");
    private final static QName _procInut_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "procInutCTe");
    private final static QName _RetInutilizacao_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "retInutCTe");
    private final static QName _TProtCTe_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "protCTe");
    private final static QName _CTe_QNAME = new QName("http://www.portalfiscal.inf.br/cte", "TCTe");

    /**
     * The interface Ret evento.
     */
    public interface retEvento {
        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = com.benparvar.cte.schema_300.evcanccte.TRetEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evcanccte.TRetEvento> createTRetEvent(com.benparvar.cte.schema_300.evcanccte.TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, com.benparvar.cte.schema_300.evcanccte.TRetEvento.class, com.benparvar.cte.schema_300.evcanccte.TRetEvento.class, value);
        }

        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = com.benparvar.cte.schema_300.evepeccte.TRetEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evepeccte.TRetEvento> createTRetEvent(com.benparvar.cte.schema_300.evepeccte.TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, com.benparvar.cte.schema_300.evepeccte.TRetEvento.class, com.benparvar.cte.schema_300.evepeccte.TRetEvento.class, value);
        }

        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = TRetEvento.class)
        static JAXBElement<TRetEvento> createTRetEvent(TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, TRetEvento.class, TRetEvento.class, value);
        }

        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = com.benparvar.cte.schema_300.evccecte.TRetEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evccecte.TRetEvento> createTRetEvent(com.benparvar.cte.schema_300.evccecte.TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, com.benparvar.cte.schema_300.evccecte.TRetEvento.class, com.benparvar.cte.schema_300.evccecte.TRetEvento.class, value);
        }

        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento> createTRetEvent(com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento.class, com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento.class, value);
        }

        /**
         * Create t ret event jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetEvento", scope = com.benparvar.cte.schema_300.evgtv.TRetEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evgtv.TRetEvento> createTRetEvent(com.benparvar.cte.schema_300.evgtv.TRetEvento value) {
            return new JAXBElement<>(_RetEvento_QNAME, com.benparvar.cte.schema_300.evgtv.TRetEvento.class, com.benparvar.cte.schema_300.evgtv.TRetEvento.class, value);
        }

    }

    /**
     * The interface Proc evento.
     */
    public interface procEvento {
        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = TProcEvento.class)
        static JAXBElement<TProcEvento> createTProcEvento(TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, TProcEvento.class, TProcEvento.class, value);
        }

        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = com.benparvar.cte.schema_300.evepeccte.TProcEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evepeccte.TProcEvento> createTProcEvento(com.benparvar.cte.schema_300.evepeccte.TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, com.benparvar.cte.schema_300.evepeccte.TProcEvento.class, com.benparvar.cte.schema_300.evepeccte.TProcEvento.class, value);
        }

        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = com.benparvar.cte.schema_300.evregmultimodal.TProcEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evregmultimodal.TProcEvento> createTProcEvento(com.benparvar.cte.schema_300.evregmultimodal.TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, com.benparvar.cte.schema_300.evregmultimodal.TProcEvento.class, com.benparvar.cte.schema_300.evregmultimodal.TProcEvento.class, value);
        }

        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = com.benparvar.cte.schema_300.evccecte.TProcEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evccecte.TProcEvento> createTProcEvento(com.benparvar.cte.schema_300.evccecte.TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, com.benparvar.cte.schema_300.evccecte.TProcEvento.class, com.benparvar.cte.schema_300.evccecte.TProcEvento.class, value);
        }

        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento> createTProcEvento(com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento.class, com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento.class, value);
        }

        /**
         * Create t proc evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TProcEvento", scope = com.benparvar.cte.schema_300.evgtv.TProcEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evgtv.TProcEvento> createTProcEvento(com.benparvar.cte.schema_300.evgtv.TProcEvento value) {
            return new JAXBElement<>(_procEvento_QNAME, com.benparvar.cte.schema_300.evgtv.TProcEvento.class, com.benparvar.cte.schema_300.evgtv.TProcEvento.class, value);
        }

    }

    /**
     * The interface Evento.
     */
    public interface evento {
        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = TEvento.class)
        static JAXBElement<TEvento> createTEvento(TEvento value) {
            return new JAXBElement<>(_evento_QNAME, TEvento.class, TEvento.class, value);
        }

        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = com.benparvar.cte.schema_300.evepeccte.TEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evepeccte.TEvento> createTEvento(com.benparvar.cte.schema_300.evepeccte.TEvento value) {
            return new JAXBElement<>(_evento_QNAME, com.benparvar.cte.schema_300.evepeccte.TEvento.class, com.benparvar.cte.schema_300.evepeccte.TEvento.class, value);
        }

        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = com.benparvar.cte.schema_300.evregmultimodal.TEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evregmultimodal.TEvento> createTEvento(com.benparvar.cte.schema_300.evregmultimodal.TEvento value) {
            return new JAXBElement<>(_evento_QNAME, com.benparvar.cte.schema_300.evregmultimodal.TEvento.class, com.benparvar.cte.schema_300.evregmultimodal.TEvento.class, value);
        }

        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = com.benparvar.cte.schema_300.evccecte.TEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evccecte.TEvento> createTEvento(com.benparvar.cte.schema_300.evccecte.TEvento value) {
            return new JAXBElement<>(_evento_QNAME, com.benparvar.cte.schema_300.evccecte.TEvento.class, com.benparvar.cte.schema_300.evccecte.TEvento.class, value);
        }

        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = com.benparvar.cte.schema_300.evprestdesacordo.TEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evprestdesacordo.TEvento> createTEvento(com.benparvar.cte.schema_300.evprestdesacordo.TEvento value) {
            return new JAXBElement<>(_evento_QNAME, com.benparvar.cte.schema_300.evprestdesacordo.TEvento.class, com.benparvar.cte.schema_300.evprestdesacordo.TEvento.class, value);
        }

        /**
         * Create t evento jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TEvento", scope = com.benparvar.cte.schema_300.evgtv.TEvento.class)
        static JAXBElement<com.benparvar.cte.schema_300.evgtv.TEvento> createTEvento(com.benparvar.cte.schema_300.evgtv.TEvento value) {
            return new JAXBElement<>(_evento_QNAME, com.benparvar.cte.schema_300.evgtv.TEvento.class, com.benparvar.cte.schema_300.evgtv.TEvento.class, value);
        }

    }

    /**
     * The interface Ret inutilizacao.
     */
    public interface retInutilizacao {
        /**
         * Create t ret inut c te jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "TRetInutCTe", scope = TRetInutCTe.class)
        static JAXBElement<TRetInutCTe> createTRetInutCTe(TRetInutCTe value) {
            return new JAXBElement<>(_RetInutilizacao_QNAME, TRetInutCTe.class, TRetInutCTe.class, value);
        }
    }

    /**
     * The interface Cte.
     */
    public interface cte {
        /**
         * Create tc te jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "cte", scope = TCTe.class)
        static JAXBElement<TCTe> createTCTe(TCTe value) {
            return new JAXBElement<>(_CTe_QNAME, TCTe.class, TCTe.class, value);
        }

        /**
         * Create tc te jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        static JAXBElement<com.benparvar.cte.schema_300.envicte.TCTe> createTCTe(com.benparvar.cte.schema_300.envicte.TCTe value) {
            return new JAXBElement<>(_CTe_QNAME, com.benparvar.cte.schema_300.envicte.TCTe.class, com.benparvar.cte.schema_300.envicte.TCTe.class, value);
        }
    }

    /**
     * The interface Cte proc.
     */
    public interface cteProc {
        /**
         * Create cte proc jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "CteProc", scope = TRetInutCTe.class)
        static JAXBElement<CteProc> createCteProc(CteProc value) {
            return new JAXBElement<>(_CteProc_QNAME, CteProc.class, CteProc.class, value);
        }
    }

    /**
     * The interface Proc inut.
     */
    public interface procInut {
        /**
         * Create proc inut jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "procInutCTe", scope = TProcInutCTe.class)
        static JAXBElement<TProcInutCTe> createProcInut(TProcInutCTe value) {
            return new JAXBElement<>(_procInut_QNAME, TProcInutCTe.class, TProcInutCTe.class, value);
        }
    }

    /**
     * The interface Cte os proc.
     */
    public interface cteOSProc {
        /**
         * Create cte os proc jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "cteOSProc", scope = CteOSProc.class)
        static JAXBElement<CteOSProc> createCteOSProc(CteOSProc value) {
            return new JAXBElement<>(_CteProc_QNAME, CteOSProc.class, CteOSProc.class, value);
        }
    }

    /**
     * The interface Rodo.
     */
    public interface rodo {
        /**
         * Create rodo jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "Rodo", scope = Rodo.class)
        static JAXBElement<Rodo> createRodo(Rodo value) {
            return new JAXBElement<>(_Rodo_QNAME, Rodo.class, Rodo.class, value);
        }
    }

    /**
     * The interface Prot c te.
     */
    public interface protCTe {
        /**
         * Create prot cte jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "protCTe", scope = TProtCTe.class)
        static JAXBElement<TProtCTe> createProtCte(TProtCTe value) {
            return new JAXBElement<>(_CteProc_QNAME, TProtCTe.class, TProtCTe.class, value);
        }

        /**
         * Create prot cte jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "protCTe", scope = TProtCTeOS.class)
        static JAXBElement<TProtCTeOS> createProtCte(TProtCTeOS value) {
            return new JAXBElement<>(_CteProc_QNAME, TProtCTeOS.class, TProtCTeOS.class, value);
        }

        /**
         * Create prot cte jaxb element.
         *
         * @param value the value
         * @return the jaxb element
         */
        @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/cte", name = "protCTe", scope = TProtCTe.class)
        static JAXBElement<com.benparvar.cte.schema_300.retconsrecicte.TProtCTe> createProtCte(com.benparvar.cte.schema_300.retconsrecicte.TProtCTe value) {
            return new JAXBElement<>(_CteProc_QNAME, com.benparvar.cte.schema_300.retconsrecicte.TProtCTe.class, com.benparvar.cte.schema_300.retconsrecicte.TProtCTe.class, value);
        }
    }

}
