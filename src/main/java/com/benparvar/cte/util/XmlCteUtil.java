/**
 *
 */
package com.benparvar.cte.util;

import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.consrecicte.TConsReciCTe;
import com.benparvar.cte.schema_300.conssitcte.TConsSitCTe;
import com.benparvar.cte.schema_300.consstatservcte.ObjectFactory;
import com.benparvar.cte.schema_300.consstatservcte.TConsStatServ;
import com.benparvar.cte.schema_300.ctemodalrodoviario.Rodo;
import com.benparvar.cte.schema_300.cteos.TCTeOS;
import com.benparvar.cte.schema_300.distdfeint.DistDFeInt;
import com.benparvar.cte.schema_300.envicte.TEnviCTe;
import com.benparvar.cte.schema_300.evcanccte.TEvento;
import com.benparvar.cte.schema_300.evcanccte.TProcEvento;
import com.benparvar.cte.schema_300.evcanccte.TRetEvento;
import com.benparvar.cte.schema_300.inutcte.TInutCTe;
import com.benparvar.cte.schema_300.inutcte.TProcInutCTe;
import com.benparvar.cte.schema_300.inutcte.TRetInutCTe;
import com.benparvar.cte.schema_300.proccte.CteProc;
import com.benparvar.cte.schema_300.proccte.TCTe;
import com.benparvar.cte.schema_300.proccteos.CteOSProc;
import com.benparvar.cte.schema_300.retconsrecicte.TProtCTe;
import com.benparvar.cte.schema_300.retconsrecicte.TRetConsReciCTe;
import com.benparvar.cte.schema_300.retconssitcte.TRetConsSitCTe;
import com.benparvar.cte.schema_300.retcteos.TProtCTeOS;
import com.benparvar.cte.schema_300.retenvicte.TRetEnviCTe;

import javax.xml.bind.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringJoiner;
import java.util.zip.GZIPInputStream;

/**
 * The type Xml cte util.
 */
public class XmlCteUtil {

    private static final String STATUS_SERVICO = "TConsStatServ";
    private static final String CTE = "TCTe";
    private static final String CTE_ENV = "com.benparvar.jaxws.cte.schema_300.enviCTe.TCTe";
    private static final String CTE_PROC = "com.benparvar.jaxws.cte.schema_300.procCTe.TCTe";
    private static final String ENVIO_CTE = "TEnviCTe";
    private static final String ENVIO_CTE_OS = "TCTeOS";
    private static final String CONSULTA_RECIBO = "TConsReciCTe";
    private static final String INUTILIZACAO = "TInutCTe";
    private static final String CONSULTA_PROTOCOLO = "TConsSitCTe";
    private static final String DISTRIBUICAO_DFE = "DistDFeInt";
    private static final String EVENTO = "TEvento";

    private static final String MODAL_RODOVIARIO = "Rodo";
    private static final String MODAL_RODOVIARIO_V3 = "com.benparvar.jaxws.cte.schema_300.cteModalRodoviario.Rodo";

    private static final String PROT_CTE = "TProtCTe";
    private static final String PROT_CTEOS = "TProtCTeOS";
    private static final String PROT_CTE_CONSULTA_RECIBO = "com.benparvar.jaxws.cte.schema_300.retConsReciCTe.TProtCTe";
    private static final String PROT_CTE_PROC = "com.benparvar.jaxws.cte.schema_300.procCTe.TProtCTe";
    private static final String PROT_CTE_CONSULTA_SITUACAO = "com.benparvar.jaxws.cte.schema_300.retConsSitCTe.TProtCTe";
    private static final String PROT_CTEOS_PROC = "com.benparvar.jaxws.cte.schema_300.retCTeOS.TProtCTeOS";

    private static final String CANCELAR = "com.benparvar.jaxws.cte.schema_300.evCancCTe.TEvento";
    private static final String EPEC = "com.benparvar.jaxws.cte.schema_300.evEPECCTe.TEvento";
    private static final String MULTIMODAL = "com.benparvar.jaxws.cte.schema_300.evRegMultimodal.TEvento";
    private static final String CCE = "com.benparvar.jaxws.cte.schema_300.evCCeCTe.TEvento";
    private static final String PRESTDESACORDO = "com.benparvar.jaxws.cte.schema_300.evPrestDesacordo.TEvento";
    private static final String GVT = "com.benparvar.jaxws.cte.schema_300.evGTV.TEvento";

    private static final String RET_EVENTO = "TRetEvento";
    private static final String RET_INUTILIZACAO = "TRetInutCTe";
    private static final String RET_CONSULTA = "TRetConsSitCTe";
    private static final String RET_ENVICTE = "TRetEnviCTe";
    private static final String RET_CONSULTA_RECIBO = "TRetConsReciCTe";
    private static final String RET_CANCELAR = "com.benparvar.jaxws.cte.schema_300.evCancCTe.TRetEvento";
    private static final String RET_EPEC = "com.benparvar.jaxws.cte.schema_300.evEPECCTe.TRetEvento";
    private static final String RET_MULTIMODAL = "com.benparvar.jaxws.cte.schema_300.evRegMultimodal.TRetEvento";
    private static final String RET_CCE = "com.benparvar.jaxws.cte.schema_300.evCCeCTe.TRetEvento";
    private static final String RET_PRESTDESACORDO = "com.benparvar.jaxws.cte.schema_300.evPrestDesacordo.TRetEvento";
    private static final String RET_GVT = "com.benparvar.jaxws.cte.schema_300.evGTV.TRetEvento";

    private static final String PROC_CANCELAR = "com.benparvar.jaxws.cte.schema_300.evCancCTe.TProcEvento";
    private static final String PROC_EPEC = "com.benparvar.jaxws.cte.schema_300.evEPECCTe.TProcEvento";
    private static final String PROC_MULTIMODAL = "com.benparvar.jaxws.cte.schema_300.evRegMultimodal.TProcEvento";
    private static final String PROC_CCE = "com.benparvar.jaxws.cte.schema_300.evCCeCTe.TProcEvento";
    private static final String PROC_PRESTDESACORDO = "com.benparvar.jaxws.cte.schema_300.evPrestDesacordo.TProcEvento";
    private static final String PROC_GVT = "com.benparvar.jaxws.cte.schema_300.evGTV.TProcEvento";
    private static final String PROC_EVENTO = "TProcEvento";
    private static final String PROC_CTE = "CteProc";
    private static final String PROC_CTEOS = "CteOSProc";
    private static final String PROC_INUTILIZAR = "TProcInutCTe";

    /**
     * Xml to object t.
     *
     * @param <T>    the type parameter
     * @param xml    the xml
     * @param classe the classe
     * @return the t
     * @throws JAXBException the jaxb exception
     */
    public static <T> T xmlToObject(String xml, Class<T> classe) throws JAXBException {

        JAXBContext context = JAXBContext.newInstance(classe);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        return unmarshaller.unmarshal(new StreamSource(new StringReader(xml)), classe).getValue();
    }

    /**
     * Object to xml string.
     *
     * @param <T> the type parameter
     * @param obj the obj
     * @return the string
     * @throws JAXBException the jaxb exception
     * @throws CteException  the cte exception
     */
    public static <T> String objectToXml(Object obj) throws JAXBException, CteException {

        JAXBContext context;
        JAXBElement<?> element;

        switch (obj.getClass().getSimpleName()) {

            case STATUS_SERVICO:
                context = JAXBContext.newInstance(TConsStatServ.class);
                element = new ObjectFactory()
                        .createConsStatServCte((TConsStatServ) obj);
                break;

            case ENVIO_CTE:
                context = JAXBContext.newInstance(TEnviCTe.class);
                element = new com.benparvar.cte.schema_300.envicte.ObjectFactory()
                        .createEnviCTe((TEnviCTe) obj);
                break;

            case ENVIO_CTE_OS:
                context = JAXBContext.newInstance(TCTeOS.class);
                element = new com.benparvar.cte.schema_300.cteos.ObjectFactory()
                        .createCTeOS((TCTeOS) obj);
                break;
            case CTE:

                switch (obj.getClass().getName()) {

                    case CTE_PROC:
                        context = JAXBContext.newInstance(TCTe.class);
                        element = XsdCteUtil.cte
                                .createTCTe((TCTe) obj);
                        break;

                    case CTE_ENV:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.envicte.TCTe.class);
                        element = XsdCteUtil.cte
                                .createTCTe((com.benparvar.cte.schema_300.envicte.TCTe) obj);
                        break;
                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case CONSULTA_RECIBO:
                context = JAXBContext.newInstance(TConsReciCTe.class);
                element = new com.benparvar.cte.schema_300.consrecicte.ObjectFactory()
                        .createConsReciCTe((TConsReciCTe) obj);
                break;

            case INUTILIZACAO:
                context = JAXBContext.newInstance(TInutCTe.class);
                element = new com.benparvar.cte.schema_300.inutcte.ObjectFactory()
                        .createInutCTe((TInutCTe) obj);
                break;

            case CONSULTA_PROTOCOLO:
                context = JAXBContext.newInstance(TConsSitCTe.class);
                element = new com.benparvar.cte.schema_300.conssitcte.ObjectFactory()
                        .createConsSitCTe((TConsSitCTe) obj);

                break;

            case DISTRIBUICAO_DFE:
                context = JAXBContext.newInstance(DistDFeInt.class);
                element = new com.benparvar.cte.schema_300.distdfeint.ObjectFactory()
                        .createDistDFeInt((DistDFeInt) obj);
                break;

            case RET_ENVICTE:
                context = JAXBContext.newInstance(TRetEnviCTe.class);
                element = new com.benparvar.cte.schema_300.retenvicte.ObjectFactory()
                        .createRetEnviCte((TRetEnviCTe) obj);
                break;

            case RET_CONSULTA_RECIBO:
                context = JAXBContext.newInstance(TRetConsReciCTe.class);
                element = new com.benparvar.cte.schema_300.retconsrecicte.ObjectFactory()
                        .createRetConsReciCTe((TRetConsReciCTe) obj);
                break;

            case RET_INUTILIZACAO:
                context = JAXBContext.newInstance(TRetInutCTe.class);
                element = XsdCteUtil.retInutilizacao.createTRetInutCTe((TRetInutCTe) obj);
                break;

            case RET_CONSULTA:
                context = JAXBContext.newInstance(TRetConsSitCTe.class);
                element = new com.benparvar.cte.schema_300.retconssitcte.ObjectFactory()
                        .createRetConsSitCTe((TRetConsSitCTe) obj);
                break;

            case PROC_EVENTO:

                switch (obj.getClass().getName()) {

                    case PROC_CANCELAR:
                        context = JAXBContext.newInstance(TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((TProcEvento) obj);
                        break;

                    case PROC_EPEC:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evepeccte.TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((com.benparvar.cte.schema_300.evepeccte.TProcEvento) obj);
                        break;

                    case PROC_MULTIMODAL:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evepeccte.TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((com.benparvar.cte.schema_300.evregmultimodal.TProcEvento) obj);
                        break;

                    case PROC_CCE:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evccecte.TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((com.benparvar.cte.schema_300.evccecte.TProcEvento) obj);
                        break;

                    case PROC_PRESTDESACORDO:
                        context = JAXBContext
                                .newInstance(com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((com.benparvar.cte.schema_300.evprestdesacordo.TProcEvento) obj);
                        break;

                    case PROC_GVT:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evgtv.TProcEvento.class);
                        element = XsdCteUtil.procEvento
                                .createTProcEvento((com.benparvar.cte.schema_300.evgtv.TProcEvento) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case PROC_CTE:
                context = JAXBContext.newInstance(CteProc.class);
                element = XsdCteUtil.cteProc
                        .createCteProc((CteProc) obj);
                break;

            case PROC_INUTILIZAR:
                context = JAXBContext.newInstance(TProcInutCTe.class);
                element = XsdCteUtil.procInut
                        .createProcInut((TProcInutCTe) obj);
                break;

            case PROC_CTEOS:
                context = JAXBContext.newInstance(CteOSProc.class);
                element = XsdCteUtil.cteOSProc
                        .createCteOSProc((CteOSProc) obj);
                break;

            case EVENTO:

                switch (obj.getClass().getName()) {

                    case CANCELAR:
                        context = JAXBContext.newInstance(TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((TEvento) obj);
                        break;

                    case EPEC:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evepeccte.TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((com.benparvar.cte.schema_300.evepeccte.TEvento) obj);
                        break;

                    case MULTIMODAL:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evregmultimodal.TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((com.benparvar.cte.schema_300.evregmultimodal.TEvento) obj);
                        break;

                    case CCE:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evccecte.TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((com.benparvar.cte.schema_300.evccecte.TEvento) obj);
                        break;

                    case PRESTDESACORDO:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evprestdesacordo.TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((com.benparvar.cte.schema_300.evprestdesacordo.TEvento) obj);
                        break;

                    case GVT:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evgtv.TEvento.class);
                        element = XsdCteUtil.evento
                                .createTEvento((com.benparvar.cte.schema_300.evgtv.TEvento) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case RET_EVENTO:

                switch (obj.getClass().getName()) {

                    case RET_CANCELAR:
                        context = JAXBContext.newInstance(TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((TRetEvento) obj);
                        break;

                    case RET_EPEC:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evepeccte.TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((com.benparvar.cte.schema_300.evepeccte.TRetEvento) obj);
                        break;

                    case RET_MULTIMODAL:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evregmultimodal.TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((com.benparvar.cte.schema_300.evregmultimodal.TRetEvento) obj);
                        break;

                    case RET_CCE:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evccecte.TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((com.benparvar.cte.schema_300.evccecte.TRetEvento) obj);
                        break;

                    case RET_PRESTDESACORDO:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento) obj);
                        break;

                    case RET_GVT:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.evgtv.TRetEvento.class);
                        element = XsdCteUtil.retEvento.createTRetEvent((com.benparvar.cte.schema_300.evgtv.TRetEvento) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case PROT_CTE:

                switch (obj.getClass().getName()) {

                    case PROT_CTE_CONSULTA_RECIBO:
                        context = JAXBContext.newInstance(TProtCTe.class);
                        element = XsdCteUtil.protCTe
                                .createProtCte((TProtCTe) obj);
                        break;

                    case PROT_CTE_PROC:
                        context = JAXBContext.newInstance(com.benparvar.cte.schema_300.proccte.TProtCTe.class);
                        element = XsdCteUtil.protCTe
                                .createProtCte((com.benparvar.cte.schema_300.proccte.TProtCTe) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case PROT_CTEOS:

                switch (obj.getClass().getName()) {

                    case PROT_CTEOS_PROC:
                        context = JAXBContext.newInstance(TProtCTeOS.class);
                        element = XsdCteUtil.protCTe
                                .createProtCte((TProtCTeOS) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            case MODAL_RODOVIARIO:

                switch (obj.getClass().getName()) {

                    case MODAL_RODOVIARIO:
                        context = JAXBContext.newInstance(Rodo.class);
                        element = XsdCteUtil.rodo
                                .createRodo((Rodo) obj);
                        break;

                    default:
                        throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getName());
                }

                break;

            default:
                throw new CteException("Objeto não mapeado no XmlCteUtil:" + obj.getClass().getSimpleName());
        }

        Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty("jaxb.encoding", "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

        StringWriter sw = new StringWriter();
        marshaller.marshal(element, sw);

        return replacesCte("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + sw.toString());

    }

    /**
     * G zip to xml string.
     *
     * @param conteudo the conteudo
     * @return the string
     * @throws IOException the io exception
     */
    public static String gZipToXml(byte[] conteudo) throws IOException {
        if (conteudo == null || conteudo.length == 0) {
            return "";
        }
        GZIPInputStream gis;
        gis = new GZIPInputStream(new ByteArrayInputStream(conteudo));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, StandardCharsets.UTF_8));
        StringBuilder outStr = new StringBuilder();
        String line;
        while ((line = bf.readLine()) != null) {
            outStr.append(line);
        }

        return outStr.toString();
    }

    /**
     * Cria cte proc string.
     *
     * @param enviCte the envi cte
     * @param retorno the retorno
     * @return the string
     * @throws JAXBException the jaxb exception
     * @throws CteException  the cte exception
     */
    public static String criaCteProc(TEnviCTe enviCte, Object retorno)
            throws JAXBException, CteException {

        CteProc cteProc = new CteProc();
        cteProc.setVersao("3.00");
        cteProc.setCTe(xmlToObject(objectToXml(enviCte.getCTe().get(0)), TCTe.class));
        cteProc.setProtCTe(
                xmlToObject(objectToXml(retorno), com.benparvar.cte.schema_300.proccte.TProtCTe.class));

        return XmlCteUtil.objectToXml(cteProc);
    }

    /**
     * Cria cte os proc string.
     *
     * @param enviCte the envi cte
     * @param retorno the retorno
     * @return the string
     * @throws JAXBException the jaxb exception
     * @throws CteException  the cte exception
     */
    public static String criaCteOSProc(TCTeOS enviCte, Object retorno)
            throws JAXBException, CteException {

        CteOSProc cteProc = new CteOSProc();
        cteProc.setVersao("3.00");
        cteProc.setCTeOS(xmlToObject(objectToXml(enviCte), com.benparvar.cte.schema_300.proccteos.TCTeOS.class));
        cteProc.setProtCTe(
                xmlToObject(objectToXml(retorno), com.benparvar.cte.schema_300.proccteos.TProtCTeOS.class));

        return XmlCteUtil.objectToXml(cteProc);
    }

    /**
     * Le xml string.
     *
     * @param arquivo the arquivo
     * @return the string
     * @throws IOException the io exception
     */
    public static String leXml(String arquivo) throws IOException {

        ObjetoCTeUtil.verifica(arquivo).orElseThrow(() -> new IllegalArgumentException("Arquivo xml não pode ser nulo/vazio."));
        if (!Files.exists(Paths.get(arquivo))) {
            throw new FileNotFoundException("Arquivo " + arquivo + " não encontrado.");
        }
        List<String> list = Files.readAllLines(Paths.get(arquivo));
        StringJoiner joiner = new StringJoiner("\n");
        list.forEach(joiner::add);

        return joiner.toString();
    }

    private static String replacesCte(String xml) {
        xml = xml.replaceAll("ns2:", "");
        xml = xml.replaceAll("ns3:", "");
        xml = xml.replaceAll("&lt;", "<");
        xml = xml.replaceAll("&gt;", ">");
        xml = xml.replaceAll("<Signature>", "<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">");
        xml = xml.replaceAll(" xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"", "");
        xml = xml.replaceAll(" xmlns:ns3=\"http://www.portalfiscal.inf.br/cte\"", "");
        xml = xml.replaceAll(" xmlns:ns2=\"http://www.portalfiscal.inf.br/cte\"", "");
        xml = xml.replaceAll(" xmlns=\"\"", "");
        return xml;
    }

    /**
     * Data cte string.
     *
     * @param dataASerFormatada the data a ser formatada
     * @return the string
     */
    public static String dataCte(LocalDateTime dataASerFormatada) {
        return dataCte(dataASerFormatada, ZoneId.systemDefault());
    }

    /**
     * Data cte string.
     *
     * @param dataASerFormatada the data a ser formatada
     * @param zoneId            the zone id
     * @return the string
     */
    public static String dataCte(LocalDateTime dataASerFormatada, ZoneId zoneId) {
        try {
            GregorianCalendar calendar = GregorianCalendar.from(dataASerFormatada.atZone(ObjetoCTeUtil.verifica(zoneId).orElse(ZoneId.of("Brazil/East"))));

            XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            xmlCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
            return xmlCalendar.toString();

        } catch (DatatypeConfigurationException e) {
            LoggerUtil.log(XmlCteUtil.class, e.getMessage());
        }
        return null;
    }

}
