/**
 *
 */
package com.benparvar.cte.util;

/**
 * The interface Constantes cte.
 */
public interface ConstantesCte {

    /**
     * The interface Versao.
     */
    interface VERSAO {
        /**
         * The constant CTE.
         */
        String CTE = "3.00";
        /**
         * The constant DISTRIBUICAO.
         */
        String DISTRIBUICAO = "1.00";
    }
}
