package com.benparvar.cte.util;

import br.com.swconsultoria.certificado.CertificadoService;
import br.com.swconsultoria.certificado.exception.CertificadoException;
import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.exception.CteException;

/**
 * The type Configuracoes util.
 */
public class ConfiguracoesUtil {

    /**
     * Inicia configuracoes configuracoes cte.
     *
     * @param configuracoesCte the configuracoes cte
     * @return the configuracoes cte
     * @throws CteException the cte exception
     */
    public static ConfiguracoesCte iniciaConfiguracoes(ConfiguracoesCte configuracoesCte) throws CteException {

        return iniciaConfiguracoes(configuracoesCte, null);
    }

    /**
     * Inicia configuracoes configuracoes cte.
     *
     * @param configuracoesCte the configuracoes cte
     * @param cpfCnpj          the cpf cnpj
     * @return the configuracoes cte
     * @throws CteException the cte exception
     */
    public static ConfiguracoesCte iniciaConfiguracoes(ConfiguracoesCte configuracoesCte, String cpfCnpj) throws CteException {

        ObjetoCTeUtil.verifica(configuracoesCte).orElseThrow(() -> new CteException("Configurações não foram criadas"));

        try {
            if (!configuracoesCte.getCertificado().isValido()) {
                throw new CertificadoException("Certificado vencido ou inválido.");
            }

            if (configuracoesCte.isValidacaoDocumento() && cpfCnpj != null && !configuracoesCte.getCertificado().getCnpjCpf().substring(0, 8).equals(cpfCnpj.substring(0, 8))) {
                throw new CertificadoException("Documento do Certificado(" + configuracoesCte.getCertificado().getCnpjCpf() + ") não equivale ao Documento do Emissor(" + cpfCnpj + ")");
            }
            CertificadoService.inicializaCertificado(configuracoesCte.getCertificado(), ConfiguracoesUtil.class.getResourceAsStream("/Cacert"));
        } catch (CertificadoException e) {
            throw new CteException(e.getMessage());
        }

        return configuracoesCte;
    }

}