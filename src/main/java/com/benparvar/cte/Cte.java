package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ConsultaDFeEnum;
import com.benparvar.cte.dom.enuns.PessoaEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.cteos.TCTeOS;
import com.benparvar.cte.schema_300.envicte.TEnviCTe;
import com.benparvar.cte.schema_300.evepeccte.TEvento;
import com.benparvar.cte.schema_300.evepeccte.TRetEvento;
import com.benparvar.cte.schema_300.inutcte.TInutCTe;
import com.benparvar.cte.schema_300.inutcte.TRetInutCTe;
import com.benparvar.cte.schema_300.retconsrecicte.TRetConsReciCTe;
import com.benparvar.cte.schema_300.retconssitcte.TRetConsSitCTe;
import com.benparvar.cte.schema_300.retconsstatservcte.TRetConsStatServ;
import com.benparvar.cte.schema_300.retcteos.TRetCTeOS;
import com.benparvar.cte.schema_300.retdistdfeint.RetDistDFeInt;
import com.benparvar.cte.schema_300.retenvicte.TRetEnviCTe;
import com.benparvar.cte.util.ConfiguracoesUtil;

/**
 * The type Cte.
 */
public class Cte {

    private Cte() {
    }

    /**
     * Status servico t ret cons stat serv.
     *
     * @param configuracoesCte the configuracoes cte
     * @return the t ret cons stat serv
     * @throws CteException the cte exception
     */
    public static TRetConsStatServ statusServico(ConfiguracoesCte configuracoesCte) throws CteException {

        return Status.statusServico(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte));

    }

    /**
     * Consulta xml t ret cons sit c te.
     *
     * @param configuracoesCte the configuracoes cte
     * @param chave            the chave
     * @return the t ret cons sit c te
     * @throws CteException the cte exception
     */
    public static TRetConsSitCTe consultaXml(ConfiguracoesCte configuracoesCte, String chave)
            throws CteException {

        return ConsultaXml.consultaXml(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), chave);

    }

    /**
     * Inutilizacao t ret inut c te.
     *
     * @param configuracoesCte the configuracoes cte
     * @param inutCTe          the inut c te
     * @param valida           the valida
     * @return the t ret inut c te
     * @throws CteException the cte exception
     */
    public static TRetInutCTe inutilizacao(ConfiguracoesCte configuracoesCte,
                                           TInutCTe inutCTe, boolean valida) throws CteException {

        return Inutilizar.inutiliza(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), inutCTe, valida);

    }

    /**
     * Distribuicao dfe ret dist d fe int.
     *
     * @param configuracoesCte the configuracoes cte
     * @param tipoPessoa       the tipo pessoa
     * @param cpfCnpj          the cpf cnpj
     * @param tipoConsulta     the tipo consulta
     * @param nsu              the nsu
     * @return the ret dist d fe int
     * @throws CteException the cte exception
     */
    public static RetDistDFeInt distribuicaoDfe(ConfiguracoesCte configuracoesCte, PessoaEnum tipoPessoa, String cpfCnpj,
                                                ConsultaDFeEnum tipoConsulta, String nsu) throws CteException {

        return DistribuicaoDFe.consultaCte(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), tipoPessoa, cpfCnpj, tipoConsulta, nsu);

    }

    /**
     * Monta cte t envi c te.
     *
     * @param configuracoesCte the configuracoes cte
     * @param enviCTe          the envi c te
     * @param valida           the valida
     * @return the t envi c te
     * @throws CteException the cte exception
     */
    public static TEnviCTe montaCte(ConfiguracoesCte configuracoesCte, TEnviCTe enviCTe, boolean valida) throws CteException {

        return EnvioCte.montaCte(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), enviCTe, valida);

    }

    /**
     * Monta cte os tc te os.
     *
     * @param configuracoesCte the configuracoes cte
     * @param enviCTe          the envi c te
     * @param valida           the valida
     * @return the tc te os
     * @throws CteException the cte exception
     */
    public static TCTeOS montaCteOS(ConfiguracoesCte configuracoesCte,
                                    TCTeOS enviCTe, boolean valida) throws CteException {

        return EnvioCteOS.montaCteOS(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), enviCTe, valida);

    }

    /**
     * Enviar cte t ret envi c te.
     *
     * @param configuracoesCte the configuracoes cte
     * @param enviCTe          the envi c te
     * @return the t ret envi c te
     * @throws CteException the cte exception
     */
    public static TRetEnviCTe enviarCte(ConfiguracoesCte configuracoesCte,
                                        TEnviCTe enviCTe) throws CteException {

        return EnvioCte.enviaCte(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), enviCTe);

    }

    /**
     * Enviar cte os t ret c te os.
     *
     * @param configuracoesCte the configuracoes cte
     * @param enviCTe          the envi c te
     * @return the t ret c te os
     * @throws CteException the cte exception
     */
    public static TRetCTeOS enviarCteOS(ConfiguracoesCte configuracoesCte, TCTeOS enviCTe) throws CteException {

        return EnvioCteOS.enviaCteOS(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), enviCTe);

    }

    /**
     * Cancelar cte com . benparvar . cte . schema 300 . evcanccte . t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the com . benparvar . cte . schema 300 . evcanccte . t ret evento
     * @throws CteException the cte exception
     */
    public static com.benparvar.cte.schema_300.evcanccte.TRetEvento cancelarCte(ConfiguracoesCte configuracoesCte,
                                                                                com.benparvar.cte.schema_300.evcanccte.TEvento evento, boolean valida) throws CteException {

        return Cancelar.eventoCancelamento(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Epec cte t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the t ret evento
     * @throws CteException the cte exception
     */
    public static TRetEvento epecCte(ConfiguracoesCte configuracoesCte,
                                     TEvento evento, boolean valida) throws CteException {

        return Epec.eventoEpec(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Multimodal cte com . benparvar . cte . schema 300 . evregmultimodal . t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the com . benparvar . cte . schema 300 . evregmultimodal . t ret evento
     * @throws CteException the cte exception
     */
    public static com.benparvar.cte.schema_300.evregmultimodal.TRetEvento multimodalCte(ConfiguracoesCte configuracoesCte,
                                                                                        com.benparvar.cte.schema_300.evregmultimodal.TEvento evento, boolean valida) throws CteException {

        return Multimodal.eventoMulti(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Cce cte com . benparvar . cte . schema 300 . evccecte . t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the com . benparvar . cte . schema 300 . evccecte . t ret evento
     * @throws CteException the cte exception
     */
    public static com.benparvar.cte.schema_300.evccecte.TRetEvento cceCte(ConfiguracoesCte configuracoesCte,
                                                                          com.benparvar.cte.schema_300.evccecte.TEvento evento, boolean valida) throws CteException {

        return CartaCorrecao.eventoCCe(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Prestacao desacordo cte com . benparvar . cte . schema 300 . evprestdesacordo . t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the com . benparvar . cte . schema 300 . evprestdesacordo . t ret evento
     * @throws CteException the cte exception
     */
    public static com.benparvar.cte.schema_300.evprestdesacordo.TRetEvento prestacaoDesacordoCte(ConfiguracoesCte configuracoesCte,
                                                                                                 com.benparvar.cte.schema_300.evprestdesacordo.TEvento evento, boolean valida) throws CteException {

        return PrestacaoDesacordo.eventoPrestacaoDesacordo(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Gvt cte com . benparvar . cte . schema 300 . evgtv . t ret evento.
     *
     * @param configuracoesCte the configuracoes cte
     * @param evento           the evento
     * @param valida           the valida
     * @return the com . benparvar . cte . schema 300 . evgtv . t ret evento
     * @throws CteException the cte exception
     */
    public static com.benparvar.cte.schema_300.evgtv.TRetEvento gvtCte(ConfiguracoesCte configuracoesCte,
                                                                       com.benparvar.cte.schema_300.evgtv.TEvento evento, boolean valida) throws CteException {

        return Gvt.eventoGvt(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), evento, valida);

    }

    /**
     * Consulta recibo t ret cons reci c te.
     *
     * @param configuracoesCte the configuracoes cte
     * @param recibo           the recibo
     * @return the t ret cons reci c te
     * @throws CteException the cte exception
     */
    public static TRetConsReciCTe consultaRecibo(ConfiguracoesCte configuracoesCte, String recibo)
            throws CteException {

        return ConsultaRecibo.reciboCte(ConfiguracoesUtil.iniciaConfiguracoes(configuracoesCte), recibo);

    }
}
