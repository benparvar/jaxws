package com.benparvar.cte.dom;

import org.apache.axis2.transport.http.HttpTransportProperties;

/**
 * The type Proxy.
 */
public class Proxy {
    private HttpTransportProperties.ProxyProperties proxyProperties;

    /**
     * Instantiates a new Proxy.
     *
     * @param ip      the ip
     * @param porta   the porta
     * @param usuario the usuario
     * @param senha   the senha
     */
    public Proxy(String ip, int porta, String usuario, String senha) {

        proxyProperties = new HttpTransportProperties.ProxyProperties();

        proxyProperties.setProxyName(ip);

        proxyProperties.setProxyPort(porta);

        proxyProperties.setUserName(usuario);

        proxyProperties.setPassWord(senha);
    }

    /**
     * Gets proxy host name.
     *
     * @return the proxy host name
     */
    public String getProxyHostName() {
        return proxyProperties.getProxyHostName();
    }

    /**
     * Gets proxy port.
     *
     * @return the proxy port
     */
    public String getProxyPort() {
        return String.valueOf(proxyProperties.getProxyPort());
    }

    /**
     * Gets proxy user name.
     *
     * @return the proxy user name
     */
    public String getProxyUserName() {
        return proxyProperties.getUserName();
    }

    /**
     * Gets proxy pass word.
     *
     * @return the proxy pass word
     */
    public String getProxyPassWord() {
        return proxyProperties.getPassWord();
    }
}

