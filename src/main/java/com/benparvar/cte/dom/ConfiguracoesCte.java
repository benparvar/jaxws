package com.benparvar.cte.dom;

import br.com.swconsultoria.certificado.Certificado;
import br.com.swconsultoria.certificado.exception.CertificadoException;
import com.benparvar.cte.dom.enuns.AmbienteEnum;
import com.benparvar.cte.dom.enuns.EstadosEnum;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Configuracoes cte.
 */
public class ConfiguracoesCte {

    private EstadosEnum estado;
    private AmbienteEnum ambiente;
    private Certificado certificado;
    private String pastaSchemas;
    private Proxy proxy;
    private Integer timeout;
    private boolean contigenciaSCAN;
    private boolean validacaoDocumento = true;
    private String arquivoWebService;

    /**
     * Criar configuracoes configuracoes cte.
     *
     * @param estado       the estado
     * @param ambiente     the ambiente
     * @param certificado  the certificado
     * @param pastaSchemas the pasta schemas
     * @return the configuracoes cte
     * @throws CertificadoException the certificado exception
     */
    public static ConfiguracoesCte criarConfiguracoes(EstadosEnum estado, AmbienteEnum ambiente, Certificado certificado,
                                                      String pastaSchemas) throws CertificadoException {

        ConfiguracoesCte configuracoesCte = new ConfiguracoesCte();
        configuracoesCte.setEstado(estado);
        configuracoesCte.setAmbiente(ambiente);
        configuracoesCte.setCertificado(certificado);
        configuracoesCte.setPastaSchemas(pastaSchemas);

        try {
            //Setando Encoding.
            System.setProperty("file.encoding", "UTF-8");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new CertificadoException("Erro ao setar Encoding.");
        }

        if (Logger.getLogger("").isLoggable(Level.SEVERE)) {
            System.err.println();
            System.err.println("#########################################################");
            System.err.println("         Api Cte - Versão 0.0.1 (D)       ");
            if (Logger.getLogger("").isLoggable(Level.WARNING)) {
                System.err.println("  ");
            }
            System.err.println(" Tipo Certificado: " + certificado.getTipoCertificado().toString());
            System.err.println(" Alias Certificado: " + certificado.getNome().toUpperCase());
            System.err.println(" Vencimento Certificado: " + certificado.getVencimento());
            System.err.println(" Cnpj/Cpf Certificado: " + certificado.getCnpjCpf());
            System.err.println(" Ambiente: " + (ambiente.equals(AmbienteEnum.PRODUCAO) ? "Produção" : "Homologação") + " - Estado: "
                    + estado.getNome());
            System.err.println("#########################################################");
            System.err.println();
        }
        if (!certificado.isValido()) {
            throw new CertificadoException("Certificado Vencido!");
        }
        return configuracoesCte;
    }

    /**
     * Gets pasta schemas.
     *
     * @return the pasta schemas
     */
    public String getPastaSchemas() {
        return pastaSchemas;
    }

    private void setPastaSchemas(String pastaSchemas) {
        this.pastaSchemas = pastaSchemas;
    }

    /**
     * Gets ambiente.
     *
     * @return the ambiente
     */
    public AmbienteEnum getAmbiente() {
        return ambiente;
    }

    /**
     * Sets ambiente.
     *
     * @param ambiente the ambiente
     */
    public void setAmbiente(AmbienteEnum ambiente) {
        this.ambiente = ambiente;
    }

    /**
     * Gets certificado.
     *
     * @return the certificado
     */
    public Certificado getCertificado() {
        return certificado;
    }

    private void setCertificado(Certificado certificado) {
        this.certificado = certificado;
    }

    /**
     * Is contigencia scan boolean.
     *
     * @return the boolean
     */
    public boolean isContigenciaSCAN() {
        return contigenciaSCAN;
    }

    /**
     * Sets contigencia scan.
     *
     * @param contigenciaSCAN the contigencia scan
     */
    public void setContigenciaSCAN(boolean contigenciaSCAN) {
        this.contigenciaSCAN = contigenciaSCAN;
    }

    /**
     * Gets estado.
     *
     * @return the estado
     */
    public EstadosEnum getEstado() {
        return estado;
    }

    /**
     * Sets estado.
     *
     * @param estado the estado
     */
    public void setEstado(EstadosEnum estado) {
        this.estado = estado;
    }

    /**
     * Gets proxy.
     *
     * @return the proxy
     */
    public Proxy getProxy() {
        return proxy;
    }

    /**
     * Sets proxy.
     *
     * @param proxy the proxy
     */
    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    /**
     * Gets timeout.
     *
     * @return the timeout
     */
    public Integer getTimeout() {
        return timeout;
    }

    /**
     * Sets timeout.
     *
     * @param timeout the timeout
     */
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    /**
     * Is validacao documento boolean.
     *
     * @return the boolean
     */
    public boolean isValidacaoDocumento() {
        return validacaoDocumento;
    }

    /**
     * Sets validacao documento.
     *
     * @param validacaoDocumento the validacao documento
     */
    public void setValidacaoDocumento(boolean validacaoDocumento) {
        this.validacaoDocumento = validacaoDocumento;
    }

    /**
     * Gets arquivo web service.
     *
     * @return the arquivo web service
     */
    public String getArquivoWebService() {
        return arquivoWebService;
    }

    /**
     * Sets arquivo web service.
     *
     * @param arquivoWebService the arquivo web service
     */
    public void setArquivoWebService(String arquivoWebService) {
        this.arquivoWebService = arquivoWebService;
    }
}
