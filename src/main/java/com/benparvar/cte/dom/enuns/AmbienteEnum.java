package com.benparvar.cte.dom.enuns;

/**
 * The enum Ambiente enum.
 */
public enum AmbienteEnum {

    /**
     * Homologacao ambiente enum.
     */
    HOMOLOGACAO("2"),
    /**
     * Producao ambiente enum.
     */
    PRODUCAO("1");

    private final String codigo;

    AmbienteEnum(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Gets by codigo.
     *
     * @param codigo the codigo
     * @return the by codigo
     */
    public static AmbienteEnum getByCodigo(String codigo) {
        for (AmbienteEnum e : values()) {
            if (e.codigo.equals(codigo)) return e;
        }
        throw new IllegalArgumentException();
    }

    /**
     * Gets codigo.
     *
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }
}
