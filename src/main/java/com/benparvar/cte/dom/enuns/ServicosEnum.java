package com.benparvar.cte.dom.enuns;

/**
 * The enum Servicos enum.
 */
public enum ServicosEnum {

    /**
     * Status servico servicos enum.
     */
    STATUS_SERVICO("ctestatusservico_3.00", "consStatServCTe_v3.00.xsd"),
    /**
     * Envio cte servicos enum.
     */
    ENVIO_CTE("CTeRecepcao_3.00", "enviCTe_v3.00.xsd"),
    /**
     * Envio cte os servicos enum.
     */
    ENVIO_CTE_OS("CTeRecepcaoOS_3.00", "CTeOS_v3.00.xsd"),
    /**
     * Consulta recibo servicos enum.
     */
    CONSULTA_RECIBO("CTeRetRecepcao_3.00", "consReciCTe_v3.00.xsd"),
    /**
     * Inutilizacao servicos enum.
     */
    INUTILIZACAO("CTeInutilizacao_3.00", "inutCTe_v3.00.xsd"),
    /**
     * Consulta xml servicos enum.
     */
    CONSULTA_XML("CTeConsultaProtocolo_3.00", "consSitCTe_v3.00.xsd"),
    /**
     * Distribuicao dfe servicos enum.
     */
    DISTRIBUICAO_DFE("CTeDistribuicaoDFe_1.00", "distDFeInt_v1.00.xsd"),
    /**
     * Cancelamento servicos enum.
     */
    CANCELAMENTO("recepcaoevento_3.00", "evCancCTe_v3.00.xsd"),
    /**
     * Epec servicos enum.
     */
    EPEC("recepcaoevento_3.00", "evEPECCTe_v3.00.xsd"),
    /**
     * Multimodal servicos enum.
     */
    MULTIMODAL("recepcaoevento_3.00", "evRegMultimodal_v3.00.xsd"),
    /**
     * Cce servicos enum.
     */
    CCE("recepcaoevento_3.00", "evCCeCTe_v3.00.xsd"),
    /**
     * Prestacao desacordo servicos enum.
     */
    PRESTACAO_DESACORDO("recepcaoevento_3.00", "evPrestDesacordo_v3.00.xsd"),
    /**
     * Gvt servicos enum.
     */
    GVT("recepcaoevento_3.00", "evGTV_v3.00.xsd"),
    /**
     * Qrcode servicos enum.
     */
    QRCODE("QR_Code", ""),
    /**
     * Evento servicos enum.
     */
    EVENTO("", "eventoCTe_v3.00.xsd");

    //    URL_QRCODE("url-qrcode",null),
//    URL_CONSULTANFCE("url-consultanfce", null);

    private final String servico;
    private final String xsd;

    ServicosEnum(String servico, String xsd) {
        this.servico = servico;
        this.xsd = xsd;
    }

    /**
     * Gets servico.
     *
     * @return the servico
     */
    public String getServico() {
        return servico;
    }

    /**
     * Gets xsd.
     *
     * @return the xsd
     */
    public String getXsd() {
        return xsd;
    }
}
