package com.benparvar.cte.dom.enuns;

/**
 * The enum Consulta d fe enum.
 */
public enum ConsultaDFeEnum {
    /**
     * Nsu consulta d fe enum.
     */
    NSU,
    /**
     * Nsu unico consulta d fe enum.
     */
    NSU_UNICO
}
