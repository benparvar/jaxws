package com.benparvar.cte.dom.enuns;

/**
 * The enum Pessoa enum.
 */
public enum PessoaEnum {
    /**
     * Fisica pessoa enum.
     */
    FISICA,
    /**
     * Juridica pessoa enum.
     */
    JURIDICA
}
