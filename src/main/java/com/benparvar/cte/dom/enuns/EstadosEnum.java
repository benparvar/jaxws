/**
 *
 */
package com.benparvar.cte.dom.enuns;

/**
 * The enum Estados enum.
 */
public enum EstadosEnum {

    /**
     * Ro estados enum.
     */
    RO("11", "Rondônia"),
    /**
     * Ac estados enum.
     */
    AC("12", "Acre"),
    /**
     * Am estados enum.
     */
    AM("13", "Amazonas"),
    /**
     * Rr estados enum.
     */
    RR("14", "Roraima"),
    /**
     * Pa estados enum.
     */
    PA("15", "Pará"),
    /**
     * Ap estados enum.
     */
    AP("16", "Amapá"),
    /**
     * To estados enum.
     */
    TO("17", "Tocantins"),
    /**
     * Ma estados enum.
     */
    MA("21", "Maranhão"),
    /**
     * Pi estados enum.
     */
    PI("22", "Piauí"),
    /**
     * Ce estados enum.
     */
    CE("23", "Ceará"),
    /**
     * The Rn.
     */
    RN("24", "Rio Grande do Norte"),
    /**
     * Pb estados enum.
     */
    PB("25", "Paraíba"),
    /**
     * Pe estados enum.
     */
    PE("26", "Pernambuco"),
    /**
     * Al estados enum.
     */
    AL("27", "Alagoas"),
    /**
     * Se estados enum.
     */
    SE("28", "Sergipe"),
    /**
     * Ba estados enum.
     */
    BA("29", "Bahia"),
    /**
     * The Mg.
     */
    MG("31", "Minas Gerais"),
    /**
     * The Es.
     */
    ES("32", "Espírito Santo"),
    /**
     * The Rj.
     */
    RJ("33", "Rio de Janeiro"),
    /**
     * The Sp.
     */
    SP("35", "São Paulo"),
    /**
     * Pr estados enum.
     */
    PR("41", "Paraná"),
    /**
     * The Sc.
     */
    SC("42", "Santa Catarina"),
    /**
     * The Rs.
     */
    RS("43", "Rio Grande do Sul"),
    /**
     * The Ms.
     */
    MS("50", "Mato Grosso do Sul"),
    /**
     * The Mt.
     */
    MT("51", "Mato Grosso"),
    /**
     * Go estados enum.
     */
    GO("52", "Goiás"),
    /**
     * The Df.
     */
    DF("53", "Distrito Federal");

    private final String codigoUF;
    private final String nome;

    EstadosEnum(String codigoUF, String nome) {
        this.codigoUF = codigoUF;
        this.nome = nome;
    }

    /**
     * Gets by codigo ibge.
     *
     * @param codigo the codigo
     * @return the by codigo ibge
     */
    public static EstadosEnum getByCodigoIbge(String codigo) {
        for (EstadosEnum e : values()) {
            if (e.codigoUF.equals(codigo)) return e;
        }
        throw new IllegalArgumentException();
    }

    /**
     * Gets nome.
     *
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Gets codigo uf.
     *
     * @return the codigo uf
     */
    public String getCodigoUF() {
        return codigoUF;
    }
}
