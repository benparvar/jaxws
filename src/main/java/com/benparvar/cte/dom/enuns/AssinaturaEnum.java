package com.benparvar.cte.dom.enuns;

/**
 * The enum Assinatura enum.
 */
public enum AssinaturaEnum {

    /**
     * Cte assinatura enum.
     */
    CTE("CTe", "infCte"),
    /**
     * Cte os assinatura enum.
     */
    CTE_OS("CTeOS", "infCte"),
    /**
     * Inutilizacao assinatura enum.
     */
    INUTILIZACAO("infInut", "infInut"),
    /**
     * Evento assinatura enum.
     */
    EVENTO("eventoCTe", "infEvento");

    private final String tipo;
    private final String tag;

    AssinaturaEnum(String tipo, String tag) {
        this.tipo = tipo;
        this.tag = tag;
    }

    /**
     * Gets tipo.
     *
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Gets tag.
     *
     * @return the tag
     */
    public String getTag() {
        return tag;
    }
}
