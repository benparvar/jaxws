package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ConsultaDFeEnum;
import com.benparvar.cte.dom.enuns.PessoaEnum;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.distdfeint.DistDFeInt;
import com.benparvar.cte.schema_300.retdistdfeint.RetDistDFeInt;
import com.benparvar.cte.util.*;
import com.benparvar.cte.wsdl.ctedistribuicaodfe.CTeDistribuicaoDFeStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axis2.transport.http.HTTPConstants;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

/**
 * The type Distribuicao d fe.
 */
public class DistribuicaoDFe {
    /**
     * Consulta cte ret dist d fe int.
     *
     * @param config       the config
     * @param tipoPessoa   the tipo pessoa
     * @param cpfCnpj      the cpf cnpj
     * @param tipoConsulta the tipo consulta
     * @param nsuChave     the nsu chave
     * @return the ret dist d fe int
     * @throws CteException the cte exception
     */
    static RetDistDFeInt consultaCte(ConfiguracoesCte config, PessoaEnum tipoPessoa, String cpfCnpj, ConsultaDFeEnum tipoConsulta,
                                     String nsuChave) throws CteException {

        try {

            DistDFeInt distDFeInt = new DistDFeInt();
            distDFeInt.setVersao(ConstantesCte.VERSAO.DISTRIBUICAO);
            distDFeInt.setTpAmb(config.getAmbiente().getCodigo());
            distDFeInt.setCUFAutor(config.getEstado().getCodigoUF());

            if (PessoaEnum.JURIDICA.equals(tipoPessoa)) {
                distDFeInt.setCNPJ(cpfCnpj);
            } else {
                distDFeInt.setCPF(cpfCnpj);
            }

            switch (tipoConsulta) {
                case NSU:
                    DistDFeInt.DistNSU distNSU = new DistDFeInt.DistNSU();
                    distNSU.setUltNSU(nsuChave);
                    distDFeInt.setDistNSU(distNSU);
                    break;
                case NSU_UNICO:
                    DistDFeInt.ConsNSU consNSU = new DistDFeInt.ConsNSU();
                    consNSU.setNSU(nsuChave);
                    distDFeInt.setConsNSU(consNSU);
                    break;
            }

            String xml = XmlCteUtil.objectToXml(distDFeInt);

            LoggerUtil.log(DistribuicaoDFe.class, "[XML-ENVIO]: " + xml);

            OMElement ome = AXIOMUtil.stringToOM(xml);

            CTeDistribuicaoDFeStub.CteDadosMsg_type0 dadosMsgType0 = new CTeDistribuicaoDFeStub.CteDadosMsg_type0();
            dadosMsgType0.setExtraElement(ome);

            CTeDistribuicaoDFeStub.CteDistDFeInteresse distDFeInteresse = new CTeDistribuicaoDFeStub.CteDistDFeInteresse();
            distDFeInteresse.setCteDadosMsg(dadosMsgType0);

            CTeDistribuicaoDFeStub stub = new CTeDistribuicaoDFeStub(
                    WebServiceCteUtil.getUrl(config, ServicosEnum.DISTRIBUICAO_DFE));

            // Timeout
            if (ObjetoCTeUtil.verifica(config.getTimeout()).isPresent()) {
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getTimeout());
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
                        config.getTimeout());
            }
            CTeDistribuicaoDFeStub.CteDistDFeInteresseResponse result = stub.cteDistDFeInteresse(distDFeInteresse);

            LoggerUtil.log(DistribuicaoDFe.class, "[XML-RETORNO]: " + result.getCteDistDFeInteresseResult().getExtraElement().toString());
            return XmlCteUtil.xmlToObject(result.getCteDistDFeInteresseResult().getExtraElement().toString(),
                    RetDistDFeInt.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }
    }

}
