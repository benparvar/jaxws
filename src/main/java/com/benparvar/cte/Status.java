package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.consstatservcte.TConsStatServ;
import com.benparvar.cte.schema_300.retconsstatservcte.TRetConsStatServ;
import com.benparvar.cte.util.ConstantesCte;
import com.benparvar.cte.util.LoggerUtil;
import com.benparvar.cte.util.WebServiceCteUtil;
import com.benparvar.cte.util.XmlCteUtil;
import com.benparvar.cte.wsdl.ctestatusservico.CteStatusServicoStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

/**
 * The type Status.
 */
class Status {

    /**
     * Status servico t ret cons stat serv.
     *
     * @param config the config
     * @return the t ret cons stat serv
     * @throws CteException the cte exception
     */
    static TRetConsStatServ statusServico(ConfiguracoesCte config) throws CteException {

        try {

            TConsStatServ consStatServ = new TConsStatServ();
            consStatServ.setTpAmb(config.getAmbiente().getCodigo());
            consStatServ.setVersao(ConstantesCte.VERSAO.CTE);
            consStatServ.setXServ("STATUS");
            String xml = XmlCteUtil.objectToXml(consStatServ);

            LoggerUtil.log(Status.class, "[XML-ENVIO]: " + xml);

            OMElement ome = AXIOMUtil.stringToOM(xml);

            CteStatusServicoStub.CteDadosMsg dadosMsg = new CteStatusServicoStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);

            CteStatusServicoStub stub = new CteStatusServicoStub(
                    WebServiceCteUtil.getUrl(config, ServicosEnum.STATUS_SERVICO));

            CteStatusServicoStub.CteCabecMsg cteCabecMsg = new CteStatusServicoStub.CteCabecMsg();
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteStatusServicoStub.CteCabecMsgE cteCabecMsgE = new CteStatusServicoStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            CteStatusServicoStub.CteStatusServicoCTResult result = stub.cteStatusServicoCT(dadosMsg, cteCabecMsgE);

            LoggerUtil.log(Status.class, "[XML-RETORNO]: " + result.getExtraElement().toString());
            return XmlCteUtil.xmlToObject(result.getExtraElement().toString(), TRetConsStatServ.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }
    }

}