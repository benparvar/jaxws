package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.consrecicte.TConsReciCTe;
import com.benparvar.cte.schema_300.retconsrecicte.TRetConsReciCTe;
import com.benparvar.cte.util.*;
import com.benparvar.cte.wsdl.cteretrecepcao.CteRetRecepcaoStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axis2.transport.http.HTTPConstants;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

/**
 * The type Consulta recibo.
 */
class ConsultaRecibo {

    /**
     * Recibo cte t ret cons reci c te.
     *
     * @param config the config
     * @param recibo the recibo
     * @return the t ret cons reci c te
     * @throws CteException the cte exception
     */
    static TRetConsReciCTe reciboCte(ConfiguracoesCte config, String recibo) throws CteException {

        try {

            /**
             * Informaçoes do Certificado Digital.
             */

            TConsReciCTe consReciCTe = new TConsReciCTe();
            consReciCTe.setVersao(ConstantesCte.VERSAO.CTE);
            consReciCTe.setTpAmb(config.getAmbiente().getCodigo());
            consReciCTe.setNRec(recibo);

            String xml = XmlCteUtil.objectToXml(consReciCTe);

            LoggerUtil.log(ConsultaRecibo.class, "[XML-ENVIO]: " + xml);

            OMElement ome = AXIOMUtil.stringToOM(xml);
            CteRetRecepcaoStub.CteDadosMsg dadosMsg = new CteRetRecepcaoStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);

            CteRetRecepcaoStub stub = new CteRetRecepcaoStub(WebServiceCteUtil.getUrl(config, ServicosEnum.CONSULTA_RECIBO));

            CteRetRecepcaoStub.CteCabecMsg cteCabecMsg = new CteRetRecepcaoStub.CteCabecMsg();
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteRetRecepcaoStub.CteCabecMsgE cteCabecMsgE = new CteRetRecepcaoStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            // Timeout
            if (ObjetoCTeUtil.verifica(config.getTimeout()).isPresent()) {
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getTimeout());
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
                        config.getTimeout());
            }
            CteRetRecepcaoStub.CteRetRecepcaoResult result = stub.cteRetRecepcao(dadosMsg, cteCabecMsgE);

            LoggerUtil.log(ConsultaRecibo.class, "[XML-RETORNO]: " + result.getExtraElement().toString());
            return XmlCteUtil.xmlToObject(result.getExtraElement().toString(), TRetConsReciCTe.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }
}
