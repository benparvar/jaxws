package com.benparvar.cte;

import com.benparvar.cte.dom.ConfiguracoesCte;
import com.benparvar.cte.dom.enuns.ServicosEnum;
import com.benparvar.cte.exception.CteException;
import com.benparvar.cte.schema_300.conssitcte.TConsSitCTe;
import com.benparvar.cte.schema_300.retconssitcte.TRetConsSitCTe;
import com.benparvar.cte.util.*;
import com.benparvar.cte.wsdl.cteconsulta.CteConsultaStub;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axis2.transport.http.HTTPConstants;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.rmi.RemoteException;

/**
 * The type Consulta xml.
 */
class ConsultaXml {

    /**
     * Consulta xml t ret cons sit c te.
     *
     * @param config the config
     * @param chave  the chave
     * @return the t ret cons sit c te
     * @throws CteException the cte exception
     */
    static TRetConsSitCTe consultaXml(ConfiguracoesCte config, String chave) throws CteException {

        try {

            TConsSitCTe consSitCTe = new TConsSitCTe();
            consSitCTe.setVersao(ConstantesCte.VERSAO.CTE);
            consSitCTe.setTpAmb(config.getAmbiente().getCodigo());
            consSitCTe.setXServ("CONSULTAR");
            consSitCTe.setChCTe(chave);

            String xml = XmlCteUtil.objectToXml(consSitCTe);

            LoggerUtil.log(ConsultaXml.class, "[XML-ENVIO]: " + xml);

            OMElement ome = AXIOMUtil.stringToOM(xml);

            CteConsultaStub.CteDadosMsg dadosMsg = new CteConsultaStub.CteDadosMsg();
            dadosMsg.setExtraElement(ome);

            CteConsultaStub stub = new CteConsultaStub(
                    WebServiceCteUtil.getUrl(config, ServicosEnum.CONSULTA_XML));

            CteConsultaStub.CteCabecMsg cteCabecMsg = new CteConsultaStub.CteCabecMsg();
            cteCabecMsg.setCUF(String.valueOf(config.getEstado().getCodigoUF()));
            cteCabecMsg.setVersaoDados(ConstantesCte.VERSAO.CTE);

            CteConsultaStub.CteCabecMsgE cteCabecMsgE = new CteConsultaStub.CteCabecMsgE();
            cteCabecMsgE.setCteCabecMsg(cteCabecMsg);

            // Timeout
            if (ObjetoCTeUtil.verifica(config.getTimeout()).isPresent()) {
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getTimeout());
                stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
                        config.getTimeout());
            }
            CteConsultaStub.CteConsultaCTResult result = stub.cteConsultaCT(dadosMsg, cteCabecMsgE);

            LoggerUtil.log(ConsultaXml.class, "[XML-RETORNO]: " + result.getExtraElement().toString());
            return XmlCteUtil.xmlToObject(result.getExtraElement().toString(), TRetConsSitCTe.class);

        } catch (RemoteException | XMLStreamException | JAXBException e) {
            throw new CteException(e.getMessage());
        }

    }

}